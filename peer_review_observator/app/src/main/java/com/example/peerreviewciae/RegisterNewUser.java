package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.squareup.okhttp.OkHttpClient;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


/**
 * Clase que controla la vista para registrar un usuario nuevo
 */
public class RegisterNewUser extends Activity {

    private MobileServiceClient mClient;

    private MobileServiceTable<user> userTable;
    private userAdapter userAdapter;

    private EditText firstName_input;
    private EditText lastName1_input;
    private EditText lastName2_input;
    private EditText email1_input;
    private EditText email2_input;
    private EditText password1_input;
    private EditText password2_input;

    private TextView ok;
    private TextView okCheckDatos;
    private TextView okCheckDatos2;
    private Button continueButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new_user);

        //Edittext para que el usuario indique su primer nombre
        firstName_input = (EditText) findViewById(R.id.firstName_editText);
        //Edittext para que el usuario indique su apellido paterno
        lastName1_input = (EditText) findViewById(R.id.lastname1_editText);
        //Edittext para que el usuario indique su apellido materno
        lastName2_input = (EditText) findViewById(R.id.lastname2_editText);
        ////Edittext para que el usuario indique su correo electrónico
        email1_input = (EditText) findViewById(R.id.email1_editText);
        //Edittext para que el usuario indique de nuevo su correo electrónico (para chequear que no haya error en los datos)
        email2_input = (EditText) findViewById(R.id.email2_editText);
        ////Edittext para que el usuario indique su contraseña
        password1_input = (EditText) findViewById(R.id.password1_editText);
        //Edittext para que el usuario indique su contraseña (para chequear que no haya error en los datos)
        password2_input = (EditText) findViewById(R.id.password2_editText);

        //Textview que se muestra cuando la cuenta es creada exitosamente
        ok = (TextView) findViewById(R.id.ok_textView);
        ok.setVisibility(View.GONE);

        //Textview que se muestra si alguno de los datos ingresados están incorrectos
        okCheckDatos = (TextView) findViewById(R.id.okCheckDatos);
        okCheckDatos.setVisibility(View.GONE);

        //Textview que se muestra si el correo electrónico que el usuario está registrando ya se encuentra registrado en el sistema
        okCheckDatos2 = (TextView) findViewById(R.id.ok2);
        okCheckDatos2.setVisibility(View.GONE);

        //Botón para continuar una vez que la cuenta es creada exitosamente
        continueButton = (Button) findViewById((R.id.continue_button));
        continueButton.setVisibility(View.GONE);

        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this).withFilter(new ProgressFilter());

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //Obtenemos la tabla de usuarios
            userTable = mClient.getTable(user.class);

        } catch (MalformedURLException e) {

        } catch (Exception e) {

        }
    }

    /**
     * Método que se llama al apretar el botón de registrar un nuevo usuario
     * @param view
     */
    public void onCLickRegister(View view) {

        okCheckDatos.setVisibility(View.GONE);
        okCheckDatos2.setVisibility(View.GONE);

        if (mClient == null) {
            return;
        }

        //Creamos al usuario que se agregará a la BD
        final user u = new user();

        //Primero se llama al método que chequea que los datos estén correctos
        Boolean okBool = checkOK();

        //Si están correctos, agregamos los datos al objeto "user" y lo agregamos a la BD
        if (okBool) {

            //Se setean los parámetros del usuario nuevo
            u.setName(firstName_input.getText().toString());
            u.setLastName1(lastName1_input.getText().toString());
            u.setLastName2(lastName2_input.getText().toString());
            u.setEmail(email1_input.getText().toString());
            u.setPassword(password1_input.getText().toString());
            u.setRol_id("passive");
            u.setUsername(email1_input.getText().toString());


            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {

                        //Primero buscamos que el usuario no exista ya en la BD
                        final List<user> results = findItemsFromMobileServiceTable();

                        //Si existe, mostramos el mensaje correspondiente
                        if(results.size()>0){

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    okCheckDatos2.setVisibility(View.VISIBLE);
                                    }

                            });

                        }

                        //Si no existe, lo agregamos
                        else
                        {
                            //Se agrega a la BD, y si el proceso es exitoso, se muestra el mensaje correspondiente.
                            final user entity = addItemInTable(u);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (entity != null) {
                                        userAdapter.add(entity);
                                        ok.setVisibility(View.VISIBLE);
                                        continueButton.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            }
                    }

                    catch (final Exception e) {
                        //createAndShowDialogFromTask(e, "Error-4");
                    }
                    return null;
                }
            };

            runAsyncTask(task);
        }

        //Si los datos ingrsados por el usuario son incorrectos, se muestra el mensaje correspondiente.
        else
        {
            okCheckDatos.setVisibility(View.VISIBLE);
        }
    }

    //Método que agrega al usuario en la BD
    public user addItemInTable(user u) throws ExecutionException, InterruptedException {
        user entity = userTable.insert(u).get();
        return entity;
    }

    /**Método que chequea que la información ingresada sea correcta.
     * Chequea:
     * 1- que no hayan datos vacíos
     * 2- que el correo electrónico esté correctamente escrito (viendo que coincidan ambas entradas)
     * 3- que la contraseña esté correcta (viendo que coincidan ambas entradas)
     */
    public Boolean checkOK() {

        Boolean ok = true;

        if(firstName_input.getText().toString().equals(""))
            {ok = false;}
        else if(lastName1_input.getText().toString().equals(""))
            {ok = false;}
        else if(lastName2_input.getText().equals(""))
            {ok = false;}
        else if(email1_input.getText().toString().equals(""))
            {ok = false;}
        else if(password1_input.getText().toString().equals(""))
            {ok = false;}

        else if(!password1_input.getText().toString().equals(password2_input.getText().toString()))
            {ok = false;}
        else if(!email1_input.getText().toString().equals(email2_input.getText().toString()))
            {ok = false;}

        return ok;
    }

    //Método que busca en la BD si existe un usuario con el correo electrónico que se está intentando crear.
    private List<user> findItemsFromMobileServiceTable() throws ExecutionException, InterruptedException {

        return userTable.where().field("Email").eq(email1_input.getText().toString()).execute().get();
    }


    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    //Devuelve a la vista del Login. Se llama cuando se aprieta el botón de continuar.
    public void onClickContinue (View view)
    {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    //Método automático de Azure. No tocar.
    private class ProgressFilter implements ServiceFilter {

        @Override
        public ListenableFuture<ServiceFilterResponse> handleRequest(ServiceFilterRequest request, NextServiceFilterCallback nextServiceFilterCallback) {

            final SettableFuture<ServiceFilterResponse> resultFuture = SettableFuture.create();


            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.VISIBLE);
                }
            });

            ListenableFuture<ServiceFilterResponse> future = nextServiceFilterCallback.onNext(request);

            Futures.addCallback(future, new FutureCallback<ServiceFilterResponse>() {
                @Override
                public void onFailure(Throwable e) {
                    resultFuture.setException(e);
                }

                @Override
                public void onSuccess(ServiceFilterResponse response) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            //  if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.GONE);
                        }
                    });

                    resultFuture.set(response);
                }
            });

            return resultFuture;
        }
    }
}
