package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.squareup.okhttp.OkHttpClient;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class stage3 extends AppCompatActivity {

    private MobileServiceClient mClient;

    String sName;
    String sInstructions;
    String sOpen;
    String sClose;
    String stageID;

    TextView nameTV;
    TextView insTV;
    TextView openTV;
    TextView closeTV;
    TextView chroneExplain;
    TextView chrone;

    private MobileServiceTable<Question> questionTable;

    private MobileServiceTable<Answer> answerTable;

    private MobileServiceTable<Revision>revisionTable;
    private RevisionAdapter revisionAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage3);

        //menú superior (franja azul)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**Seteando la IG. Para no ir a buscarla a la BD en cada etapa y que sea más rápido el sistema, se envía siempre a través
         * un Intent de la vista anterior.
         **/

        //nombre de la etapa
        sName = getIntent().getStringExtra("stageName");
        //instrucciones de la etapa
        sInstructions = getIntent().getStringExtra("stageInstructions");
        //fecha de apertura de la etapa
        sOpen = getIntent().getStringExtra("stageOpen");
        //fecha de cierre de la etapa
        sClose = getIntent().getStringExtra("stageClose");
        //ID de esta etapa, sirve para ir a buscar datos a la BD
        stageID = getIntent().getStringExtra("stageID");

        //El Textview que muestra el nombre de la etapa
        nameTV = (TextView) findViewById(R.id.sName);
        //El Textview que muestra las instrucciones de la etapa
        insTV = (TextView) findViewById(R.id.sInstructions);
        //El Textview que muestra la fecha y hora de apertura de la etapa
        openTV = (TextView) findViewById(R.id.sOpen);
        //El Textview que muestra la fecha y hora de cierre de la etapa
        closeTV = (TextView) findViewById(R.id.sClose);
        //El Textview que muestra lo que es el cronómetro ("El tiempo restante al cierre de la etapa es...")
        chroneExplain = (TextView) findViewById(R.id.chone_explain);
        //El Textview que muestra el cronómetro
        chrone = (TextView) findViewById(R.id.chronometer);

        //seteamos los textview con la información correspondiente.
        nameTV.setText(sName);
        insTV.setText(sInstructions + ":");
        openTV.setText(getString(R.string.FechaAperturaTV) + sOpen);
        closeTV.setText(getString(R.string.FechaCierreTV) + sClose);
        chroneExplain.setText(getString(R.string.CronometroTV));

        //Este método es para el cronómetro. Está explicado más abajo
        //Aquí se echa a andar el cronómetro.
        long diff = getTimeinMilliseconds();
        new CountDownTimer(diff, 1000) {

            public void onTick(long millisUntilFinished) {
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));

                if((millisUntilFinished/1000 < 300) & (millisUntilFinished/1000 > 1))
                {
                    chrone.setTextColor(Color.parseColor("#FF0000"));
                    chrone.playSoundEffect(SoundEffectConstants.CLICK);
                }

                chrone.setText("" + hms);
            }

            public void onFinish() {

            }
        }.start();

        //Aquí nos conectamos a la BD. Este es código estándar de Azure
        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //una vez realizada la conexión, buscamos las tablas:
            //Tabla de preguntas
            questionTable = mClient.getTable(Question.class);
            //tabla de respuestas
            answerTable = mClient.getTable(Answer.class);
            //Tabla de revisiones
            revisionTable = mClient.getTable(Revision.class);

            /**
             * Al igual que en la etapa anterior (Stage2.java), la lista de revisiones
             * se mostrará a través de un "Adapter".
             * Para más información ver Todo: BUSCAR
             */
            revisionAdapter = new RevisionAdapter(this,R.layout.revision_list);
            ListView lvRevision= (ListView) findViewById(R.id.listRevisions);
            lvRevision.setAdapter(revisionAdapter);

            refreshItemsFromTableRevisions();


        } catch (MalformedURLException e) {
            //createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
        } catch (Exception e){
            //createAndShowDialog(e, "Error-here");
        }

    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     */
    private void refreshItemsFromTableRevisions() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //método que llama a la BD y retorna la lista de revisiones (retroalimentaciones)
                    //que le llegaron a este usuario
                    final List<Revision> prevRev = refreshItemsFromMobileServiceTableRevisions();

                    /**
                     * Cuando un usuario envía una respuesta, se guarda, en la columna "ID", el número de esa pregunta
                     * (si era la pregunta 0 o 1).
                     * Inicialmente la idea era guardar el ID de la pregunta pero me equivoqué y quedó así, y sirve igual.
                     */
                    String q1ID = "0";
                    String q2ID = "1";

                    /**
                     * La lista de revisiones tiene las revisiones de la pregunta N°1 y N°2 por
                     * separado de cada usuario revisor, pero nosotros debemos mostrarlas agrupadas
                     * por revisor, entonces agregamos al Adapter solamente las revisiones a la
                     * pregunta N°1:
                     */

                    List<Revision> raux = new ArrayList<>();

                    for(int i =0; i<prevRev.size();i++)
                    {
                        if(prevRev.get(i).getQuestion_id().equals(q1ID)){
                            raux.add(prevRev.get(i));
                        }
                    }

                    /**
                     * Luego chequeamos que no hayan más de una revisión por
                     * revisor, y guardamos en raux2 las revisiones NO repetidas
                     */

                    final List<Revision>raux2 = new ArrayList<>();

                    for(int i = 0; i<raux.size(); i++)
                    {
                        Boolean repeated = false;

                        for(int j = 0; j<raux2.size(); j++)
                        {
                            if(raux2.get(j).getUsername_Reviser().equals(raux.get(i).getUsername_Reviser()))
                            {repeated = true;}
                        }

                        if(!repeated)
                        {
                            raux2.add(raux.get(i));
                        }
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //Finalmente agregamos las revisiones al adapter para mostrar.
                            revisionAdapter.clear();

                            for(Revision item: raux2)
                            revisionAdapter.add(item);

                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Buscamos en la BD las revisiones de:
     * 1- esta actividad
     * 2- esta etapa
     * 3- este usuario como usuario REVISADO
     *
     */
    private List<Revision> refreshItemsFromMobileServiceTableRevisions() throws ExecutionException, InterruptedException {

        List<Revision> a = revisionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage2ID).and().field("Username_Reviewed").eq(Login.userLoggedIn.getUsername()).orderBy("Question_id", QueryOrder.Ascending).execute().get();
        return a;
    }

    //Método que se llama al apretar el botón para ir a ver una revisión.
    public void goToRevision(String usernameReviser, int position){

        chrone.setSoundEffectsEnabled(false);

        Intent intent = new Intent(this, stage3_3.class);

        intent.putExtra("usernameReviewed", usernameReviser);
        intent.putExtra("position", position);
        intent.putExtra("stageName", sName);
        intent.putExtra("stageInstructions", sInstructions);
        intent.putExtra("stageOpen", sOpen);
        intent.putExtra("stageClose",sClose);

        this.finish();

        startActivity(intent);
    }

    public void onVolver (View view)
    {
        chrone.setSoundEffectsEnabled(false);

        Intent intent = new Intent(this, StagesGrid.class);

        intent.putExtra("stageID",stageID);
        intent.putExtra("stageName", sName);
        intent.putExtra("stageInstructions", sInstructions);

        intent.putExtra("stageOpen", sOpen);
        intent.putExtra("stageClose",sClose);

        this.finish();
        startActivity(intent);
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    public long getTimeinMilliseconds() {

        try {
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date date = (Date) formatter.parse(sClose);

            Calendar today = Calendar.getInstance();
            long milisClose = date.getTime();
            long milisNow = today.getTimeInMillis();

            long diff = milisClose - milisNow  + StagesGrid.currentDiffWithLocalTime;
            return diff;

        } catch (Exception e) {
        }

        return 30000;

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_peer_review, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle("Bienvenido " + Login.userNameLoggedIn);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {

            return true;
        }

        else if (id == R.id.changeActivity){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, SelectActivitySession.class);
            this.finish();
            startActivity(intent);

            return true;
        }

        else if (id == R.id.observations){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, ObserverMain.class);
            this.finish();
            startActivity(intent);

            return true;
        }



        else if (id == R.id.closeSession){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, Login.class);
            Login.userLoggedIn = null;
            this.finish();
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }


}
