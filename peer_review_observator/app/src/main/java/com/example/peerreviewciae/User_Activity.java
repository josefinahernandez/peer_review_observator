package com.example.peerreviewciae;

import java.util.Date;

/**
 * Created by Josefina on 11-09-2016.
 */
public class User_Activity {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Username")
    private String Username;

    @com.google.gson.annotations.SerializedName("Activity_id")
    private String Activity_id;


    @com.google.gson.annotations.SerializedName("createdAt")
    private Date createdAt;


    public User_Activity(){

    }

    public User_Activity(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id= id;}

    public String getUsername() {return Username;}
    public void setUsername(String name) {this.Username= name;}

    public String getActivity_id() {return Activity_id;}
    public void setActivity_id(String desc) {this.Activity_id= desc;}

    public Date getCreatedAt() {return createdAt;}


}
