package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.sync.MobileServiceSyncContext;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.ColumnDataType;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.MobileServiceLocalStoreException;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.SQLiteLocalStore;
import com.microsoft.windowsazure.mobileservices.table.sync.synchandler.SimpleSyncHandler;
import com.squareup.okhttp.OkHttpClient;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static com.microsoft.windowsazure.mobileservices.table.query.QueryOperations.val;

/**
 * Clase que controla la vista para seleccionar la actividad
 */
public class SelectActivitySession extends AppCompatActivity {

    private MobileServiceClient mClient;

    private MobileServiceTable<Activity_Session> activity_sessionTable;
    private Activity_SessionAdapter activitySessionAdapter;

    public Boolean isAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_activity_session);

        //Menú superior
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**Si se está llamando a esta vista desde la vista de Administrador, se envía un boolean
         * verdadero para que, luego de escoger la actividad, retorne a la vista de administrador y
         * no a la vista siguiente de usuario normal.
         */
        isAdmin = getIntent().getBooleanExtra("isAdmin", false);

        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //Buscamos la tabla de actividades
            activity_sessionTable= mClient.getTable(Activity_Session.class);

            //Se muestran las actividades a través de un Adapter.
            activitySessionAdapter = new Activity_SessionAdapter(this,R.layout.activity_list);
            ListView lv1= (ListView) findViewById(R.id.listSessions);
            lv1.setAdapter(activitySessionAdapter);

            //Se llama al método que busca las actividades en la BD y las muestra en la vista.
            refreshItemsFromTable();

        } catch (MalformedURLException e) {
            //createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
        } catch (Exception e){
            //createAndShowDialog(e, "Error-here");
        }
    }

    private void refreshItemsFromTable() {

        // Get the items that weren't marked as completed and add them in the
        // adapter

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //Método que retorna las actividades de la BD
                    final List<Activity_Session> results = refreshItemsFromMobileServiceTable();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activitySessionAdapter.clear();

                            //Se agregan todas las Adapter.
                            for (Activity_Session item : results) {
                                activitySessionAdapter.add(item);
                            }
                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    //Se buscan las actividades en la BD
    private List<Activity_Session> refreshItemsFromMobileServiceTable() throws ExecutionException, InterruptedException {
        return activity_sessionTable.where().field("Status").eq("abierto").execute().get();
    }

    /**
     * Método que se llama al seleccionar una actividad. Los parámetros que se piden son enviados en
     * el Adapter
     */
    public void goToActivity(String id, String name, String description, String loginCode){

        Intent intent = new Intent(this, SessionCode.class);

        intent.putExtra("activityID", id);
        intent.putExtra("activityName", name);
        intent.putExtra("activityDescription", description);
        intent.putExtra("activityLoginCode", loginCode);

        //El parámetro de IsAdmin se envía a la siguiente vista porque ahí pide el código,
        // entonces de ahí se decide adonde enviar al usuario
        intent.putExtra("isAdmin", isAdmin);
        this.finish();
        startActivity(intent);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_peer_review, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle("Bienvenido " + Login.userNameLoggedIn); //BienvenidoTitleMenu

        MenuItem otherActivity = menu.findItem(R.id.changeActivity);
        otherActivity.setEnabled(false);

        MenuItem seeObs = menu.findItem(R.id.observations);
        seeObs.setEnabled(false);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.home) {
            return true;
        }

        else if (id == R.id.changeActivity){

            Intent intent = new Intent(this, SelectActivitySession.class);
            this.finish();
            startActivity(intent);

            return true;
        }

        else if (id == R.id.observations){

            Intent intent = new Intent(this, ObserverMain.class);

            this.finish();
            startActivity(intent);

            return true;
        }


        else if (id == R.id.closeSession){

            Intent intent = new Intent(this, Login.class);
            Login.userLoggedIn = null;
            this.finish();
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }
}
