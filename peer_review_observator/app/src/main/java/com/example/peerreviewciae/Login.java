package com.example.peerreviewciae;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.squareup.okhttp.OkHttpClient;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static com.microsoft.windowsazure.mobileservices.table.query.QueryOperations.val;

public class Login extends Activity {

    public static user userLoggedIn;
    public static String userNameLoggedIn;
    public static String userEMailLoggedIn;
    private MobileServiceClient mClient;

    private MobileServiceTable<user> userTable;
    private userAdapter userAdapter;

    private EditText email_input;
    private EditText password_input;

    private TextView incorrectTV;

    TextView errorInternet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Entrada de texto para que el usuario ingrese su correo electrónico
        email_input= (EditText) findViewById(R.id.email_editText);
        //Entrada de texto para que el usuario ingrese su clave
        password_input = (EditText) findViewById(R.id.pasword_editText);

        //Textview que muestra si hay un error en el correo o clave
        incorrectTV = (TextView) findViewById(R.id.incorrect_textView);
        incorrectTV.setVisibility(View.GONE);

        //Textview que indica que no hay conexión a internet
        errorInternet = (TextView)findViewById(R.id.errorInternet);
        errorInternet.setVisibility(View.GONE);

        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this).withFilter(new ProgressFilter());

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //Tabla de todos los usuarios
            userTable = mClient.getTable(user.class);

        } catch (MalformedURLException e) {

        } catch (Exception e){

        }
    }

    //Método que se llama al apretar "Ingresar" y revisa que los datos estén correctos.
    public void pressIngresar (View view){

        incorrectTV.setVisibility(View.GONE);
        errorInternet.setVisibility(View.GONE);

        //Se obtiene el correo ingresado en la variable email
        final String email = email_input.getText().toString();
        //Se obtiene la clave ingresado en la variable password
        final String password = password_input.getText().toString();

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //Se llama a la BD para buscar un usuario que tenga el correo y clave ingresados
                    final List<user> results = refreshItemsFromMobileServiceTable(email,password);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Boolean userExists = false;

                            //Si results no está vacío, entonces el usuario existe
                            for (user item : results) {
                                //crear el string que se mostrará  con el nombre del usuario de ahora en adelante
                                userNameLoggedIn = results.get(0).getName() + " " + results.get(0).getLastName1();
                                //crear un string con el correo
                                userEMailLoggedIn = results.get(0).getEmail();
                                //método que abre la siguiente actividad
                                continueToActivity(item);
                                //se deja el boolean de que el usuario existe en verdadero.
                                userExists = true;

                            }

                            //Si los datos están incorrectos el sistema solamente muestra el aviso correspondiente.
                            if(!userExists){
                                incorrectTV.setVisibility(View.VISIBLE);
                            }


                        }
                    });
                } catch (final Exception e){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            createErrorMessage();
                        }
                    });
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    public void createErrorMessage(){
        errorInternet.setVisibility(View.VISIBLE);
    }

    //Al apretar registrar, el sistema lleva a la vista para crear un nuevo usuario
    public void pressRegistrar(View view){
        Intent intent = new Intent(this, RegisterNewUser.class);
        startActivity(intent);

    }

    //Al apretar para ir al sitio de administrador, el sistema revisa que el usuario y clave corresponda al del administrador
    public void pressAdmin(View view){

        incorrectTV.setVisibility(View.GONE);
        errorInternet.setVisibility(View.GONE);

        //Obtener el correo ingresado
        String email = email_input.getText().toString();
        //Obtener la clave ingresada
        String password = password_input.getText().toString();

        //Solamente hay un usuario que puede ingresar por el momento, con las siguientes credenciales:
        if(email.equals(getString(R.string.adminEmail)) && password.equals(getString(R.string.adminPassword)))
        {
            Intent intent = new Intent(this, Admin.class);
            startActivity(intent);
        }

        else
        {
            incorrectTV.setVisibility(View.VISIBLE);
            errorInternet.setVisibility(View.VISIBLE);
        }

    }

    //Método que busca en la BD al usuario que está intentando ingresar con un correo y una clave específica.
    private List<user> refreshItemsFromMobileServiceTable(String email, String password) throws ExecutionException, InterruptedException {
        return userTable.where().field("Email").eq(email).and().field("Password").eq(password).execute().get();
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    //Método que lleva a la siguiente vista
    public void continueToActivity(user u){

        userLoggedIn = u;

        Intent intent = new Intent(this, SelectActivitySession.class);
        startActivity(intent);
    }

    //Este método venía con Azure pero creo que no se necesita. En otras clases lo borré pero en esta quedó acá.
    private class ProgressFilter implements ServiceFilter {

        @Override
        public ListenableFuture<ServiceFilterResponse> handleRequest(ServiceFilterRequest request, NextServiceFilterCallback nextServiceFilterCallback) {

            final SettableFuture<ServiceFilterResponse> resultFuture = SettableFuture.create();


            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                   // if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.VISIBLE);
                }
            });

            ListenableFuture<ServiceFilterResponse> future = nextServiceFilterCallback.onNext(request);

            Futures.addCallback(future, new FutureCallback<ServiceFilterResponse>() {
                @Override
                public void onFailure(Throwable e) {
                    resultFuture.setException(e);
                }

                @Override
                public void onSuccess(ServiceFilterResponse response) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                          //  if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.GONE);
                        }
                    });

                    resultFuture.set(response);
                }
            });

            return resultFuture;
        }
    }
}
