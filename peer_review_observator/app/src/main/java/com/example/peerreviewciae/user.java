package com.example.peerreviewciae;

/**
 * Created by Josefina on 11-09-2016.
 */
public class user {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Name")
    private String Name;

    @com.google.gson.annotations.SerializedName("LastName1")
    private String LastName1;

    @com.google.gson.annotations.SerializedName("LastName2")
    private String LastName2;

    @com.google.gson.annotations.SerializedName("Username")
    private String Username;

    @com.google.gson.annotations.SerializedName("Rol_id")
    private String Rol_id;

    @com.google.gson.annotations.SerializedName("Email")
    private String Email;

    @com.google.gson.annotations.SerializedName("Password")
    private String Password;


    public user(){

    }

    public user(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id= id;}
    public String getName() {return Name;}
    public void setName(String name) {this.Name= name;}

    public String getLastName1() {return LastName1;}
    public void setLastName1(String name) {this.LastName1= name;}

    public String getLastName2() {return LastName2;}
    public void setLastName2(String name) {this.LastName2= name;}

    public String getUsername() {return Username;}
    public void setUsername(String name) {this.Username= name;}

    public String getRol_id() {return Rol_id;}
    public void setRol_id(String desc) {this.Rol_id= desc;}

    public String getEmail() {return Email;}
    public void setEmail(String status) {this.Email= status;}

    public String getPassword() {return Password;}
    public void setPassword(String name) {this.Password= name;}

}
