package com.example.peerreviewciae;

/**
 * Esta clase no se usa! Fue creada siguiendo el formato del sistema anterior
 */
public class Menu_Options {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Menu_Name")
    private String Menu_Name;

    @com.google.gson.annotations.SerializedName("Menu_Order")
    private int Menu_Order;



    public Menu_Options(){

    }

    public Menu_Options(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}

    public String getMenu_Name() {return Menu_Name;}
    public void setMenu_Name(String mn) {this.Menu_Name= mn;}

    public int getMenu_Order(){return Menu_Order;}
    public void setMenu_Order(int mn){this.Menu_Order= mn;}


}
