package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

/**
 * Clase tipo "Adapter" que controla la forma en que se muestran las etapas en StagesGrid
 */
public class StageAdapter extends ArrayAdapter<Stage> {


    StagesGrid mStagesGrid;
    Context mContext;

    int mLayoutResourceId;

    public StageAdapter(Context context, int layoutResourceId, StagesGrid stagesGrid) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;

        //Se necesita el objeto de StageGrid para actualizar algunos valores.
        mStagesGrid = stagesGrid;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        /**
         * Cada etapa muestra varios datos en forma de Textview
         */
        final Stage currentItem = getItem(position);

        row.setTag(currentItem);

        //Nombre de la etapa
        final TextView name = (TextView)row.findViewById(R.id.sName);
        //Estado de la etapa (solo habrá 1 etapa abierta, las demás cerradas)
        final TextView status = (TextView)row.findViewById(R.id.sStatus);
        //Fecha de apertura
        final TextView openD = (TextView)row.findViewById(R.id.openDate);
        //Fecha de cierre
        final TextView closeD = (TextView)row.findViewById(R.id.closeDate);

        //obtenemos las fechas de apertura y cierre
        Date openDate = currentItem.getStart_Date();
        Date closeDate = currentItem.getClose_Date_Date();

        //Seteamos el nombre y fechas.
        name.setText(currentItem.getStage_Name());
        openD.setText(mContext.getString(R.string.FechaAperturaTV) + openDate);
        closeD.setText(mContext.getString(R.string.FechaCierreTV) + closeDate);

        final Button go = (Button) row.findViewById(R.id.go_button);
        go.setText(mContext.getString(R.string.goButton));

        /**
         * Para el estado de la etapa, primero vemos qué dice la BD, pero luego, chequeamos el horario
         * del dispositivo v/s la hr verdadera y cambiamos el estado si corresponde.
         *
         */

        status.setText(currentItem.getStage_Status().toUpperCase());

        //Si currentStatus es verdadero, esta etapa está abierta.
        Boolean currentStatus = false;

        //Cálculo de la hr real v/s de la hora del dispositivo:

        //now es la hr actual del dispositivo
        Date now = Calendar.getInstance().getTime();
        //realnow es la hr actual del dispositivo ajustada a la diferencia con la hr local
        Date realNow = new Date(now.getTime() - StagesGrid.currentDiffWithLocalTime);
        //hr de apertura de la etapa
        Date open = currentItem.getStart_Date();
        //hr de cierre de la etapa
        Date close = currentItem.getClose_Date_Date();

        //vemos si la hr de apertura es antes de la hr actual
        Boolean nowAfterOpen = realNow.after(open);
        //vemos si la hr de cierre es después de la hr actual
        Boolean nowBeforeClose = realNow.before(close);

        //si ambos booleans anteriores son verdaderos, entonces la etapa está abierta.
        if(nowAfterOpen && nowBeforeClose)
        {
            currentStatus = true;
            mStagesGrid.setActiveStageID(currentItem.getID());
        }

        //si la etapa está cerrada, se deshabilita el botón para seleccionar esta etapa y se setea el texto del textview
        if(!currentStatus){
            go.setEnabled(false);
            status.setText(mContext.getString(R.string.statusCerrado));

        }
        //si la etapa está abierta, se habilita el botón para seleccionar esta etapa y se setea el texto del textview
        else {
            go.setEnabled(true);

            status.setText(mContext.getString(R.string.statusAbierto));

            //se setea también qué ocurre cuando se aprieta el botón. Se envía la etapa completa como parámetro.
            go.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {

                    if (mContext instanceof StagesGrid) {
                        StagesGrid activityChosen = (StagesGrid) mContext;
                            activityChosen.goToActivity(currentItem);
                    }
                }
            }
            );
        }

        return row;
    }
}
