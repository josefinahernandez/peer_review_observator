package com.example.peerreviewciae;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Daniela on 23-06-2016.
 */
public class PhotographActivity extends Activity implements OnClickListener {

    int observationTemplate;
    String observationItem;
    Button btnSavePic;
    ImageView ivThumbnailPhoto;
    Bitmap bitMap;
    static int TAKE_PICTURE = 1;
    String mCurrentPhotoPath;
    TextView tv_item;
    String observationSubitem;
    String path;
    EditText photoComment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_picture);
        observationTemplate = (int) getIntent().getExtras().get("template");
        observationItem = (String) getIntent().getExtras().get("item");
        observationSubitem = (String) getIntent().getExtras().get("subitem");

        // Get reference to views
        btnSavePic = (Button) findViewById(R.id.button_save_image);
        ivThumbnailPhoto = (ImageView) findViewById(R.id.imageview);
        tv_item = (TextView) findViewById(R.id.item_photo);
        photoComment = (EditText) findViewById(R.id.comment_photo);

        if (observationItem != null && observationSubitem!= null){
            String text = observationItem+" "+observationSubitem;
            tv_item.setText(text);
        }else{
            if (observationTemplate == 1)
                tv_item.setText("COPUS");
        }

        // add onclick listener to the button
        btnSavePic.setOnClickListener(this);

        /*
        To write in devices with sdk>=23 Android 6.0
         */
        if (Build.VERSION.SDK_INT >= 23){
            if (ContextCompat.checkSelfPermission(PhotographActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                createFolder();
                if (ContextCompat.checkSelfPermission(PhotographActivity.this,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, TAKE_PICTURE);
                }else{
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},1);
                }

            }else{
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
            }
        }
        else{
            createFolder();
            // create intent with ACTION_IMAGE_CAPTURE action
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            startActivityForResult(intent, TAKE_PICTURE);
        }

    }

    public void createFolder(){
        File dir = new File(Environment.getExternalStorageDirectory(),"PeerReview");

        if (!dir.exists()){
            try{
                if(dir.mkdir()) {
                    Log.d("Directory created",dir.getAbsolutePath());
                } else {
                    Log.d("Dir is not created", dir.getAbsolutePath());
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("PHOTO","Permission: "+permissions[0]+ "was "+grantResults[0]);
            createFolder();
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(i, TAKE_PICTURE);
        }
    }

    // on button "btnTackPic" is clicked
    @Override
    public void onClick(View view) {

        //Aca va cuando queremos guardar la foto
        if (observationTemplate == 1){
            COPUSRegister cr = COPUSRegister.getInstance();
            cr.putPhoto(path,photoComment.getText().toString());
        }

        finish();
    }

    private File createImageFile() throws IOException{

        String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        String imageFileName = "";
        switch (observationTemplate){
            case 0:
                imageFileName += "MINEDUC";
                break;
            case 1:
                imageFileName += "COPUS";
                break;
        }
        imageFileName += timeStamp;
        /*
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,"SmartSpeech");
         */
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //File image = File.createTempFile(imageFileName,".jpg",storageDir);

        File storageDir = Environment.getExternalStorageDirectory();
        File image = new File(storageDir,"PeerReview");


        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == TAKE_PICTURE && resultCode== RESULT_OK && intent != null){
            // get bundle
            Bundle extras = intent.getExtras();
            Bitmap bitMap = (Bitmap) intent.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitMap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
            String imageFileName = "";
            switch (observationTemplate){
                case 0:
                    imageFileName += " MINEDUC ";
                    break;
                case 1:
                    imageFileName += " COPUS ";
                    break;
            }
            imageFileName += timeStamp+".jpg";
            //File destination = new File(Environment.getExternalStorageDirectory(),
                    //imageFileName + ".jpg");
            File destination = new File(Environment.getExternalStorageDirectory()+"/"+"PeerReview/"+imageFileName);

            /*
            if (!destination.exists()){
                destination.mkdirs();
            }
            */

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                Log.d("Foto","OK");
                fo.close();
                path = destination.getAbsolutePath();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ivThumbnailPhoto.setImageBitmap(bitMap);
        }
    }
}

