package com.example.peerreviewciae;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Esta clase no se usa! Fue creada siguiendo el formato del sistema anterior
 */
public class Menu_AccessAdapter extends ArrayAdapter<Menu_Access> {

    Context context;

    int layoutResourceId;

    public Menu_AccessAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        context = context;
        layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        return row;
    }
}
