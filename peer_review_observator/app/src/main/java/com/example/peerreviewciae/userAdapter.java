package com.example.peerreviewciae;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Clase creada por defecto. No se usa
 */
public class userAdapter extends ArrayAdapter<user> {

    Context context;

    int layoutResourceId;

    public userAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        context = context;
        layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        return row;
    }
}
