package com.example.peerreviewciae;

/**
 * Created by Josefina on 13-09-2016.
 */
public class Assignment {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Activity_id")
    private String Activity_id;

    @com.google.gson.annotations.SerializedName("Stage_id")
    private String Stage_id;

    @com.google.gson.annotations.SerializedName("Username_Reviser")
    private String Username_Reviser;

    @com.google.gson.annotations.SerializedName("Username_Reviewed")
    private String Username_Reviewed;

    @com.google.gson.annotations.SerializedName("Assignment_Status")
    private String Assignment_Status;


    public Assignment(){

    }

    public Assignment(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}

    public String getStage_id() {return Stage_id;}
    public void setStage_id(String desc) {this.Stage_id= desc;}

    public String getActivity_id(){return Activity_id;}
    public void setActivity_id(String at){this.Activity_id= at;}

    public String getUsername_Reviser() {return Username_Reviser;}
    public void setUsername_Reviser(String qID) {this.Username_Reviser= qID;}

    public String getUsername_Reviewed() {return Username_Reviewed;}
    public void setUsername_Reviewed(String name) {this.Username_Reviewed= name;}

    public String getAssignment_Status(){return Assignment_Status;}
    public void setAssignment_Status(String at){this.Assignment_Status = at;}

}
