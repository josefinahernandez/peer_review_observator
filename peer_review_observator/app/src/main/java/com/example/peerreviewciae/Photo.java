package com.example.peerreviewciae;

import android.database.Cursor;

/**
 * Created by Daniela on 25-07-2016.
 */
public class Photo {

    public final static int PHOTO_NEW = 0;
    public final static int PHOTO_SAVED = 1;
    public final static int PHOTO_UPLOADING = 2;
    public final static int PHOTO_UPLOADED = 3;
    private int status;
    private long observationId;
    private String item;
    private String hash;
    private String path;
    private boolean sync;
    private long id;
    private String comment;
    private long time;

    public long getTime() {return time;}

    public void setTime(long time) {this.time = time;}

    public String getPath(){
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public int getStatus(){
        return status;
    }

    public void setObservationId(long observationId){
        this.observationId = observationId;
    }

    public long getObservationId(){
        return observationId;
    }

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setHash(String hash){
        this.hash = hash;
    }

    public String getHash(){
        return hash;
    }

    public void setItem(String item){
        this.item = item;
    }

    public String getItem(){
        return item;
    }

    public String getComment(){return comment;}

    public void setComment(String comment) {this.comment = comment;}

    public static Photo load(Cursor c){
        Photo photo = new Photo();
        photo.setPath(c.getString(c.getColumnIndex("path")));
        photo.setStatus(c.getInt(c.getColumnIndex("status")));
        photo.setSync(false);
        photo.setObservationId(c.getLong(c.getColumnIndex("observation_id")));
        photo.setId(c.getLong(c.getColumnIndex("_id")));
        photo.setHash(c.getString(c.getColumnIndex("hash")));
        photo.setItem(c.getString(c.getColumnIndex("item")));
        photo.setComment(c.getString(c.getColumnIndex("comment")));
        return photo;
    }

}
