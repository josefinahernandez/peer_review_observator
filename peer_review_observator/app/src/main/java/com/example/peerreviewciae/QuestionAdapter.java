package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

/**
 * Clase de tipo "Adapter" que muestra una lista de preguntas en forma de Textview
 */
public class QuestionAdapter extends ArrayAdapter<Question>{

    Context mcontext;

    int mlayoutResourceId;

    public QuestionAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mcontext = context;
        mlayoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final Question currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(mlayoutResourceId, parent, false);
        }

        row.setTag(currentItem);

        //Se crea el textview y se le entrega el texto correspondiente.
        final TextView textView= (TextView) row.findViewById(R.id.question_textview);
        textView.setText(currentItem.getQuestion_text());
        textView.setEnabled(true);

        return row;
    }
}
