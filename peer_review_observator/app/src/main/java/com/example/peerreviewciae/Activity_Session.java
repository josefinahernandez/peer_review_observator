package com.example.peerreviewciae;

import android.content.Intent;

import java.util.Date;

/**
 * Created by Josefina on 11-09-2016.
 */
public class Activity_Session {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Description")
    private String Description;

    @com.google.gson.annotations.SerializedName("Status")
    private String Status;

    @com.google.gson.annotations.SerializedName("Welcome_message")
    private String Welcome_message;

    @com.google.gson.annotations.SerializedName("Name")
    private String Name;

    @com.google.gson.annotations.SerializedName("Login_Code")
    private String Login_Code;

    @com.google.gson.annotations.SerializedName("createdAt")
    private Date createdAt;


    public Activity_Session(){

    }

    public Activity_Session(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id= id;}
    public String getDescription() {return Description;}
    public void setDescription(String desc) {this.Description= desc;}
    public String getStatus() {return Status;}
    public void setStatus(String status) {this.Status= status;}
    public String getName() {return Name;}
    public void setName(String name) {this.Name= name;}
    public String getWelcome_message() {return Welcome_message;}
    public void setWelcome_message(String name) {this.Welcome_message= name;}

    public String getLogin_Code(){return Login_Code;}
    public void setLogin_Code(String c){this.Login_Code = c;}

    public Date getCreatedAt() {return createdAt;}


}
