package com.example.peerreviewciae;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniela on 19-07-2016.
 */
public class CustomTimeLineAdapter extends RecyclerView.Adapter<CustomTimeLineAdapter.MyViewHolder> {

    private List<String> titles = new ArrayList<>();
    private List<String> subtitles = new ArrayList<>();
    /*private int[] mineducImages = {R.drawable.ic_done_black_24dp,
            R.drawable.ic_block_black_24dp,
            R.drawable.ic_clear_black_24dp};*/
    private List<Photo> photos = new ArrayList<>();


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewSubtitle;
        TextView textViewComment;
        ImageView imageViewIcon;
        ImageView imageViewItem;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewTitle = (TextView) itemView.findViewById(R.id.time_line_item_title);
            this.textViewSubtitle = (TextView) itemView.findViewById(R.id.time_line_item_subtitle);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.imageView);
            this.imageViewItem = (ImageView) itemView.findViewById(R.id.time_line_item_photo);
            this.textViewComment = (TextView) itemView.findViewById(R.id.comment_photo_time_line);
        }
    }

    public CustomTimeLineAdapter(List<String> titles, List<String> subtitles, List<Photo> photos) {
        this.titles = titles;
        this.subtitles = subtitles;
        this.photos = photos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_line_item, parent, false);



        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        TextView textViewTitle = holder.textViewTitle;
        TextView textViewSubtitle = holder.textViewSubtitle;
        ImageView imageView = holder.imageViewIcon;
        ImageView imageViewItem = holder.imageViewItem;
        TextView textViewComment = holder.textViewComment;


        textViewTitle.setText(titles.get(position));
        textViewSubtitle.setText(subtitles.get(position));

        /*if (titles.get(position).contains("no presente")){
            imageView.setImageResource(R.drawable.ic_clear_blue_24dp);
        }else if (titles.get(position).contains("presente")) {
            imageView.setImageResource(R.drawable.ic_done_blue_24dp);
        } else if (titles.get(position).contains("Estudiante")){
            imageView.setImageResource(R.drawable.ic_face_blue_24dp);
        } else if (titles.get(position).contains("Profesor")) {
            imageView.setImageResource(R.drawable.ic_school_blue_24dp);
        }else{
            imageView.setImageResource(R.drawable.ic_block_blue_24dp);
        }
*/
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        for(Photo p:photos){
            String fecha = df.format(p.getTime());
            if (df.format(p.getTime()).equals(titles.get(position))) {
                Bitmap bitmap = BitmapFactory.decodeFile(p.getPath());
                imageViewItem.setImageBitmap(bitmap);
                //textViewComment.setText("Comentario: " + p.getComment());
            }
            else {
                imageViewItem.getLayoutParams().height = 0;
                textViewComment.setPadding(0, 0, 0, 0);
            }
        }




    }

    @Override
    public int getItemCount() {
        return titles.size();
    }

}
