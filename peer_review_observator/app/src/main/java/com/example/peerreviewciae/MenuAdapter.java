package com.example.peerreviewciae;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Esta clase no se usa! Fue creada siguiendo el formato del sistema anterior
 */
public class MenuAdapter extends ArrayAdapter<Menu_Options> {

    Context context;

    int layoutResourceId;

    public MenuAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        context = context;
        layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        return row;
    }
}
