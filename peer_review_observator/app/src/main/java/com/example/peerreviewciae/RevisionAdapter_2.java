package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

/**
 * Clase tipo "Adapter" que muestra las revisiones en forma de Textview para la etapa 4
 */
public class RevisionAdapter_2 extends ArrayAdapter<Revision> {

    Context mcontext;

    int mlayoutResourceId;

    public RevisionAdapter_2(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mcontext = context;
        mlayoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final Revision currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(mlayoutResourceId, parent, false);
        }

        row.setTag(currentItem);

        //Se crea el textview y se setea el texto correspondiente.
        final TextView tv = (TextView) row.findViewById(R.id.revision_textView);

        if(currentItem.getQuestion_id().equals("0")) {
            tv.setText(currentItem.getRevision_Feedback());
        }
        else{
            tv.setText(currentItem.getRevision_Feedback());
        }

        return row;
    }
}
