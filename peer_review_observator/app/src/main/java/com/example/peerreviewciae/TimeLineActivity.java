package com.example.peerreviewciae;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TimeLineActivity extends AppCompatActivity {

    //ListView timeLine;
    TextView title;
    List<String> titles;
    List<String> subtitles;
    List<Photo> photos;
    long observation_code;
    String observation_template;
    String data;
    String time;

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    static View.OnClickListener myOnClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_line);
        //timeLine = (ListView) findViewById(R.id.time_line_list);
        title = (TextView) findViewById(R.id.observation_title);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarObserver);
        setSupportActionBar(toolbar);
        data  = (String) getIntent().getExtras().get("data");
        observation_code = Long.parseLong(data.substring(0,data.indexOf("-")));
        Log.d ("OBS CODE",""+observation_code);
        observation_template = data.substring(data.indexOf("(")+1,data.indexOf(")"));
        time = data.substring(data.indexOf(")")+1);
        Log.d ("OBS TMP",""+observation_template);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_time_line);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        getInformation();

        //CustomItemTimeLine adapter = new CustomItemTimeLine(TimeLineActivity.this, titles, subtitles);
        CustomTimeLineAdapter adapter = new CustomTimeLineAdapter(titles,subtitles,photos);
        title.setText(observation_template+" "+time);

        recyclerView.setAdapter(adapter);
        //timeLine.setAdapter(adapter);
    }

    private void getInformation(){
        PeerReviewDB dbhelper = new PeerReviewDB(getApplicationContext());
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        Log.d("SQL","getReadableDataBase");
        titles = new ArrayList<>();
        subtitles = new ArrayList<>();
        photos = new ArrayList<>();

        switch(observation_template){
            case "COPUS":
                String selectQuery = "SELECT * FROM observation WHERE observation_code = "+observation_code +" ORDER BY time";
                Cursor c2 = db.rawQuery(selectQuery,null);
                Long startObservation = 0L;

                if (c2.moveToFirst()) {
                    do {
                        String full_code =  c2.getString(c2.getColumnIndex("code"));
                        Log.d("FULL CODE COPUS",full_code);
                        if (full_code.indexOf("-") == -1){
                            if (full_code.equals("START")){
                                startObservation = c2.getLong(c2.getColumnIndex("time"));
                            }
                            continue;
                        }


                        String code = full_code.substring(0,full_code.indexOf("-"));
                        String event = full_code.substring(full_code.indexOf("-")+1);
                        Log.d("CODE",code);
                        String title = "";
                        switch (code.charAt(0)){
                            case 's':
                                if (event.equals("S"))
                                    title += "Estudiante - ";
                                else
                                    break;
                                switch (code){
                                    case "sL":
                                        title += getString(R.string.sL_COPUS);
                                        break;
                                    case "sInd":
                                        title += getString(R.string.sInd_COPUS);
                                        break;
                                    case "sCG":
                                        title += getString(R.string.sCG_COPUS);
                                        break;
                                    case "sWG":
                                        title += getString(R.string.sWG_COPUS);
                                        break;
                                    case "sAnQ":
                                        title += getString(R.string.sAnQ_COPUS);
                                        break;
                                    case "sSQ":
                                        title += getString(R.string.sSQ_COPUS);
                                        break;
                                    case "sWC":
                                        title += getString(R.string.sWC_COPUS);
                                        break;
                                    case "sPrd":
                                        title += getString(R.string.sPrd_COPUS);
                                        break;
                                    case "sSP":
                                        title += getString(R.string.sSP_COPUS);
                                        break;
                                    case "sTQ":
                                        title += getString(R.string.sTQ_COPUS);
                                        break;
                                    case "sW":
                                        title += getString(R.string.sW_COPUS);
                                        break;
                                    case "sOG":
                                        title += getString(R.string.sOG_COPUS);
                                        break;

                                }
                                break;
                            case 't':
                                if (event.equals("S"))
                                    title += "Profesor - ";
                                else
                                    break;
                                switch (code){
                                    case "tLec":
                                        title += getString(R.string.tLec_COPUS);
                                        break;
                                    case "tW":
                                        title += getString(R.string.tW_COPUS);
                                        break;
                                    case "tRtW":
                                        title += getString(R.string.tRtW_COPUS);
                                        break;
                                    case "tFUp":
                                        title += getString(R.string.tFUp_COPUS);
                                        break;
                                    case "tPQ":
                                        title += getString(R.string.tPQ_COPUS);
                                        break;
                                    case "tCQ":
                                        title += getString(R.string.tCQ_COPUS);
                                        break;
                                    case "tAnQ":
                                        title += getString(R.string.tAnQ_COPUS);
                                        break;
                                    case "tMG":
                                        title += getString(R.string.tMG_COPUS);
                                        break;
                                    case "t101":
                                        title += getString(R.string.t101_COPUS);
                                        break;
                                    case "tDV":
                                        title += getString(R.string.tDV_COPUS);
                                        break;
                                    case "tAdm":
                                        title += getString(R.string.tAdm_COPUS);
                                        break;
                                }
                            break;
                        }
                        /*
                        Sacamos los eventos
                         */
                        if (title != ""){
                            titles.add(title);
                            String selectQuery2 = "SELECT * FROM observation WHERE observation_code = "+observation_code +" and code LIKE '"+code+"%' order BY time";
                            Log.d("Consu",selectQuery2);
                            Cursor c3 = db.rawQuery(selectQuery2,null);
                            Log.d("Respuesta",""+c3.getCount());
                            Long duration = 0L;
                            Long start = 0L;
                            Long end = 0L;
                            boolean isObservation = false;
                            String subtitle = "";

                            if (c3.moveToFirst()) {
                                do {
                                    Log.d("C3",""+c3.getLong(c3.getColumnIndex("time")));
                                    if (c3.getLong(c3.getColumnIndex("time")) == c2.getLong(c2.getColumnIndex("time"))){
                                        isObservation = true;
                                        start = c3.getLong(c3.getColumnIndex("time"));
                                        Log.d("START TIME",start.toString());
                                        continue;
                                    }
                                    if (isObservation){
                                        end = c3.getLong(c3.getColumnIndex("time"));
                                        duration = end - start;
                                        Log.d("DURATION",""+c3.getLong(c3.getColumnIndex("time"))+"-"+start);
                                        break;
                                    }
                                } while (c3.moveToNext());
                            }

                            DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                            subtitle += "Hora inicio "+formatter.format(start) +"- Hora termino "+formatter.format(end)+"\n";

                            DateFormat formatter2 = new SimpleDateFormat("mm:ss");

                            subtitle += "Start: "+formatter2.format(start-startObservation)+ " - End "+formatter2.format(end-startObservation)+"\n";
                            subtitle += "Duration: "+formatter2.format(duration)+"\n";

                            subtitles.add(subtitle);
                        }
                    } while (c2.moveToNext());
                }
                /*
                Sacamos las fotos
                 */
                String selectQueryPhoto = "SELECT * FROM photos WHERE observation_code = "+observation_code +" ORDER BY time";
                Log.d("Consulta", selectQueryPhoto);
                Cursor c5 = db.rawQuery(selectQueryPhoto,null);

                if (c5.moveToFirst()){
                    do {
                        Photo p = new Photo();
                        Long time = c5.getLong(c5.getColumnIndex("time"));
                        String path = c5.getString(c5.getColumnIndex("path"));
                        String comment = c5.getString(c5.getColumnIndex("comment"));
                        p.setPath(path);
                        p.setTime(time);
                        p.setComment(comment);
                        photos.add(p);
                        DateFormat df = new SimpleDateFormat("HH:mm:ss");
                        titles.add(df.format(time));
                        subtitles.add("Comentario:\n"+comment);
                    } while (c5.moveToNext());
                }

                /*
                Sacamos los comentarios
                 */
                String selectQueryComment = "SELECT * FROM comments WHERE observation_code = "+observation_code +" ORDER BY time";
                Log.d("Consulta ",selectQueryComment);
                Cursor c4 = db.rawQuery(selectQueryComment,null);

                if (c4.moveToFirst()) {
                    do {
                        String title = "Comentario - ";
                        title += (c4.getString(c4.getColumnIndex("observation_item")).equals("T"))?"Profesor":"Estudiante";
                        String subtitle = "";

                        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                        subtitle += "Hora: "+formatter.format(c4.getLong(c4.getColumnIndex("time")))+"\n";
                        subtitle += c4.getString(c4.getColumnIndex("comment"));

                        titles.add(title);
                        subtitles.add(subtitle);

                    } while (c4.moveToNext());
                }
                        break;

        }

        /*
        String selectQueryCode = "SELECT * FROM observation WHERE observation_code = "+observation_code +" ORDER BY code";
        titles = new ArrayList<>();
        subtitles = new ArrayList<>();
        Log.d("BUSCANDO",selectQueryCode);
        Cursor c = db.rawQuery(selectQueryCode,null);


        if (c.moveToFirst()){
            do {
                switch (observation_template){
                    case "MINEDUC":
                        String full_code = c.getString(c.getColumnIndex("code"));
                        String isPresent = full_code.substring(full_code.indexOf("-")+1);
                        String code = full_code.substring(0,full_code.indexOf("-"));
                        String title = "Aspecto "+ code;
                        switch (isPresent){
                            case "0":
                                title += " presente";
                                break;
                            case "1":
                                title += " no presente";
                                break;
                            case "2":
                                title += " no aplica";
                                break;
                        }

                        titles.add(title);
                        Log.d("CODE",code);
                        Log.d("isPresent",isPresent);

                        String subtitle = "";
                        switch (code){
                            case "1.1":
                                subtitle = getString(R.string.mineduc_1_1);
                                break;
                            case "1.2":
                                subtitle = getString(R.string.mineduc_1_2);
                                break;
                            case "1.3":
                                subtitle = getString(R.string.mineduc_1_3);
                                break;
                            case "1.4":
                                subtitle = getString(R.string.mineduc_1_4);
                                break;
                            case "1.5":
                                subtitle = getString(R.string.mineduc_1_5);
                                break;
                            case "1.6":
                                subtitle = getString(R.string.mineduc_1_6);
                                break;
                            case "1.7":
                                subtitle = getString(R.string.mineduc_1_7);
                                break;
                            case "1.8":
                                subtitle = getString(R.string.mineduc_1_8);
                                break;
                            case "1.9":
                                subtitle = getString(R.string.mineduc_1_9);
                                break;
                            case "2.1":
                                subtitle = getString(R.string.mineduc_2_1);
                                break;
                            case "2.2":
                                subtitle = getString(R.string.mineduc_2_2);
                                break;
                            case "2.3":
                                subtitle = getString(R.string.mineduc_2_3);
                                break;
                            case "2.4":
                                subtitle = getString(R.string.mineduc_2_4);
                                break;
                            case "2.5":
                                subtitle = getString(R.string.mineduc_2_5);
                                break;
                            case "2.6":
                                subtitle = getString(R.string.mineduc_2_6);
                                break;
                            case "2.7":
                                subtitle = getString(R.string.mineduc_2_7);
                                break;
                            case "2.8":
                                subtitle = getString(R.string.mineduc_2_8);
                                break;
                            case "2.9":
                                subtitle = getString(R.string.mineduc_2_9);
                                break;
                            case "2.10":
                                subtitle = getString(R.string.mineduc_2_10);
                                break;
                            case "2.11":
                                subtitle = getString(R.string.mineduc_2_11);
                                break;
                            case "3.1":
                                subtitle = getString(R.string.mineduc_3_1);
                                break;
                            case "3.2":
                                subtitle = getString(R.string.mineduc_3_2);
                                break;
                            case "3.3":
                                subtitle = getString(R.string.mineduc_3_3);
                                break;
                            case "3.4":
                                subtitle = getString(R.string.mineduc_3_4);
                                break;
                            case "3.5":
                                subtitle = getString(R.string.mineduc_3_5);
                                break;
                            case "3.6":
                                subtitle = getString(R.string.mineduc_3_6);
                                break;
                            case "3.7":
                                subtitle = getString(R.string.mineduc_3_7);
                                break;
                            case "3.8":
                                subtitle = getString(R.string.mineduc_3_8);
                                break;
                            case "4.1":
                                subtitle = getString(R.string.mineduc_4_1);
                                break;
                            case "4.2":
                                subtitle = getString(R.string.mineduc_4_2);
                                break;
                            case "4.3":
                                subtitle = getString(R.string.mineduc_4_3);
                                break;
                            case "4.4":
                                subtitle = getString(R.string.mineduc_4_4);
                                break;
                            case "4.5":
                                subtitle = getString(R.string.mineduc_4_5);
                                break;
                            case "4.6":
                                subtitle = getString(R.string.mineduc_4_6);
                                break;
                            case "4.7":
                                subtitle = getString(R.string.mineduc_4_7);
                                break;
                            case "4.8":
                                subtitle = getString(R.string.mineduc_4_8);
                                break;
                            case "4.9":
                                subtitle = getString(R.string.mineduc_4_9);
                                break;


                        }
                        subtitles.add(subtitle);
                        break;
                    case "COPUS":

                        break;
                }



                String mostrar = "";
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                int id = c.getInt(c.getColumnIndex("_id"));
                String template = c.getString(c.getColumnIndex("observation_template"));
                long time = c.getLong(c.getColumnIndex("time"));


                mostrar += id + " "+ "("+template+")"+formatter.format(time)+"\n";
                //obs_done_list.add(mostrar);

            }while(c.moveToNext());
        }
*/
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_observer, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle("Bienvenido " + Login.userNameLoggedIn);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {

            return true;
        }

        else if (id == R.id.changeActivity){
            Intent intent = new Intent(this, SelectActivitySession.class);
            startActivity(intent);

            return true;
        }

        else if (id == R.id.changeToPeerReview){
            Intent intent = new Intent(this, StagesGrid.class);
            startActivity(intent);

            return true;
        }

        else if (id == R.id.closeSession){
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }
}
