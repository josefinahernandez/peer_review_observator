package com.example.peerreviewciae;

import android.app.Activity;
import android.app.assist.AssistContent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.squareup.okhttp.OkHttpClient;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Admin extends Activity {


    private MobileServiceClient mClient;

    private MobileServiceTable<Activity_Session> activity_sessionTable;
    private MobileServiceTable<Question> questionTable;
    private MobileServiceTable<Stage> stageTable;
    private MobileServiceTable<Answer> answerTable;
    private MobileServiceTable<Assignment> assignmentTable;


    private String sessionID;
    private String stage1ID;
    private String stage2ID;
    private String stage3ID;
    private String stage4ID;

    TextView ok1;
    TextView ok2;
    TextView ok3;
    TextView ok4;
    TextView ok5;
    TextView ok6;
    TextView ok7;

    List<Answer> allAnswers;

    Date currentTimeWorldAdmin;
    long currentDiffWithLocalTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            activity_sessionTable= mClient.getTable(Activity_Session.class);
            questionTable= mClient.getTable(Question.class);
            stageTable= mClient.getTable(Stage.class);

            answerTable=mClient.getTable(Answer.class);
            assignmentTable=mClient.getTable(Assignment.class);


        } catch (MalformedURLException e) {
            //createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
        } catch (Exception e){
            //createAndShowDialog(e, "Error-here");
        }

        ok1 = (TextView)findViewById(R.id.ok1);
        ok2 = (TextView)findViewById(R.id.ok2);
        ok3 = (TextView)findViewById(R.id.ok3);
        ok4 = (TextView)findViewById(R.id.ok4);
        ok5 = (TextView)findViewById(R.id.ok5);
        ok6 = (TextView)findViewById(R.id.ok6);
        ok7 = (TextView)findViewById(R.id.ok7);

        ok1.setVisibility(View.GONE);
        ok2.setVisibility(View.GONE);
        ok3.setVisibility(View.GONE);
        ok4.setVisibility(View.GONE);
        ok5.setVisibility(View.GONE);
        ok6.setVisibility(View.GONE);
        ok7.setVisibility(View.GONE);

        allAnswers = new ArrayList<>();
    }

    public void pressCreateSession(View view){

        ok1.setVisibility(View.VISIBLE);

        final Activity_Session act = new Activity_Session();

        EditText name = (EditText)findViewById(R.id.editText);
        EditText description = (EditText)findViewById(R.id.editText2);
        EditText code = (EditText)findViewById(R.id.editText3);
        EditText welcomeMessage = (EditText)findViewById(R.id.editText4);

        act.setName(name.getText().toString());
        act.setDescription(description.getText().toString());
        act.setLogin_Code(code.getText().toString());
        act.setStatus(getString(R.string.estadoNuevaActividad));
        act.setWelcome_message(welcomeMessage.getText().toString());

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final Activity_Session entity = addItemInTable(act);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (entity != null) {
                                setSessionID(entity.getID());
                                ok1.setText(getString(R.string.tvok2));

                                currentTimeWorldAdmin = entity.getCreatedAt();

                                long currentTimeWorldMillis = currentTimeWorldAdmin.getTime();
                                currentDiffWithLocalTime = System.currentTimeMillis() - currentTimeWorldMillis;

                            }

                            else {ok1.setText(getString(R.string.tvok3));}
                        }
                    });
                } catch (final Exception e) {

                }
                return null;
            }
        };

        runAsyncTask(task);

    }

    public void pressCreateStages (View view) {

        ok2.setVisibility(View.VISIBLE);

        EditText sD = (EditText)findViewById(R.id.editText5);

        Date date11 = new Date();
        Date date22 = new Date();
        Date date33 = new Date();
        Date date44 = new Date();
        Date date55 = new Date();
        Date date66 = new Date();
        Date date77 = new Date();
        Date date88 = new Date();

        try {

            SimpleDateFormat dt1 = new SimpleDateFormat("hh:mm:ss");

            date11 = currentTimeWorldAdmin;
            date22 = new Date(date11.getTime() + TimeUnit.MINUTES.toMillis(Long.parseLong(sD.getText().toString())));
            date33 = new Date(date22.getTime() + TimeUnit.MILLISECONDS.toMillis(10));
            date44 = new Date(date33.getTime() + TimeUnit.MINUTES.toMillis(Long.parseLong(sD.getText().toString())));
            date55 = new Date(date44.getTime() + TimeUnit.MILLISECONDS.toMillis(10));
            date66 = new Date(date55.getTime() + TimeUnit.MINUTES.toMillis(Long.parseLong(sD.getText().toString())));
            date77 = new Date(date66.getTime() + TimeUnit.MILLISECONDS.toMillis(10));
            date88 = new Date(date77.getTime() + TimeUnit.MINUTES.toMillis(Long.parseLong(sD.getText().toString())));
        }

        catch (Exception e) {}

        final Stage s1 = new Stage();
        final Stage s2 = new Stage();
        final Stage s3 = new Stage();
        final Stage s4 = new Stage();

        s1.setActivity_id(sessionID);
        s1.setStage_Name(getString(R.string.etapa1Nombre));
        s1.setStage_Status(getString(R.string.etapa1Estado));
        s1.setStart_Date(date11);
        s1.setClose_Date(date22);
        s1.setStage_Type(getString(R.string.etapa1Tipo));
        s1.setAnswer_Type(getString(R.string.etapa1RespTipo));
        s1.setLast_Stage(false);
        s1.setInstructions(getString(R.string.etapa1Instrucciones));
        s1.setAmount_Revision_Assignments(2);
        s1.setAssignment_Status(getString(R.string.etapa1AsigEstado));
        s1.setStage_Order(1);
        s1.setMax_Grade(getString(R.string.etapa1MaxNota));

        s2.setActivity_id(sessionID);
        s2.setStage_Name(getString(R.string.etapa2Nombre));
        s2.setStage_Status(getString(R.string.etapa2Estado));
        s2.setStart_Date(date33);
        s2.setClose_Date(date44);
        s2.setStage_Type(getString(R.string.etapa2Tipo));
        s2.setAnswer_Type(getString(R.string.etapa2RespTipo));
        s2.setLast_Stage(false);
        s2.setInstructions(getString(R.string.etapa2Instrucciones));
        s2.setAmount_Revision_Assignments(2);
        s2.setAssignment_Status(getString(R.string.etapa2AsigEstado));
        s2.setStage_Order(2);
        s2.setMax_Grade(getString(R.string.etapa2MaxNota));

        s3.setActivity_id(sessionID);
        s3.setStage_Name("Etapa N°3: evaluar las retroalimentaciones recibidas");
        s3.setStage_Status("cerrado");
        s3.setStart_Date(date55);
        s3.setClose_Date(date66);
        s3.setStage_Type("revision");
        s3.setAnswer_Type("nota");
        s3.setLast_Stage(false);
        s3.setInstructions("Favor responder las siguientes preguntas sobre las retroalimentaciones recibidas");
        s3.setAmount_Revision_Assignments(2);
        s3.setAssignment_Status("NA");
        s3.setStage_Order(3);
        s3.setMax_Grade("5");

        s4.setActivity_id(sessionID);
        s4.setStage_Name("Etapa N°4: Responder nuevamente preguntas");
        s4.setStage_Status("cerrado");
        s4.setStart_Date(date77);
        s4.setClose_Date(date88);
        s4.setStage_Type("envio");
        s4.setAnswer_Type("texto");
        s4.setLast_Stage(true);
        s4.setInstructions("Favor responder las preguntas a continuación");
        s4.setAmount_Revision_Assignments(0);
        s4.setAssignment_Status("NA");
        s4.setStage_Order(4);
        s4.setMax_Grade("NA");

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final Stage entity1 = addStageInTable(s1);
                    final Stage entity2 = addStageInTable(s2);
                    final Stage entity3 = addStageInTable(s3);
                    final Stage entity4 = addStageInTable(s4);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (entity1 != null && entity2 != null && entity3 != null && entity4 != null) {
                                setStage1ID(entity1.getID());
                                setStage2ID(entity2.getID());
                                setStage3ID(entity3.getID());
                                setStage4ID(entity4.getID());

                                ok2.setText(getString(R.string.tvok2));

                            }

                            else {ok2.setText(getString(R.string.tvok3));}
                        }
                    });
                } catch (final Exception e) {
                    //createAndShowDialogFromTask(e, "Error-4");
                }
                return null;
            }
        };

        runAsyncTask(task);

    }

    public void pressCreateQuestions(View view){

        ok3.setVisibility(View.VISIBLE);

        final Question q1 = new Question();
        final Question q2 = new Question();

        EditText p1 = (EditText)findViewById(R.id.editText6);
        EditText p2 = (EditText)findViewById(R.id.editText7);

        q1.setStage_id(getStage1ID());
        q1.setActivity_id(getSessionID());
        q1.setQuestion_Order(1);
        q1.setQuestion_text(getString(R.string.Preg1) + p1.getText().toString());
        q1.setHelp_Text("Responda brevemente");

        q2.setStage_id(getStage1ID());
        q2.setActivity_id(getSessionID());
        q2.setQuestion_Order(2);
        q2.setQuestion_text(getString(R.string.Preg2) + p2.getText().toString());
        q2.setHelp_Text("Responda brevemente");

        final Question q3 = new Question();
        final Question q4 = new Question();

        q3.setStage_id(getStage2ID());
        q3.setActivity_id(getSessionID());
        q3.setQuestion_Order(1);
        q3.setQuestion_text("Por favor entregue retroalimentación y califique en una escala de 1 a 5 la respuesta de la Pregunta N°1");
        q3.setHelp_Text("Responda brevemente");

        q4.setStage_id(getStage2ID());
        q4.setActivity_id(getSessionID());
        q4.setQuestion_Order(2);
        q4.setQuestion_text("Por favor entregue retroalimentación y califique en una escala de 1 a 5 la respuesta de la Pregunta N°2");
        q4.setHelp_Text("Responda brevemente");

        final Question q5 = new Question();
        final Question q6 = new Question();

        q5.setStage_id(getStage3ID());
        q5.setActivity_id(getSessionID());
        q5.setQuestion_Order(1);
        q5.setQuestion_text("¿Es útil la retroalimentación? Calificar en una escala de 1 a 5");
        q5.setHelp_Text("");

        q6.setStage_id(getStage3ID());
        q6.setActivity_id(getSessionID());
        q6.setQuestion_Order(2);
        q6.setQuestion_text("¿Es clara la retroalimentación? Calificar en una escala de 1 a 5");
        q6.setHelp_Text("");


        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final Question entity1 = addQuestionInTable(q1);
                    final Question entity2 = addQuestionInTable(q2);
                    final Question entity3 = addQuestionInTable(q3);
                    final Question entity4 = addQuestionInTable(q4);
                    final Question entity5 = addQuestionInTable(q5);
                    final Question entity6 = addQuestionInTable(q6);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (entity1 != null & entity2!=null & entity3 != null & entity4!=null& entity5!=null& entity6!=null) {
                                ok3.setText(getString(R.string.tvok2));
                            }

                            else {ok3.setText(getString(R.string.tvok3));}
                        }
                    });
                } catch (final Exception e) {
                    //createAndShowDialogFromTask(e, "Error-4");
                }
                return null;
            }
        };

        runAsyncTask(task);

    }

    public void pressChooseActivity(View view){
        Intent intent = new Intent(this, SelectActivitySession.class);
        intent.putExtra("isAdmin", true);
        startActivity(intent);
    }

    public void pressCloseAndOpen1 (View view) {

        ok4.setVisibility(View.VISIBLE);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Stage> results = refreshItemsFromMobileServiceTableStages();

                    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                    Stage one = results.get(0);
                    Stage two = results.get(1);

                    Date now = new Date();

                    one.setClose_Date(now);
                    one.setStage_Status("cerrado");
                    two.setStart_Date(now);
                    two.setStage_Status("abierto");

                    final Stage entity1 = updateStageInTable(one);
                    final Stage entity2 = updateStageInTable(two);

                    //Offline Sync
                    //final List<ToDoItem> results = refreshItemsFromMobileServiceTableSyncTable();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(entity1!=null && entity2!=null)
                            {
                                ok4.setText(getString(R.string.tvok2));
                            }
                            else {ok4.setText(getString(R.string.tvok3));}

                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    public void pressCloseAndOpen2 (View view){

        ok6.setVisibility(View.VISIBLE);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Stage> results = refreshItemsFromMobileServiceTableStages();

                    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                    Stage two = results.get(1);
                    Stage three = results.get(2);

                    Date now = new Date();

                    two.setClose_Date(now);
                    two.setStage_Status("cerrado");
                    three.setStart_Date(now);
                    three.setStage_Status("abierto");

                    final Stage entity1 = updateStageInTable(two);
                    final Stage entity2 = updateStageInTable(three);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(entity1!=null && entity2!=null)
                            {
                                ok6.setText(getString(R.string.tvok2));
                            }
                            else {ok6.setText(getString(R.string.tvok3));}

                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    public void pressCloseAndOpen3 (View view)
    {
        ok7.setVisibility(View.VISIBLE);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Stage> results = refreshItemsFromMobileServiceTableStages();

                    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                    Stage two = results.get(2);
                    Stage three = results.get(3);

                    Date now = new Date();

                    two.setClose_Date(now);
                    two.setStage_Status("cerrado");
                    three.setStart_Date(now);
                    three.setStage_Status("abierto");

                    final Stage entity1 = updateStageInTable(two);
                    final Stage entity2 = updateStageInTable(three);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(entity1!=null && entity2!=null)
                            {
                                ok7.setText(getString(R.string.tvok2));
                            }
                            else {ok7.setText(getString(R.string.tvok3));}

                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    public void asignarRevisionesEtapa1(View view){

        ok5.setVisibility(View.VISIBLE);


        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Stage> stages = refreshItemsFromMobileServiceTableStages();

                    Stage one = stages.get(0);
                    Stage two = stages.get(1);

                    final List<Answer> results = refreshItemsFromMobileServiceTable(one.getID());

                    List<Assignment> ass = createAssignments(results, two.getAmount_Revision_Assignments(), two.getID());

                    for (Assignment a: ass){
                        addAssignmentToDB(a);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ok5.setText(getString(R.string.tvok2));

                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    private List<Stage> refreshItemsFromMobileServiceTableStages() throws ExecutionException, InterruptedException {
        return stageTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).orderBy("Stage_Order", QueryOrder.Ascending).execute().get();
    }

    private List<Answer> refreshItemsFromMobileServiceTable(String stage1ID) throws ExecutionException, InterruptedException {
        return answerTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(stage1ID).orderBy("Username", QueryOrder.Ascending).execute().get();
    }

    public List<Assignment> createAssignments(List<Answer> answersReceived, int amountRevisionAssigments, String stage2ID){

        List<String> uniqueUsernames = new ArrayList<>();
        List<Integer> assignmentsReady = new ArrayList<>();

        for(Answer a: answersReceived){
            if(!uniqueUsernames.contains(a.getUsername())) {
                uniqueUsernames.add(a.getUsername());
                assignmentsReady.add(0);
            }
        }

        int revisionsPerPerson = 0;
        if((uniqueUsernames.size()-1) < amountRevisionAssigments){
            revisionsPerPerson = uniqueUsernames.size()-1;
        }
        else {revisionsPerPerson = amountRevisionAssigments;}

        String [][] finalAssig = new String[uniqueUsernames.size()][revisionsPerPerson];

        for (int i = 0;i<uniqueUsernames.size();i++){
            int currentUser = i+1;

            for (int j = 0;j < revisionsPerPerson;j++){
                if(currentUser == i) {
                    currentUser++;
                }

                if(currentUser >= uniqueUsernames.size()){
                    currentUser = 0;

                    if(currentUser == i) {
                        currentUser++;
                    }
                }

                finalAssig[i][j] = uniqueUsernames.get(currentUser);

                currentUser++;
            }
        }

        List<Assignment> results = new ArrayList<>();
        for(int i = 0; i< finalAssig.length; i++){
            for (int j = 0; j< finalAssig[0].length; j++){
                Assignment r = new Assignment();
                r.setActivity_id(SessionCode.finalActivitySessionID);
                r.setStage_id(stage2ID);
                r.setUsername_Reviser(uniqueUsernames.get(i));
                r.setUsername_Reviewed(finalAssig[i][j]);
                r.setAssignment_Status("pendiente");

                results.add(r);
            }
        }

        return results;

    }

    public Assignment addAssignmentToDB(Assignment item)throws Exception{
        Assignment entity = assignmentTable.insert(item).get();
        return entity;
    }

    public void assignRev(){

        ok5.setText("ok3");
        String resp1 = allAnswers.get(0).getAnswer_text();
        int i =1;
        int j = 2;
        ok5.setText("ok5");

    }

    public Activity_Session addItemInTable(Activity_Session act) throws Exception {
        Activity_Session entity = activity_sessionTable.insert(act).get();
        return entity;
    }

    public Stage addStageInTable(Stage stage) throws Exception {
        Stage entity = stageTable.insert(stage).get();
        return entity;
    }

    public Question addQuestionInTable(Question q)throws Exception {
        Question entity = questionTable.insert(q).get();
        return entity;
    }

    public Stage updateStageInTable(Stage s) throws Exception{
        Stage entity = stageTable.update(s).get();
        return entity;
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    public void setSessionID(String id) {this.sessionID = id;}
    public String getSessionID () {return sessionID;}

    public void setStage1ID(String id){this.stage1ID = id;}
    public String getStage1ID () {return stage1ID;}

    public void setStage2ID(String id){this.stage2ID = id;}
    public String getStage2ID () {return stage2ID;}

    public void setStage3ID(String id){this.stage3ID = id;}
    public String getStage3ID () {return stage3ID;}

    public void setStage4ID(String id){this.stage4ID = id;}
    public String getStage4ID () {return stage4ID;}


}
