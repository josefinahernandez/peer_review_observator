package com.example.peerreviewciae;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniela on 14-12-2016.
 */
public class PeerReviewDB extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "PeerReview";
    private static final String LOG_TAG = PeerReviewDB.class.getName();

    private static final String observationTable =
            "CREATE TABLE observation (_id integer primary key, time TIMESTAMP NOT NULL, observation_template TEXT, code TEXT, observation_code long)";

    private static final String observationsDoneTable =
            "CREATE TABLE observation_done (_id integer primary key, time TIMESTAMP NOT NULL, observation_template TEXT, id_session TEXT, id_user TEXT)";

    private static final String photosTable =
            "CREATE TABLE photos (_id integer primary key, time TIMESTAMP NOT NULL, observation_code long, observation_template TEXT, observation_item TEXT, path TEXT, comment TEXT)";

    private static final String commentsTable =
            "CREATE TABLE comments (_id integer primary key, time TIMESTAMP NOT NULL, observation_code long, observation_template TEXT, observation_item TEXT, comment TEXT)";

    PeerReviewDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "CREATING DATABASE");
        db.execSQL(observationTable);
        db.execSQL(observationsDoneTable);
        db.execSQL(photosTable);
        db.execSQL(commentsTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS observation_done");
        db.execSQL("DROP TABLE IF EXISTS observation");
        db.execSQL("DROP TABLE IF EXISTS photos");
        db.execSQL("DROP TABLE IF EXISTS comments");
        onCreate(db);
    }

    /*
    Retorna la lista de observaciones realizadas por el usuario
     */
    public List<String> getObservationsDone(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM observation_done";
        List<String> ret = new ArrayList<>();
        Log.e(LOG_TAG,selectQuery);
        Cursor c = db.rawQuery(selectQuery,null);

        if (c.moveToFirst()){
            do {
                ret.add(Integer.toString(c.getColumnIndex("_id"))+c.getColumnIndex("time")+c.getColumnIndex("observation_template"));
            }while(c.moveToNext());
        }
        return ret;
    }
}
