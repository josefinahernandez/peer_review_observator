package com.example.peerreviewciae;

import java.util.Date;

/**
 * Created by Josefina on 13-09-2016.
 */
public class Answer {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("updatedAt")
    private Date updatedAt;

    @com.google.gson.annotations.SerializedName("Activity_id")
    private String Activity_id;

    @com.google.gson.annotations.SerializedName("Stage_id")
    private String Stage_id;

    @com.google.gson.annotations.SerializedName("Question_id")
    private String Question_id;

    @com.google.gson.annotations.SerializedName("Username")
    private String Username;

    @com.google.gson.annotations.SerializedName("Answer_text")
    private String Answer_text;


    public Answer(){

    }

    public Answer(String id){
        this.setId(id);
    }

    public Date getUpdatedAt () {return updatedAt;}

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}
    public String getStage_id() {return Stage_id;}
    public void setStage_id(String desc) {this.Stage_id= desc;}
    public String getQuestion_id() {return Question_id;}
    public void setQuestion_id(String qID) {this.Question_id= qID;}
    public String getUsername() {return Username;}
    public void setUsername(String name) {this.Username= name;}
    public String getAnswer_text(){return Answer_text;}
    public void setAnswer_text(String at){this.Answer_text = at;}
    public String getActivity_id(){return Activity_id;}
    public void setActivity_id(String at){this.Activity_id= at;}

}
