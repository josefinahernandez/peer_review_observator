package com.example.peerreviewciae;

/**
 * Esta clase no se usa! Fue creada siguiendo el formato del sistema anterior
 */
public class Menu_Access {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Menu_id")
    private String Menu_id;

    @com.google.gson.annotations.SerializedName("Rol_id")
    private String Rol_id;



    public Menu_Access(){

    }

    public Menu_Access(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}

    public String getMenu_id() {return Menu_id;}
    public void setMenu_id(String mn) {this.Menu_id= mn;}

    public String getRol_id(){return Rol_id;}
    public void setRol_id(String mn){this.Rol_id= mn;}


}
