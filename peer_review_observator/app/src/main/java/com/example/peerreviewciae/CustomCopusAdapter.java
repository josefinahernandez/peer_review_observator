package com.example.peerreviewciae;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Daniela on 20-07-2016.
 */
public class CustomCopusAdapter extends RecyclerView.Adapter<CustomCopusAdapter.MyViewHolder> {

    private List<COPUSData> items = new ArrayList<>();
    private Context context;
    COPUSRegister mr = COPUSRegister.getInstance();
    private static final String LOG_TAG = CustomCopusAdapter.class.getName();

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        Button startEndButton;
        Button endButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.startEndButton = (Button) itemView.findViewById(R.id.start_end_copus_bttn);
        }
    }

    public CustomCopusAdapter(Context context, List<COPUSData> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.copus_item_list, parent, false);

        mr.setCOPUSRegisterListener(new COPUSRegister.COPUSRegisterListener() {
            @Override
            public void onObservationStarted(boolean isStarted) {
                Log.d(LOG_TAG,""+isStarted);

                for(COPUSData d:items){
                    d.isObservationRecording();
                }
            }
        });

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        Log.d("Llamando onBVH Position",""+position);
        int pos = position;
        final Button startEndButton = holder.startEndButton;
        startEndButton.setText(items.get(position).getDescription());
        startEndButton.setEnabled(true);
        startEndButton.setTextColor(ContextCompat.getColor(context,R.color.ciae_dark));

        startEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (items.get(holder.getLayoutPosition()).isRecording()){
                    mr.put(Calendar.getInstance().getTimeInMillis(),items.get(holder.getLayoutPosition()).getDescriptionCode()+"-"+items.get(holder.getLayoutPosition()).COPUS_ITEM_END);
                    items.get(holder.getLayoutPosition()).setNotRecording();
                    startEndButton.setBackgroundColor(ContextCompat.getColor(context,R.color.white));
                }else{
                    mr.put(Calendar.getInstance().getTimeInMillis(),items.get(holder.getLayoutPosition()).getDescriptionCode()+"-"+items.get(holder.getLayoutPosition()).COPUS_ITEM_START);
                    items.get(holder.getLayoutPosition()).setIsRecording();
                    startEndButton.setBackgroundColor(ContextCompat.getColor(context,R.color.lightRed));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
