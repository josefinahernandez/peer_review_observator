package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Clase de tipo "Adapter" que muestra una lista respuestas en forma de Textview
 */
public class AnswerAdapter extends ArrayAdapter<Answer>{

    Context mcontext;

    int mlayoutResourceId;

    public AnswerAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mcontext = context;
        mlayoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final Answer currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(mlayoutResourceId, parent, false);
        }

        row.setTag(currentItem);

        //Muestra las respuestas que correspondan en formato de Textview
        final TextView textView= (TextView) row.findViewById(R.id.answer_textview);
        textView.setText(currentItem.getAnswer_text());
        textView.setEnabled(true);

        return row;
    }
}
