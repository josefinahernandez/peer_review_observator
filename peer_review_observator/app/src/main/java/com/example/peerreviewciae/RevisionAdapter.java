package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

/**
 * Clase tipo "Adapter" que muestra las revisiones en forma de botón para la etapa 3
 */
public class RevisionAdapter extends ArrayAdapter<Revision> {

    Context mcontext;

    int mlayoutResourceId;

    public RevisionAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mcontext = context;
        mlayoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final Revision currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(mlayoutResourceId, parent, false);
        }

        row.setTag(currentItem);

        //Creamos el botón para cada revisión
        final Button button = (Button) row.findViewById(R.id.revision_button);

        //Si el estado es Nulo o Pendiente, entonces el texto del botón debe indicar que la revisión no ha sido revisada
        if(currentItem.getRevision_Status() == null) {
            button.setText(mcontext.getString(R.string.RevAd_button1) + Integer.toString(position+1));
        }
        else if (currentItem.getRevision_Status().equals(mcontext.getString(R.string.estadoRevisionPendiente))) {
            button.setText(mcontext.getString(R.string.RevAd_button1) + Integer.toString(position+1));
        }
        //Si el estado es que ya fue revisado, entonces el texto del botón debe indicar dar la opción de EDITAR la revisión.
        else {
            button.setText(mcontext.getString(R.string.RevAd_button2) + Integer.toString(position+1) ); //
        }
        button.setEnabled(true);

        /**
         * Al apretar el botón de la revisión, envía los siguientes parámetros:
         * 1- el nombre de usuario del revisor
         * 2- la posición de la revisión en la lista de revisiones, para saber cuál abrir.
         */
        button.setOnClickListener(new View.OnClickListener() {

                                      @Override
                                      public void onClick(View arg0) {
                                          if (mcontext instanceof stage3) {
                                              stage3 activityChosen = (stage3) mcontext;
                                              activityChosen.goToRevision(currentItem.getUsername_Reviser(), position);
                                          }
                                      }
                                  }
        );

        return row;
    }
}
