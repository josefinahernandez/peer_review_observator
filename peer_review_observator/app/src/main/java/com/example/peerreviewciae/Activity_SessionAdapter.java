package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;

/**
 * Clase de tipo "Adapter" que muestra la lista de todas las actividades que tiene el sistema en la BD en forma de botones
 */
public class Activity_SessionAdapter extends ArrayAdapter<Activity_Session> {

    Context mcontext;

    int mlayoutResourceId;

    public Activity_SessionAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mcontext = context;
        mlayoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final Activity_Session currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(mlayoutResourceId, parent, false);
        }

        row.setTag(currentItem);

        //Crear el botón correspondiente, setear el nombre y habilitarlo.
        final Button button = (Button) row.findViewById(R.id.session_button);
        button.setText(currentItem.getName());
        button.setEnabled(true);

        /**Crear un OnClickListener para que, al apretar, se vaya a la actividad correspondiente.
         Envía los siguientes parámetros:
         1- ID de la actividad
         2- Nombre de la actividad
         3- Descripción
         4- Código
         **/
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mcontext instanceof SelectActivitySession) {
                    SelectActivitySession activityChosen = (SelectActivitySession) mcontext;
                    activityChosen.goToActivity(currentItem.getID(), currentItem.getName(), currentItem.getDescription(), currentItem.getLogin_Code());
                }
            }
        }
        );

        return row;
    }



}