package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.xml.sax.helpers.LocatorImpl;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class StagesGrid extends AppCompatActivity {

    public static Date currentTimeWorld;
    public static long currentDiffWithLocalTime;

    private SwipeRefreshLayout swipeContainer;

    String activeStageID;

    String activitySessionID;
    String activitySessionName;
    String activitySessionDescription;
    String activitySessionWelcome;

    public static String stage1ID;
    public static String stage2ID;
    public static String stage3ID;
    public static String stage4ID;

    private MobileServiceClient mClient;

    private MobileServiceTable<Stage> stageTable;
    private StageAdapter stageAdapter;

    private MobileServiceTable<Activity_Session>activity_sessionTable;
    private MobileServiceTable<User_Activity>user_activityTable;

    TextView actName;
    TextView actDescrip;

    String actNameS ="";
    String actDescS ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stages_grid);

        //Menú de la franja superior
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Método para actualizar la página deslizando hacia abajo.
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //ID de la actividad
        activitySessionID = SessionCode.finalActivitySessionID;

        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //Tabla de etapas
            stageTable= mClient.getTable(Stage.class);
            //Tabla de usuarios inscritos en las actividades
            user_activityTable = mClient.getTable(User_Activity.class);
            //Tabla de actividades
            activity_sessionTable = mClient.getTable(Activity_Session.class);

            //Textview que muestra el nombre de la actividad
            actName = (TextView)findViewById(R.id.actName_textView);
            //Textview que muestra la descripción de la actividad
            actDescrip  = (TextView)findViewById(R.id.actDescrip_textView);

            //Si es primera vez que el usuario entra a esta actividad, se agrega el dato correspondiente en la BD
            if (getIntent().getBooleanExtra("requiresSessionAdd", false)){
                this.addUser_Activity();
            }

            //El Adapter que despliega la información.
            stageAdapter= new StageAdapter(this,R.layout.stage_list, this);
            ListView lv= (ListView) findViewById(R.id.listStagesOfSession);
            lv.setAdapter(stageAdapter);


        } catch (MalformedURLException e) {
            //createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
        } catch (Exception e){
            //createAndShowDialog(e, "Error-here");
        }

        //Método que se encarga de buscar la información de las etapas y mostrarlas en la vista
        refreshItemsFromTable();

        //Método que busca la información de la actividad en la BD para mostrar
        refreshItemsFromActivitySessionTable();

        //Para actualizar al deslizar.
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                RefreshPage();

            }
        });

    }

    //Método que se encarga de buscar la información de las etapas y mostrarlas en la vista
    private void refreshItemsFromTable() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //Buscamos en la BD las 4 etapas
                    final List<Stage> results = refreshItemsFromMobileServiceTableStage();

                    //Guardamos los ID porque se necesitan muchas veces entonces para tener un acceso más fácil.
                    stage1ID = results.get(0).getID();
                    stage2ID = results.get(1).getID();
                    stage3ID = results.get(2).getID();
                    stage4ID = results.get(3).getID();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stageAdapter.clear();

                            //Agregamos las 4 etapas al Adapter
                            for (Stage item : results) {
                                stageAdapter.add(item);
                            }
                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    //Método que busca en la BD las etapas de esta actividad
    private List<Stage> refreshItemsFromMobileServiceTableStage() throws ExecutionException, InterruptedException {
        return stageTable.where().field("Activity_id").eq(activitySessionID).orderBy("Stage_Order", QueryOrder.Ascending).execute().get();
    }

    //Método que busca la información de la actividad en la BD para mostrar
    private void refreshItemsFromActivitySessionTable() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {

                    //Buscamos en la BD la actividad actual
                    final List<Activity_Session> results = refreshItemsFromMobileServiceTableActivity();

                    //sacamos su nombre, descripción y mensaje de bienvenida
                    activitySessionName = results.get(0).getName();
                    activitySessionDescription = results.get(0).getDescription();
                    activitySessionWelcome = results.get(0).getWelcome_message();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //Seteamos los textos de los textview correspondientes con la información de la actividad
                            actName.setText(activitySessionName);
                            actDescrip.setText(activitySessionDescription + ": " + activitySessionWelcome);
                        }
                    });
                } catch (final Exception e){

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    //Búsqueda en la BD de la Actividad Actual.
    private List<Activity_Session> refreshItemsFromMobileServiceTableActivity() throws ExecutionException, InterruptedException {
        return activity_sessionTable.where().field("ID").eq(activitySessionID).execute().get();
    }

    /**
     * Si el usuario no ha ingresado previamente a esta actividad, se debe agregar esta información en la tabla
     * "User_Activity" de la BD, para tener un registro de todos quienes participaron de cada actividad.
     */
    public void addUser_Activity(){

        //Primero crear el objeto de tipo "User_Activity"
        final User_Activity usAc = new User_Activity();

        //información del usuario
        user u = Login.userLoggedIn;

        //setear la información correspondiente
        usAc.setActivity_id(activitySessionID);
        usAc.setUsername(u.getUsername());

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    //Agregar el objeto a la BD
                    final User_Activity entity = addItemInTable(usAc);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (entity != null) {

                                /**Acá aprovechamos de obtener la diferencia horaria entre la hr local verdadera
                                 * y la hr del dispositivo. Con esta información después se muestran los horarios de apertura
                                 * y cierre de las etapas y se calcula el tiempo del cronómetro de cada etapa.
                                  */
                                currentTimeWorld = entity.getCreatedAt();
                                long currentTimeWorldMillis = currentTimeWorld.getTime();
                                currentDiffWithLocalTime = System.currentTimeMillis() - currentTimeWorldMillis;
                            }
                        }
                    });
                } catch (final Exception e) {

                }
                return null;
            }
        };

        runAsyncTask(task);


    }

    //método que agrega a la BD
    public User_Activity addItemInTable(User_Activity u) throws ExecutionException, InterruptedException {
        User_Activity entity = user_activityTable.insert(u).get();
        return entity;
    }

    //Método que se llama cuando el usuario selecciona una etapa a la que desea ir. Los parámetros son enviados del Adapter (StageAdapter)
    public void goToActivity(Stage stage){

        int order = stage.getStage_Order();

        //Se crea el intent con la clase correspondiente
        Intent intent;
        if(order == 1){
            intent = new Intent(this, stage1.class);
        }
        else if (order == 2) {
            intent = new Intent(this, stage2.class);
        }
        else if (order == 3)
        {
            intent = new Intent(this, stage3.class);
        }
        else {
            intent = new Intent(this, stage4.class);
        }

        intent.putExtra("stageID",stage.getID());
        intent.putExtra("stageName", stage.getStage_Name());
        intent.putExtra("stageInstructions", stage.getInstructions());

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        String dateStringOpen = df.format(stage.getStart_Date());
        String dateStringClose = df.format(stage.getClose_Date_Date());

        intent.putExtra("stageOpen", dateStringOpen);
        intent.putExtra("stageClose",dateStringClose);

        this.finish();
        startActivity(intent);

    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    //Al refrescar la página deslizando, se llama a este método que es igual al OnCreate de arriba
    private void RefreshPage() {
        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            stageTable= mClient.getTable(Stage.class);
            user_activityTable = mClient.getTable(User_Activity.class);
            activity_sessionTable = mClient.getTable(Activity_Session.class);

            stageAdapter= new StageAdapter(this,R.layout.stage_list, this);
            ListView lv= (ListView) findViewById(R.id.listStagesOfSession);
            lv.setAdapter(stageAdapter);

            actName = (TextView)findViewById(R.id.actName_textView);
            actDescrip  = (TextView)findViewById(R.id.actDescrip_textView);

            refreshItemsFromTable();

            refreshItemsFromActivitySessionTable();


        } catch (MalformedURLException e) {

        } catch (Exception e){

        }

        swipeContainer.setRefreshing(false);
    }

    /**
     * Método que controla los círculos de arriba de la vista que muestra en qué etapa se está actualmente
     * Las 5 etapas que se muestran son en realidad RadioButtons activos e inactivos según corresponda.
     */
    public void setActiveStageID(String activeStageID){

        this.activeStageID = activeStageID;

        RadioButton s1 = (RadioButton)findViewById(R.id.et1);
        RadioButton s2 = (RadioButton)findViewById(R.id.et2);
        RadioButton s3 = (RadioButton)findViewById(R.id.et3);
        RadioButton s4 = (RadioButton)findViewById(R.id.et4);

        if(activeStageID.equals(stage1ID))
        {
            s1.setChecked(true);
            s1.setEnabled(true);

            s2.setChecked(false);
            s2.setEnabled(false);

            s3.setChecked(false);
            s3.setEnabled(false);

            s4.setChecked(false);
            s4.setEnabled(false);
        }
        else if(activeStageID.equals(stage2ID))
        {
            s1.setChecked(false);
            s1.setEnabled(false);

            s2.setChecked(true);
            s2.setEnabled(true);

            s3.setChecked(false);
            s3.setEnabled(false);

            s4.setChecked(false);
            s4.setEnabled(false);
        }
        else if(activeStageID.equals(stage3ID))
        {
            s1.setChecked(false);
            s1.setEnabled(false);

            s2.setChecked(false);
            s2.setEnabled(false);

            s3.setChecked(true);
            s3.setEnabled(true);

            s4.setChecked(false);
            s4.setEnabled(false);
        }
        else if(activeStageID.equals(stage4ID))
        {
            s1.setChecked(false);
            s1.setEnabled(false);

            s2.setChecked(false);
            s2.setEnabled(false);

            s3.setChecked(false);
            s3.setEnabled(false);

            s4.setChecked(true);
            s4.setEnabled(true);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_peer_review, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle("Bienvenido " + Login.userNameLoggedIn);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {

            return true;
        }

        else if (id == R.id.changeActivity){

            Intent intent = new Intent(this, SelectActivitySession.class);
            this.finish();
            startActivity(intent);

            return true;
        }

        else if (id == R.id.observations){

            Intent intent = new Intent(this, ObserverMain.class);
            this.finish();
            startActivity(intent);

            return true;
        }



        else if (id == R.id.closeSession){

            Intent intent = new Intent(this, Login.class);
            Login.userLoggedIn = null;
            this.finish();
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }
}
