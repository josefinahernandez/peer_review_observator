package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

/**
 * Clase de tipo "Adapter" que muestra una lista de asignaciones en forma de Botón
 */
public class AssignmentAdapter extends ArrayAdapter<Assignment> {

    Context mcontext;

    int mlayoutResourceId;

    public AssignmentAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mcontext = context;
        mlayoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(mlayoutResourceId, parent, false);
        }

        final Assignment currentItem = getItem(position);

        row.setTag(currentItem);

        //Crear el botón
        final Button button = (Button) row.findViewById(R.id.assigment_button);

        //Si el estado de la asignación de la revisión es "Pendiente", el texto será diferente al de la asignación que ya fue revisada

        //Enumeramos los botones de las asignaciones para que el usuario los vea ordenados
        if(currentItem.getAssignment_Status().equals(mcontext.getString(R.string.estadoAssignmentPendiente))) {
            button.setText(mcontext.getString(R.string.AssAd_button1) + Integer.toString(position+1));
        }
        else{
            button.setText(mcontext.getString(R.string.AssAd_button2) + Integer.toString(position+1));
        }
        button.setEnabled(true);

        /**
         * Al apretar el botón, se envían los siguientes parámetros:
         * 1- username del usuario REVISADO
         * 2- estado de la asignación
         */

        button.setOnClickListener(new View.OnClickListener() {

                                      @Override
                                      public void onClick(View arg0) {
                                          if (mcontext instanceof stage2) {
                                              stage2 activityChosen = (stage2) mcontext;
                                              activityChosen.goToAssignedRevision(currentItem.getUsername_Reviewed(), currentItem.getAssignment_Status());
                                          }
                                      }
                                  }
        );

        return row;
    }
}
