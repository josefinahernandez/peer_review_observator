package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.squareup.okhttp.OkHttpClient;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class stage1 extends AppCompatActivity {

    private MobileServiceClient mClient;

    private MobileServiceTable<Question> questionTable;
    private MobileServiceTable<Answer> answerTable;

    String sName;
    String sInstructions;
    String sOpen;
    String sClose;
    public static String currentStage1ID;

    TextView nameTV;
    TextView insTV;
    TextView openTV;
    TextView closeTV;
    TextView q1TV;
    TextView q2TV;
    Button send;
    EditText q1In;
    EditText q2In;
    TextView chroneExplain;
    TextView chrone;
    TextView okTV;

    List<EditText> answers;
    List<TextView> questionsTV;
    List<Question> questionsElement;

    /**
     * Esta clase maneja la Etapa 1.
     * Todas las vistas de etapas tienen 2 partes, separadas por una línea gruesa:
     * 1 - Lo llamaremos "Información General" (IG)
     * 2 - Lo llamaremos "Desarrollo de la etapa" (DE)
     *
     * La IG muestra el nombre de la etapa, las instrucciones, hora de apertura, hora de cierre y tiempo que queda para el cierre
     * El DE varía según cada etapa. En este caso, muestra dos preguntas (Textview) y un espacio para responder cada una (Edittext).
     * El sistema solamente permite 2 preguntas.
     * Luego, está el botón de envío, que cambia a "Editar" cuando ya se envió una respuesta.
     *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_stage1);

        //menú superior (franja azul)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**Seteando la IG. Para no ir a buscarla a la BD en cada etapa y que sea más rápido el sistema, se envía siempre a través
         * un Intent de la vista anterior.
        **/

        //nombre de la etapa
        sName = getIntent().getStringExtra("stageName");
        //instrucciones de la etapa
        sInstructions = getIntent().getStringExtra("stageInstructions");
        //fecha de apertura de la etapa
        sOpen = getIntent().getStringExtra("stageOpen");
        //fecha de cierre de la etapa
        sClose = getIntent().getStringExtra("stageClose");
        //ID de esta etapa, sirve para ir a buscar datos a la BD
        currentStage1ID = getIntent().getStringExtra("stageID");

        //El Textview que muestra el nombre de la etapa
        nameTV = (TextView) findViewById(R.id.sName);
        //El Textview que muestra las instrucciones de la etapa
        insTV = (TextView) findViewById(R.id.sInstructions);
        //El Textview que muestra la fecha y hora de apertura de la etapa
        openTV = (TextView) findViewById(R.id.sOpen);
        //El Textview que muestra la fecha y hora de cierre de la etapa
        closeTV = (TextView) findViewById(R.id.sClose);
        //El Textview que muestra lo que es el cronómetro ("El tiempo restante al cierre de la etapa es...")
        chroneExplain = (TextView) findViewById(R.id.chone_explain);
        //El Textview que muestra el cronómetro
        chrone = (TextView) findViewById(R.id.chronometer);

        //seteamos los textview con la información correspondiente.
        nameTV.setText(sName);
        insTV.setText(sInstructions);
        openTV.setText(getString(R.string.FechaAperturaTV) + sOpen);
        closeTV.setText(getString(R.string.FechaCierreTV) + sClose);
        chroneExplain.setText(getString(R.string.CronometroTV));

        /**
         * Ahora seteamos el DE.
         * Necesitamos ir a buscar a la BD dos datos:
         * 1 - Las preguntas,
         * 2 - Las respuestas previas que puede haber enviado el usuario y que quiera editar.
         */

        //Textview que muestra la Pregunta N°1 de la etapa
        q1TV = (TextView) findViewById(R.id.q1_tv);
        //Textview que muestra la Pregunta N°2 de la etapa
        q2TV = (TextView) findViewById(R.id.q2_tv);
        //Edittext donde el usuario responderá la Pregunta N°1 de la etapa
        q1In = (EditText) findViewById(R.id.q1_input);
        //Edittext donde el usuario responderá la Pregunta N°2 de la etapa
        q2In = (EditText) findViewById(R.id.q2_input);
        //Botón de envío de las respuestas
        send = (Button) findViewById(R.id.send_button);
        //Textview que si las respuestas fueron guardadas exitosamente en la Base de Datos o no.
        // Además avisa si alguna de las respuestas fueron enviadas en blanco (que no está permitido)
        okTV = (TextView) findViewById(R.id.okTV);

        okTV.setVisibility(View.GONE);

        /**Las siguientes tres listas fueron creadas pensando en que a futuro la aplicación podría permitir
         * más de 2 preguntas. Actualmente no lo permite por lo que no sirven tanto, pero quedaron por si se
         * se decidiera extender más adelante.
         */

        //Lista de los textview de las preguntas
        questionsTV = new ArrayList<TextView>();
        //Lista de las preguntas como objetos
        questionsElement = new ArrayList<Question>();
        //Lista de los Edittext con las respuestas
        answers = new ArrayList<EditText>();

        answers.add(q1In);
        answers.add(q2In);

        //Este método es para el cronómetro. Está explicado más abajo
        //Aquí se echa a andar el cronómetro.
        long diff = getTimeinMilliseconds();
        new CountDownTimer(diff, 1000) {

            public void onTick(long millisUntilFinished) {

                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));

                //Para que se muestre rojo y haga ruido si quedan menos de 5 minutos. Después hay que cambiarlo al tiempo que queramos.
                if((millisUntilFinished/1000 < 300) & (millisUntilFinished/1000 > 1))
                {
                    chrone.setTextColor(Color.parseColor("#FF0000"));
                    chrone.playSoundEffect(SoundEffectConstants.CLICK);
                }
                chrone.setText("" + hms);
            }

            public void onFinish() {
            }
        }.start();


        //Aquí nos conectamos a la BD. Este es código estándar de Azure
        try {

            // Create the Mobile Service Client instance, using the provided
            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //una vez realizada la conexión, buscamos las tablas.
            answerTable = mClient.getTable(Answer.class);
            questionTable = mClient.getTable(Question.class);

            //llamamos al método que va a buscar las preguntas que necesitamos a la BD y las muestra en la vista
            refreshItemsFromTableQuestions();

            //llamamos al método que va a buscar las respuestas que necesitamos a la BD y las muestra en la vista
            refreshItemsFromPreviousAnswers();

        } catch (MalformedURLException e) {

        } catch (Exception e) {

        }

    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     */
    private void refreshItemsFromTableQuestions() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {

                    //Consulta a la BD es en otro método.
                    //results es una lista con las 2 preguntas que necesitamos mostrar, en el orden que las queremos.
                    final List<Question> results = refreshItemsFromMobileServiceTableQuestions();

                    //En el thread de la Interfaz Usuario (UI), trabajamos la vista y seteamos el texto de q1TV y q2TV
                    // para que muestren las preguntas.
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //chequeamos que hayan llegado en el orden correcto y las mostramos:
                            if(results.get(0).getQuestion_Order()<results.get(1).getQuestion_Order()) {
                                q1TV.setText(results.get(0).getQuestion_text());
                                q2TV.setText(results.get(1).getQuestion_text());
                            }

                            else
                            {
                                q2TV.setText(results.get(0).getQuestion_text());
                                q1TV.setText(results.get(1).getQuestion_text());
                            }
                        }
                    });
                } catch (final Exception e) {
                    //aquí falta mostrar un mensaje si no hay internet
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /** Método que hace consultas a la BD.
     * Necesitamos las preguntas asociadas a:
     * 1 - esta actividad
     * 2 - esta etapa
     * y las pedimos en orden ascendiente según el parámetro "question_order"
    **/
    private List<Question> refreshItemsFromMobileServiceTableQuestions() throws ExecutionException, InterruptedException, MobileServiceException {

        List<Question> qq = questionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage1ID).orderBy("Question_Order", QueryOrder.Ascending).execute().get();
        return qq;
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista. Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * Acá buscamos si el usuario había respondido previamente las preguntas para mostrar las respuestas que había enviado.
     * Puede haber respondido más de una vez, y queda guardado en la BD, con distinta fecha de creación. Necesitamos entonces
     * mostrar las últimas respuestas enviadas.
     * Sigue la misma estructura que el método anterior, con threads.
     */
    private void refreshItemsFromPreviousAnswers(){

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //hacemos la consulta en la BD, que retorna TODAS las respuestas que ha enviado este usuario a estas preguntas.
                    final List<Answer> prevAns = obtainItemsFromMobileServiceTableAnswers();

                    /**
                     * Cuando un usuario envía una respuesta, se guarda, en la columna "ID", el número de esa pregunta
                     * (si era la pregunta 0 o 1).
                     * Inicialmente la idea era guardar el ID de la pregunta pero me equivoqué y quedó así, y sirve igual.
                     */
                    String q1ID = "0";
                    String q2ID = "1";

                    //Las respuestas finales que tenemos que buscar quedarán guardadas en estas variables:
                    Answer a1 = new Answer();
                    Answer a2 = new Answer();

                    //no estoy segura qué hace este for, parece que no sirve y no lo borré, porque después lo sobre-escribo.
                    for(int i =0; i<prevAns.size();i++)
                    {
                        if(prevAns.get(i).getQuestion_id().equals(q1ID)){
                            a1 = prevAns.get(i);
                        }
                        else if (prevAns.get(i).getQuestion_id().equals(q2ID)){
                            a2 = prevAns.get(i);
                        }
                    }

                    //Acá separamos la lista prevAns en dos: todas las respuestas que tenemos de la pregunta 1 y todas las de la pregunta 2.
                    //Luego, para cada pregunta, buscamos la última respuesta recibida, y la guardamos en a1 y a2.
                    for(int i=0; i<prevAns.size();i++) {

                        //Para Q1:
                        if (prevAns.get(i).getQuestion_id().equals(q1ID)) {

                            Date updated1 = a1.getUpdatedAt();
                            Date updated2 = prevAns.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                a1 = prevAns.get(i);
                            }
                        }

                        //si estamos en Q2
                        else if (prevAns.get(i).getQuestion_id().equals(q2ID)) {

                            Date updated1 = a2.getUpdatedAt();
                            Date updated2 = prevAns.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                a2 = prevAns.get(i);
                            }
                        }
                    }

                    //las copiamos a aa1 y aa2, porque para usarlas en UI thread tienen que ser declaradas como variables FINALES.
                    //Luego, las mostramos en q1In y q2In, las deshabilitamos y cambiamos el botón de envío por botón para editar.
                    final Answer aa1 = a1;
                    final Answer aa2 = a2;

                    runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String alpha = aa1.getAnswer_text();
                                String beta = aa2.getAnswer_text();

                                if(alpha!= null) {

                                    send.setText(getString(R.string.BotonEnvioParaEditar));

                                    q1In.setText(alpha);
                                    q2In.setText(beta);

                                    q1In.setEnabled(false);
                                    q2In.setEnabled(false);
                                }
                            }
                        });


                } catch (Exception e){

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /** Método que hace consultas a la BD.
     * Necesitamos las respuestas asociadas a:
     * 0 - este usuario
     * 1 - esta actividad
     * 2 - esta etapa
     * y las pedimos en orden ascendiente según el parámetro "question_order"
     **/
    private List<Answer> obtainItemsFromMobileServiceTableAnswers()throws ExecutionException, InterruptedException{
        List<Answer> a = answerTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(currentStage1ID).and().field("Username").eq(Login.userLoggedIn.getUsername()).orderBy("Question_id", QueryOrder.Ascending).execute().get();
        return a;
    }

    //método que guarda las respuestas recibidas o abre la opción de editar, según sea el caso.
    //Tiene una estructura rígida de Azure, con threads.
    public void onSend(View view) {
        if (mClient == null) {
            return;
        }

        //si se están enviando respuestas nuevas:
        if(send.getText().equals(getString(R.string.BotonEnviar))) {

            //answers es una lista con los dos Edittext
            for (int i = 0; i < answers.size(); i++) {

                //creamos la primera respuesta y la agregamos a la BD
                final Answer an = new Answer();

                an.setActivity_id(SessionCode.finalActivitySessionID);
                an.setStage_id(currentStage1ID);
                an.setQuestion_id(Integer.toString(i));
                an.setUsername(Login.userLoggedIn.getUsername());
                an.setAnswer_text(answers.get(i).getText().toString());

                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            //método que agrega un elemento a la BD
                           final Answer entity = addItemInTable(an);

                        } catch (final Exception e) {

                        }
                        return null;
                    }
                };

                runAsyncTask(task);

            }

            //avisamos al usuario que se guardaron sus respuestas y dejamos la opción de editarlas.
            okTV.setVisibility(View.VISIBLE);
            send.setText(getString(R.string.BotonEnvioParaEditar));
            answers.get(0).setEnabled(false);
            answers.get(1).setEnabled(false);
        }

        //Si el botón decía inicialmente "Editar respuestas anteriores", dejamos la opción para editarlas.
        else{
            answers.get(0).setEnabled(true);
            answers.get(1).setEnabled(true);
            send.setText(getString(R.string.BotonEnviar));
        }
    }

    //método que inserta un elemento en la BD.
    public Answer addItemInTable(Answer u) throws ExecutionException, InterruptedException {
        Answer entity = answerTable.insert(u).get();
        return entity;
    }

    //al hacer click en "Volver", te retorna a la Stage_Grid.
    public void onClickListo(View view){

        chrone.setSoundEffectsEnabled(false);

        Intent intent = new Intent(this, StagesGrid.class);

        this.finish();

        startActivity(intent);
    }

    //método default de Azure.
    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    /** método para el cronómetro.
     * Calcula la diferencia de tiempo entre la hr de cierre de la etapa y la hora actual.
     *
     */

    public long getTimeinMilliseconds() {

        try {
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date date = (Date) formatter.parse(sClose);

            Calendar today = Calendar.getInstance();
            long milisClose = date.getTime();
            long milisNow = today.getTimeInMillis();

            long diff = milisClose - milisNow + StagesGrid.currentDiffWithLocalTime;
            return diff;

        } catch (Exception e) {
        }

        return 10;

    }

    //opciones del menú superior
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_peer_review, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle(getString(R.string.BienvenidoTitleMenu) + Login.userNameLoggedIn);

        return true;
    }

    @Override
    /**
     * Método que controla qué ocurre cuando se aprieta una opción del menú superior.
     *
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /**El link donde dice "Bienvenido XX" no hace nada pero se puede apretar por si se quisiera
        * implementar alguna funcionalidad más adelante. En el fondo un link, no sólo texto
        **/
        if (id == R.id.home) {

            return true;
        }

        //Lleva a la vista que selecciona la actividad
        else if (id == R.id.changeActivity){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, SelectActivitySession.class);
            this.finish();
            startActivity(intent);

            return true;
        }

        //Lleva a la vista que selecciona el uso de la app.
        else if (id == R.id.observations){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, ObserverMain.class);
            this.finish();
            startActivity(intent);

            return true;
        }

        //Lleva al login y desactiva al usuario loggeado
        else if (id == R.id.closeSession){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, Login.class);
            Login.userLoggedIn = null;
            this.finish();
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }



}
