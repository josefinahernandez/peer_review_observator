package com.example.peerreviewciae;

import java.util.Date;

/**
 * Created by Josefina on 13-09-2016.
 */
public class Revision {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("updatedAt")
    private Date updatedAt;

    @com.google.gson.annotations.SerializedName("Answer_id")
    private String Answer_id;

    @com.google.gson.annotations.SerializedName("Activity_id")
    private String Activity_id;

    @com.google.gson.annotations.SerializedName("Stage_id")
    private String Stage_id;

    @com.google.gson.annotations.SerializedName("Question_id")
    private String Question_id;

    @com.google.gson.annotations.SerializedName("Username_Reviser")
    private String Username_Reviser;

    @com.google.gson.annotations.SerializedName("Username_Reviewed")
    private String Username_Reviewed;

    @com.google.gson.annotations.SerializedName("Revision_Status")
    private String Revision_Status;

    @com.google.gson.annotations.SerializedName("Revision_Grade")
    private String Revision_Grade;

    @com.google.gson.annotations.SerializedName("Revision_Feedback")
    private String Revision_Feedback;


    public Revision(){

    }

    public Revision(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}

    public Date getUpdatedAt () {return updatedAt;}

    public String getAnswer_id () {return Answer_id;}
    public void setAnswer_id (String id){this.Answer_id = id;}

    public String getStage_id() {return Stage_id;}
    public void setStage_id(String desc) {this.Stage_id= desc;}

    public String getActivity_id(){return Activity_id;}
    public void setActivity_id(String at){this.Activity_id= at;}

    public String getQuestion_id(){return Question_id;}
    public void setQuestion_id(String at){this.Question_id= at;}

    public String getUsername_Reviser() {return Username_Reviser;}
    public void setUsername_Reviser(String qID) {this.Username_Reviser= qID;}

    public String getUsername_Reviewed() {return Username_Reviewed;}
    public void setUsername_Reviewed(String name) {this.Username_Reviewed= name;}

    public String getRevision_Status(){return Revision_Status;}
    public void setRevision_Status(String at){this.Revision_Status= at;}
    public String getRevision_Grade(){return Revision_Grade;}
    public void setRevision_Grade(String at){this.Revision_Grade= at;}
    public String getRevision_Feedback(){return Revision_Feedback;}
    public void setRevision_Feedback(String at){this.Revision_Feedback= at;}



}
