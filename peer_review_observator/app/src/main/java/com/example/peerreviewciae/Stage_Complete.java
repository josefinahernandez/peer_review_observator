package com.example.peerreviewciae;

/**
 * Created by Josefina on 13-09-2016.
 */
public class Stage_Complete {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Activity_id")
    private String Activity_id;

    @com.google.gson.annotations.SerializedName("Stage_id")
    private String Stage_id;

    @com.google.gson.annotations.SerializedName("Username")
    private String Username;

    @com.google.gson.annotations.SerializedName("Complete")
    private Boolean Complete;


    public Stage_Complete(){

    }

    public Stage_Complete(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}
    public String getStage_id() {return Stage_id;}
    public void setStage_id(String desc) {this.Stage_id= desc;}

    public String getUsername() {return Username;}
    public void setUsername(String name) {this.Username= name;}

    public String getActivity_id(){return Activity_id;}
    public void setActivity_id(String at){this.Activity_id= at;}

    public Boolean getComplete(){return Complete;}
    public void setComplete(Boolean at){this.Complete= at;}

}
