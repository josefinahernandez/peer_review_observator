package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.TextView;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.squareup.okhttp.OkHttpClient;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class stage2_2 extends AppCompatActivity {

    private MobileServiceClient mClient;

    String sName;
    String sInstructions;
    String sOpen;
    String sClose;

    String usernameReviewed;

    TextView nameTV;
    TextView insTV;
    TextView openTV;
    TextView closeTV;
    TextView chroneExplain;
    TextView chrone;

    TextView p1;
    TextView p2;
    TextView r1;
    TextView r2;
    TextView ps1;
    TextView ps2;

    Button send;

    RatingBar ev1;
    RatingBar ev2;

    TextView okTV2;
    TextView notOKtv;

    private MobileServiceTable<Question> questionTable;

    private MobileServiceTable<Answer> answerTable;

    private MobileServiceTable<Revision>revisionTable;
    private MobileServiceTable<Assignment>assignmentTable;

    private String stageID;
    private String assignmentStatus;

    EditText i1;
    EditText i2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage2_2);

        //menú superior (franja azul)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**Seteando la IG. Para no ir a buscarla a la BD en cada etapa y que sea más rápido el sistema, se envía siempre a través
         * un Intent de la vista anterior.
         **/

        //nombre de la etapa
        sName = getIntent().getStringExtra("stageName");
        //instrucciones de la etapa
        sInstructions = getIntent().getStringExtra("stageInstructions");
        //fecha de apertura de la etapa
        sOpen = getIntent().getStringExtra("stageOpen");
        //fecha de cierre de la etapa
        sClose = getIntent().getStringExtra("stageClose");
        //ID de esta etapa, sirve para ir a buscar datos a la BD
        stageID = getIntent().getStringExtra("stageID");

        //El Textview que muestra el nombre de la etapa
        nameTV = (TextView) findViewById(R.id.sName);
        //El Textview que muestra las instrucciones de la etapa
        insTV = (TextView) findViewById(R.id.sInstructions);
        //El Textview que muestra la fecha y hora de apertura de la etapa
        openTV = (TextView) findViewById(R.id.sOpen);
        //El Textview que muestra la fecha y hora de cierre de la etapa
        closeTV = (TextView) findViewById(R.id.sClose);
        //El Textview que muestra lo que es el cronómetro ("El tiempo restante al cierre de la etapa es...")
        chroneExplain = (TextView) findViewById(R.id.chone_explain);
        //El Textview que muestra el cronómetro
        chrone = (TextView) findViewById(R.id.chronometer);

        nameTV.setText(sName);
        insTV.setText(sInstructions + ":");
        openTV.setText(getString(R.string.FechaAperturaTV) + sOpen);
        closeTV.setText(getString(R.string.FechaCierreTV) + sClose);
        chroneExplain.setText(getString(R.string.CronometroTV));


        /**
         * Ahora seteamos el DE.
         * */

        /**En el intent anterior se envió el estado de la revisión, por si ya fue revisado y se están metiendo
         * por segunda vez o por primera y nunca ha sido revisado.
         * Además necesitamos el username de la persona revisada para guardar después la revisión con la información
         * correspondiente.
         */

        assignmentStatus = getIntent().getStringExtra("assignmentStatus");
        usernameReviewed = getIntent().getStringExtra("usernameReviewed");

        //Botón de envío
        send = (Button) findViewById(R.id.send_button_stage2);

        //Edittext para los comentarios que entregará el revisor para las preguntas 1 y 2.
        i1 = (EditText)findViewById(R.id.input1);
        i2 = (EditText)findViewById(R.id.input2);

        //Pregunta N°1 de la etapa anterior
        p1 = (TextView)findViewById(R.id.p1);
        //Pregunta N°2 de la etapa anterior
        p2 = (TextView)findViewById(R.id.p2);
        //Respuesta a la Pregunta N°1 que el revisor deberá revisar
        r1 = (TextView)findViewById(R.id.r1);
        //Respuesta a la Pregunta N°2 que el revisor deberá revisar
        r2 = (TextView)findViewById(R.id.r2);
        //Instrucciones de esta etapa sobre la retroalimentación que debe entregar el revisor
        //a la pregunta N°1
        ps1 = (TextView)findViewById(R.id.ps1);
        //Instrucciones de esta etapa sobre la retroalimentación que debe entregar el revisor
        //a la pregunta N°2
        ps2 = (TextView)findViewById(R.id.ps2);
        //Nota para la Pregunta N°1 (estrellas)
        ev1 = (RatingBar) findViewById(R.id.ratingBar1);
        //Nota para la Pregunta N°2 (estrellas)
        ev2 = (RatingBar) findViewById(R.id.ratingBar2);

        //Textview que confirma que la revisión fue guardada en la BD
        okTV2 = (TextView)findViewById(R.id.okTV2);
        okTV2.setVisibility(View.GONE);

        //Textview que avisa que hay una respuesta vacía y que no se guardarán los datos
        notOKtv = (TextView)findViewById(R.id.notOKtv);
        notOKtv.setVisibility(View.GONE);

        //Este método es para el cronómetro. Está explicado más abajo
        //Aquí se echa a andar el cronómetro.
        long diff = getTimeinMilliseconds();
        new CountDownTimer(diff, 1000) {

            public void onTick(long millisUntilFinished) {
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));

                //chrone.setText("" + millisUntilFinished / 1000);

                if((millisUntilFinished/1000 < 300) & (millisUntilFinished/1000 > 1))
                {
                    chrone.setTextColor(Color.parseColor("#FF0000"));
                    chrone.playSoundEffect(SoundEffectConstants.CLICK);
                }

                chrone.setText("" + hms);
            }

            public void onFinish() {

            }
        }.start();


        //Aquí nos conectamos a la BD. Este es código estándar de Azure
        try {
            // Create the Mobile Service Client instance, using the provided
            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //una vez realizada la conexión, buscamos las tablas.
            //tabla con las preguntas de la etapa 1 y 2
            questionTable = mClient.getTable(Question.class);
            //tabla con las respuestas recibidas en la etapa 1 que deberán ser revisadas ahora
            answerTable = mClient.getTable(Answer.class);
            //tabla con las revisiones, para agregar las revisiones que se enviarán en esta etapa
            revisionTable = mClient.getTable(Revision.class);
            //tabla con las revisiones asignadas a cada revisor
            assignmentTable = mClient.getTable(Assignment.class);

            //Buscamos las preguntas de la etapa 1
            refreshItemsFromTableQuestionsPrevious();
            //buscamos las respuestas a revisar
            refreshItemsFromTableAnswers();
            //buscamos las preguntas o instrucciones de la etapa actual
            refreshItemsFromTableQuestionsNow();

            /**
             * Si el revisor ya revisó esta respuesta y está ingresando por segunda vez (o más),
             * hay que mostrar lo que retroalimentó previamente.
             */
            if(!assignmentStatus.equals(getString(R.string.estadoAssignmentPendiente))){
                refreshItemsFromPreviousRevisions();
            }


        } catch (MalformedURLException e) {

        } catch (Exception e){

        }
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * Buscamos las preguntas de la etapa 1
     */
    private void refreshItemsFromTableQuestionsPrevious() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Question> resultsQ = refreshItemsFromMobileServiceTableQuestionsPrevious();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            p1.setText(resultsQ.get(0).getQuestion_text());
                            p2.setText(resultsQ.get(1).getQuestion_text());

                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * buscamos las respuestas a revisar
     */
    private void refreshItemsFromTableAnswers() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //Buscamos en la BD las respuesta enviadas por el usuario revisado en la etapa anterior
                    final List<Answer> prevAns = refreshItemsFromMobileServiceTableAnswers();

                    /**
                     * Cuando un usuario envía una respuesta, se guarda, en la columna "ID", el número de esa pregunta
                     * (si era la pregunta 0 o 1).
                     * Inicialmente la idea era guardar el ID de la pregunta pero me equivoqué y quedó así, y sirve igual.
                     */

                    String q1ID = "0";
                    String q2ID = "1";

                    //Las respuestas finales que tenemos que buscar quedarán guardadas en estas variables:
                    Answer a1 = new Answer();
                    Answer a2 = new Answer();

                    //no estoy segura qué hace este for, parece que no sirve y no lo borré, porque después lo sobre-escribo.
                    for(int i =0; i<prevAns.size();i++)
                    {
                        if(prevAns.get(i).getQuestion_id().equals(q1ID)){
                            a1 = prevAns.get(i);
                        }
                        else if (prevAns.get(i).getQuestion_id().equals(q2ID)){
                            a2 = prevAns.get(i);
                        }
                    }

                    //Acá separamos la lista prevAns en dos: todas las respuestas que tenemos de la pregunta 1 y todas las de la pregunta 2.
                    //Luego, para cada pregunta, buscamos la última respuesta recibida, y la guardamos en a1 y a2.
                    for(int i=0; i<prevAns.size();i++) {

                        //Para Q1:
                        if (prevAns.get(i).getQuestion_id().equals(q1ID)) {

                            Date updated1 = a1.getUpdatedAt();
                            Date updated2 = prevAns.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                a1 = prevAns.get(i);
                            }
                        }

                        //si estamos en Q2
                        else if (prevAns.get(i).getQuestion_id().equals(q2ID)) {

                            Date updated1 = a2.getUpdatedAt();
                            Date updated2 = prevAns.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                a2 = prevAns.get(i);
                            }

                        }
                    }

                    //las copiamos a aa1 y aa2, porque para usarlas en UI thread tienen que ser declaradas como variables FINALES.
                    //Luego, las mostramos en r1 y r2, las deshabilitamos y cambiamos el botón de envío por botón para editar.
                    final Answer aa1 = a1;
                    final Answer aa2 = a2;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            r1.setText(getString(R.string.S2_2_rTV)+ aa1.getAnswer_text());
                            r2.setText(getString(R.string.S2_2_rTV)+ aa2.getAnswer_text());

                        }
                    });
                } catch (final Exception e){

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * buscamos las preguntas o instrucciones de la etapa actual
     */
    private void refreshItemsFromTableQuestionsNow() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Question> resultsQ = refreshItemsFromMobileServiceTableQuestionsNow();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ps1.setText(resultsQ.get(0).getQuestion_text());
                            ps2.setText(resultsQ.get(1).getQuestion_text());


                        }
                    });
                } catch (final Exception e){
                    //createAndShowDialogFromTask(e, "Error-5");
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    /**
     * Buscamos en la BD las preguntas de:
     * 1- esta actividad
     * 2- etapa anterior
     */
    private List<Question> refreshItemsFromMobileServiceTableQuestionsPrevious() throws ExecutionException, InterruptedException {

        List<Question> q = questionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage1ID).orderBy("Question_Order", QueryOrder.Ascending).execute().get();
        return q;
    }

    /**
     * Buscamos en la BD las respuestas de:
     * 1- esta actividad
     * 2- etapa anterior
     * 3- usuario a quien se revisará
     */
    private List<Answer> refreshItemsFromMobileServiceTableAnswers() throws ExecutionException, InterruptedException {

        List<Answer> a = answerTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage1ID).and().field("Username").eq(usernameReviewed).orderBy("Question_id", QueryOrder.Ascending).execute().get();
        return a;
    }

    /**
     * Buscamos en la BD las preguntas de:
     * 1- esta actividad
     * 2- esta etapa
     */
    private List<Question> refreshItemsFromMobileServiceTableQuestionsNow() throws ExecutionException, InterruptedException {

        List<Question> q = questionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage2ID).orderBy("Question_Order", QueryOrder.Ascending).execute().get();
        return q;
    }

    /**
     * Buscamos en la BD las revisiones asignadas de:
     * 1- esta actividad
     * 2- esta etapa
     * 3- este usuario revisor (el que está loggeado)
     * 4- el usuario a revisar (revisado)
     */
    private List<Assignment> obtainItemsFromMobileServiceTableAssignment () throws ExecutionException, InterruptedException {

        List<Assignment> a = assignmentTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage2ID).and().field("Username_Reviser").eq(Login.userLoggedIn.getUsername()).and().field("Username_Reviewed").eq(usernameReviewed).execute().get();
        return a;
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * Si el revisor ya revisó esta respuesta y está ingresando por segunda vez (o más),
     * hay que mostrar lo que retroalimentó previamente.
     */
    private void refreshItemsFromPreviousRevisions(){

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //hacemos la consulta en la BD, que retorna TODAS las retroalimentaciones que ha enviado este usuario a estas preguntas.
                    final List<Revision> prevRev = obtainItemsFromMobileServiceTableRevisions();

                    /**
                     * Cuando un usuario envía una respuesta, se guarda, en la columna "ID", el número de esa pregunta
                     * (si era la pregunta 0 o 1).
                     * Inicialmente la idea era guardar el ID de la pregunta pero me equivoqué y quedó así, y sirve igual.
                     */
                    String q1ID = "0";
                    String q2ID = "1";

                    //Las revisiones finales que tenemos que buscar quedarán guardadas en estas variables:
                    Revision q1 = new Revision();
                    Revision q2 = new Revision();

                    //no estoy segura qué hace este for, parece que no sirve y no lo borré, porque después lo sobre-escribo.
                    for (int i = prevRev.size()-1; i >=0; i--) {

                        if (prevRev.get(i).getQuestion_id().equals(q1ID)) {
                            q1 = prevRev.get(i);
                        }
                        else if (prevRev.get(i).getQuestion_id().equals(q2ID))
                        {
                            q2 = prevRev.get(i);
                        }
                    }

                    //Acá separamos la lista prevRev en dos: todas las revisiones que tenemos de la pregunta 1 y todas las de la pregunta 2.
                    //Luego, para cada pregunta, buscamos la última revisión recibida, y la guardamos en q1 y q2.
                    for (int i = 0; i < prevRev.size() - 1; i++) {

                        //si estamos en Q1
                        if (prevRev.get(i).getQuestion_id().equals(q1ID)) {

                            Date updated1 = q1.getUpdatedAt();
                            Date updated2 = prevRev.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                q1 = prevRev.get(i);
                            }
                        }

                        //si estamos en Q2
                        else if (prevRev.get(i).getQuestion_id().equals(q2ID)) {

                            Date updated1 = q2.getUpdatedAt();
                            Date updated2 = prevRev.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                q2 = prevRev.get(i);
                            }

                        }
                    }

                    //las copiamos a qq1 y qq2, porque para usarlas en UI thread tienen que ser declaradas como variables FINALES.
                    //Luego, las mostramos en i1 e i2, las deshabilitamos y cambiamos el botón de envío por botón para editar.

                    final Revision qq1 = q1;
                        final Revision qq2 = q2;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String alpha = qq1.getRevision_Feedback();
                                String beta = qq2.getRevision_Feedback();

                                if(alpha!=null) {

                                    i1.setText(alpha);
                                    i2.setText(beta);

                                    send.setText(getString(R.string.BotonEnvioParaEditar));

                                    i1.setEnabled(false);
                                    i2.setEnabled(false);
                                    ev1.setEnabled(false);
                                    ev2.setEnabled(false);

                                }
                            }
                        });


                } catch (Exception e){

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    public void onSend(View view) {

        notOKtv.setVisibility(View.GONE);

        if (mClient == null) {
            return;
        }

        //si se están enviando respuestas nuevas:
        if(send.getText().equals(getString(R.string.BotonEnviar)))
        {
            //Revisamos que no estén vacías las retroalimentaciones
            List<String> feedback = new ArrayList<>();

            feedback.add(i1.getText().toString());
            feedback.add(i2.getText().toString());

            Boolean ok = true;
            for(int i = 0; i<feedback.size();i++)
            {
                if(feedback.get(i).equals(""))
                {
                    notOKtv.setVisibility(View.VISIBLE);
                    ok = false;
                }
            }

            //Si no están vacías
            if(ok) {

                //creamos cada revisión y las agregamos a la BD
                for (int i = 0; i < feedback.size(); i++) {
                    final Revision an = new Revision();

                    an.setActivity_id(SessionCode.finalActivitySessionID);
                    an.setStage_id(StagesGrid.stage2ID);
                    an.setQuestion_id(Integer.toString(i));
                    an.setUsername_Reviser(Login.userLoggedIn.getUsername());
                    an.setUsername_Reviewed(usernameReviewed);
                    an.setRevision_Feedback(feedback.get(i));
                    an.setRevision_Status(getString(R.string.estadoRevisionPendiente));

                    if (i == 0) {
                        an.setRevision_Grade(Float.toString(ev1.getRating()));
                    } else {
                        an.setRevision_Grade(Float.toString(ev2.getRating()));
                    }

                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                final Revision entity = addItemInTable(an);

                            } catch (final Exception e) {

                            }
                            return null;
                        }
                    };

                    runAsyncTask(task);
                }

                //avisamos al usuario que se guardaron sus retroalimentaciones y dejamos la opción de editarlas.
                okTV2.setVisibility(View.VISIBLE);
                updateAssigmentTable();
                send.setText(getString(R.string.BotonEnvioParaEditar));
                i1.setEnabled(false);
                i2.setEnabled(false);
                ev1.setEnabled(false);
                ev2.setEnabled(false);
                }
            }

        //Si el botón decía inicialmente "Editar respuestas anteriores", dejamos la opción para editarlas.
        else{

                i1.setEnabled(true);
                i2.setEnabled(true);
                ev1.setEnabled(true);
                ev2.setEnabled(true);
                send.setText(getString(R.string.BotonEnviar));
            }

    }

    //Método que agrega una revisión en la BD
    public Revision addItemInTable(Revision u) throws ExecutionException, InterruptedException {
        Revision entity = revisionTable.insert(u).get();
        return entity;
    }

    /**
     * Método que retorna de la BD las revisiones guardadas de:
     * 1- esta actividad
     * 2- esta etapa
     * 3- este usuario revisor
     * 4- el usuario revisado
     */
    private List<Revision> obtainItemsFromMobileServiceTableRevisions () throws ExecutionException, InterruptedException {

        List<Revision> a = revisionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage2ID).and().field("Username_Reviser").eq(Login.userLoggedIn.getUsername()).and().field("Username_Reviewed").eq(usernameReviewed).orderBy("Question_id", QueryOrder.Ascending).execute().get();
        return a;
    }

    /**
     * Método que llama a actualizar en la BD el estado de las asignaciones de revisiones,
     * para cambiarlas de "pendiente" a "revisado" y así que muestre la opción de editar al usuario,
     * en lugar de "revisar por primera vez"
     */
    public void updateAssigmentTable(){

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Assignment> assig = obtainItemsFromMobileServiceTableAssignment();

                            for (Assignment item : assig) {
                                try {
                                    item.setAssignment_Status(getString(R.string.estadoAssignmentCompletado));
                                    Assignment a = updateItemInTable(item);
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }
                } catch (Exception e){

                }

                return null;
            }
        };

        runAsyncTask(task);


    }

    //Método que actualiza las asignaciones, de estado pendiente a estado revisado
    public Assignment updateItemInTable (Assignment as1) throws ExecutionException, InterruptedException {
        Assignment as11 = assignmentTable.update(as1).get();
        return as11;
    }

    //Vuelve a la lista de revisiones que debe retroalimentar el usuario
    public void onVolver (View view)
    {
        chrone.setSoundEffectsEnabled(false);

        Intent intent = new Intent(this, stage2.class);

        intent.putExtra("stageID",stageID);
        intent.putExtra("stageName", sName);
        intent.putExtra("stageInstructions", sInstructions);

        intent.putExtra("stageOpen", sOpen);
        intent.putExtra("stageClose",sClose);

        this.finish();

        startActivity(intent);
    }

    //Cronómetro
    public long getTimeinMilliseconds() {

        try {
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date date = (Date) formatter.parse(sClose);

            Calendar today = Calendar.getInstance();
            long milisClose = date.getTime();
            long milisNow = today.getTimeInMillis();

            long diff = milisClose - milisNow  + StagesGrid.currentDiffWithLocalTime;
            return diff;

        } catch (Exception e) {
        }

        return 30000;

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_peer_review, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle("Bienvenido " + Login.userNameLoggedIn);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {

            return true;
        }

        else if (id == R.id.changeActivity){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, SelectActivitySession.class);
            this.finish();

            startActivity(intent);

            return true;
        }

        else if (id == R.id.observations){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, ObserverMain.class);
            this.finish();
            startActivity(intent);

            return true;
        }



        else if (id == R.id.closeSession){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, Login.class);
            Login.userLoggedIn=null;
            this.finish();
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }
}
