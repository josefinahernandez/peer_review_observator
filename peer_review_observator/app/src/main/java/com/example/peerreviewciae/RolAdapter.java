package com.example.peerreviewciae;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * No se usa esta clase
 */
public class RolAdapter extends ArrayAdapter<Rol> {

    Context context;

    int layoutResourceId;

    public RolAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        context = context;
        layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        return row;
    }
}
