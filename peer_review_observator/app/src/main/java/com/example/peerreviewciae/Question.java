package com.example.peerreviewciae;

/**
 * Created by Josefina on 13-09-2016.
 */
public class Question {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Activity_id")
    private String Activity_id;

    @com.google.gson.annotations.SerializedName("Stage_id")
    private String Stage_id;

    @com.google.gson.annotations.SerializedName("Question_text")
    private String Question_text;

    @com.google.gson.annotations.SerializedName("Question_Order")
    private int Question_Order;

    @com.google.gson.annotations.SerializedName("Help_Text")
    private String Help_Text;


    public Question(){

    }

    public Question(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}
    public String getStage_id() {return Stage_id;}
    public void setStage_id(String desc) {this.Stage_id= desc;}
    public String getQuestion_text() {return Question_text;}
    public void setQuestion_text(String qID) {this.Question_text= qID;}
    public int getQuestion_Order() {return Question_Order;}
    public void setQuestion_Order(int question_order) {this.Question_Order= question_order;}
    public String getHelp_Text(){return Help_Text;}
    public void setHelp_Text(String at){this.Help_Text= at;}
    public String getActivity_id(){return Activity_id;}
    public void setActivity_id(String at){this.Activity_id= at;}

}
