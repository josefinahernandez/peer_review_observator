package com.example.peerreviewciae;

/**
 * Created by Josefina on 11-09-2016.
 */
public class Rol {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Description")
    private String Description;


    public Rol(){

    }

    public Rol(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id= id;}
    public String getDescription() {return Description;}
    public void setDescription(String desc) {this.Description= desc;}

}
