package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


/**
 * Clase que controla la vista que permite decidir si se utilizará la parte de revisión de pares o el observador.
 */
public class ChooseObservatorOrPeerreview extends Activity {

    String activitySessionID;
    String activitySessionName;
    String activitySessionDescription;
    Boolean requiresSessionAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_observator_or_peerreview);

        //String que contiene el ID de la actividad
        activitySessionID = this.getIntent().getStringExtra("activityID");
        //String que contiene el nombre de la actividad
        activitySessionName = this.getIntent().getStringExtra("");
        //String que contiene la descripción de la actividad
        activitySessionDescription = this.getIntent().getStringExtra("");
        //Boolean que avisa si el usuario loggeado ya se ha inscrito en esta actividad previamente o no. Si no, entonces hay que inscribirlo.
        requiresSessionAdd = this.getIntent().getBooleanExtra("", true);
    }

    //Si se escoge el observador
    public void goToObserve (View view){

        Intent intent = new Intent(this, ObserverMain.class);
        startActivity(intent);
    }

    //Si se escoge la revisión entre pares.
    public void goToReview (View view){

        Intent intent = new Intent(this, StagesGrid.class);
        intent.putExtra("activityID", activitySessionID);
        intent.putExtra("activityName", activitySessionName);
        intent.putExtra("activityDescription", activitySessionDescription);
        intent.putExtra("requiresSessionAdd", requiresSessionAdd);

        startActivity(intent);

    }


}
