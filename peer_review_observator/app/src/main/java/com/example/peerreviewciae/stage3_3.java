package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.Rating;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.squareup.okhttp.OkHttpClient;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class stage3_3 extends AppCompatActivity {

    private MobileServiceClient mClient;

    String sName;
    String sInstructions;
    String sOpen;
    String sClose;
    String stageID;
    String username_Reviser;

    TextView nameTV;
    TextView insTV;
    TextView openTV;
    TextView closeTV;
    TextView chroneExplain;
    TextView chrone;

    TextView ok;
    RatingBar ev1;
    RatingBar ev2;

    TextView pant1;
    TextView pant2;
    TextView ans1;
    TextView ans2;

    Button send;

    private MobileServiceTable<Question> questionTable;

    private MobileServiceTable<Answer> answerTable;

    private MobileServiceTable<Revision> revisionTable;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage3_3);

        //menú superior (franja azul)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**Seteando la IG. Para no ir a buscarla a la BD en cada etapa y que sea más rápido el sistema, se envía siempre a través
         * un Intent de la vista anterior.
         **/

        //nombre de la etapa
        sName = getIntent().getStringExtra("stageName");
        //instrucciones de la etapa
        sInstructions = getIntent().getStringExtra("stageInstructions");
        //fecha de apertura de la etapa
        sOpen = getIntent().getStringExtra("stageOpen");
        //fecha de cierre de la etapa
        sClose = getIntent().getStringExtra("stageClose");
        //ID de esta etapa, sirve para ir a buscar datos a la BD
        stageID = getIntent().getStringExtra("stageID");
        username_Reviser = getIntent().getStringExtra("usernameReviewed");

        //El Textview que muestra el nombre de la etapa
        nameTV = (TextView) findViewById(R.id.sName);
        //El Textview que muestra la fecha y hora de apertura de la etapa
        openTV = (TextView) findViewById(R.id.sOpen);
        //El Textview que muestra la fecha y hora de cierre de la etapa
        closeTV = (TextView) findViewById(R.id.sClose);
        //El Textview que muestra lo que es el cronómetro ("El tiempo restante al cierre de la etapa es...")
        chroneExplain = (TextView) findViewById(R.id.chone_explain);
        //El Textview que muestra el cronómetro
        chrone = (TextView) findViewById(R.id.chronometer);

        //seteamos los textview con la información correspondiente.
        nameTV.setText(sName);
        openTV.setText(getString(R.string.FechaAperturaTV) + sOpen);
        closeTV.setText(getString(R.string.FechaCierreTV) + sClose);
        chroneExplain.setText(getString(R.string.CronometroTV));

        //Este método es para el cronómetro. Está explicado más abajo
        //Aquí se echa a andar el cronómetro.
        long diff = getTimeinMilliseconds();
        new CountDownTimer(diff, 1000) {

            public void onTick(long millisUntilFinished) {
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));

                //chrone.setText("" + millisUntilFinished / 1000);
                if((millisUntilFinished/1000 < 300) & (millisUntilFinished/1000 > 1))
                {
                    chrone.setTextColor(Color.parseColor("#FF0000"));
                    chrone.playSoundEffect(SoundEffectConstants.CLICK);
                }

                chrone.setText("" + hms);
            }

            public void onFinish() {

            }
        }.start();


        /**
         * Ahora seteamos el DE.
         * */

        //textview que muestra que los datos fueron enviados exitosamente
        ok = (TextView) findViewById(R.id.ok);
        ok.setVisibility(View.GONE);

        //botón para enviar
        send = (Button) findViewById(R.id.enviarRetr);
        //Nota para la pregunta 1 sobre la retroalimentación recibida (estrellas)
        ev1 = (RatingBar) findViewById(R.id.ev1);
        //Nota para la pregunta 2 sobre la retroalimentación recibida (estrellas)
        ev2 = (RatingBar) findViewById(R.id.ev2);

        //Textview que muestra Pregunta N°1 de la etapa 1
        pant1 = (TextView) findViewById(R.id.pant1);
        //Textview que muestra Pregunta N°2 de la etapa 1
        pant2 = (TextView) findViewById(R.id.pant2);
        //Textview que muestra la respuesta del usuario en la Pregunta N°1 de la etapa 1
        ans1 = (TextView) findViewById(R.id.ans1);
        //Textview que muestra la respuesta del usuario en la Pregunta N°2 de la etapa 1
        ans2 = (TextView) findViewById(R.id.ans2);

        //Aquí nos conectamos a la BD. Este es código estándar de Azure
        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient("https://peerreviewciae.azurewebsites.net", this);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            //una vez realizada la conexión, buscamos las tablas.
            //Tabla de preguntas
            questionTable = mClient.getTable(Question.class);
            //Tabla de respuestas
            answerTable = mClient.getTable(Answer.class);
            //Tabla de revisiones
            revisionTable = mClient.getTable(Revision.class);

            //Buscamos las preguntas de la etapa 1
            refreshItemsFromTableQuestionsPrevious();
            //Buscamos las respuestas de la etapa 1 de este usuario
            refreshItemsFromTableAnswers();
            //Buscamos las revisiones que recibió este usuario en la etapa 2
            refreshItemsFromTableRevisions();
            //Buscamos las preguntas de la etapa 3
            refreshItemsFromTableQuestionsNow();


        } catch (MalformedURLException e) {
            //createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
        } catch (Exception e) {
            //createAndShowDialog(e, "Error-here");
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * Buscamos las preguntas de la etapa 1
     */
    private void refreshItemsFromTableQuestionsPrevious() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Question> resultsQ = refreshItemsFromMobileServiceTableQuestionsPrevious();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            pant1.setText(resultsQ.get(0).getQuestion_text());
                            pant2.setText(resultsQ.get(1).getQuestion_text());

                        }
                    });
                } catch (final Exception e) {

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * Buscamos las respuestas de la etapa 1 de este usuario
     */
    private void refreshItemsFromTableAnswers() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //Buscamos en la BD las respuesta enviadas por este usuario en la etapa anterior
                    final List<Answer> prevAns = refreshItemsFromMobileServiceTableAnswers();

                    /**
                     * Cuando un usuario envía una respuesta, se guarda, en la columna "ID", el número de esa pregunta
                     * (si era la pregunta 0 o 1).
                     * Inicialmente la idea era guardar el ID de la pregunta pero me equivoqué y quedó así, y sirve igual.
                     */
                    String q1ID = "0";
                    String q2ID = "1";

                    //Las respuestas finales que tenemos que buscar quedarán guardadas en estas variables:
                    Answer a1 = new Answer();
                    Answer a2 = new Answer();

                    //no estoy segura qué hace este for, parece que no sirve y no lo borré, porque después lo sobre-escribo.
                    for (int i = 0; i < prevAns.size(); i++) {
                        if (prevAns.get(i).getQuestion_id().equals(q1ID)) {
                            a1 = prevAns.get(i);
                        } else if (prevAns.get(i).getQuestion_id().equals(q2ID)) {
                            a2 = prevAns.get(i);
                        }
                    }

                    //Acá separamos la lista prevAns en dos: todas las respuestas que tenemos de la pregunta 1 y todas las de la pregunta 2.
                    //Luego, para cada pregunta, buscamos la última respuesta recibida, y la guardamos en a1 y a2.
                    for (int i = 0; i < prevAns.size(); i++) {

                        //Para Q1:
                        if (prevAns.get(i).getQuestion_id().equals(q1ID)) {

                            Date updated1 = a1.getUpdatedAt();
                            Date updated2 = prevAns.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                a1 = prevAns.get(i);
                            }
                        }

                        //si estamos en Q2
                        else if (prevAns.get(i).getQuestion_id().equals(q2ID)) {

                            Date updated1 = a2.getUpdatedAt();
                            Date updated2 = prevAns.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                a2 = prevAns.get(i);
                            }

                        }
                    }

                    //las copiamos a aa1 y aa2, porque para usarlas en UI thread tienen que ser declaradas como variables FINALES.
                    //Luego, las mostramos en r1 y r2, las deshabilitamos y cambiamos el botón de envío por botón para editar.
                    final Answer aa1 = a1;
                    final Answer aa2 = a2;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ans1.setText(getString(R.string.S3_3_aTV) + aa1.getAnswer_text());
                            ans2.setText(getString(R.string.S3_3_aTV) + aa2.getAnswer_text());

                        }
                    });
                } catch (final Exception e) {

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Buscamos en la BD las preguntas de:
     * 1- esta actividad
     * 2- etapa 1
     *
     * */
    private List<Question> refreshItemsFromMobileServiceTableQuestionsPrevious() throws ExecutionException, InterruptedException {

        List<Question> q = questionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage1ID).orderBy("Question_Order", QueryOrder.Ascending).execute().get();
        return q;
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * Buscamos las revisiones que recibió este usuario en la etapa 2
     */
    private void refreshItemsFromTableRevisions() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    //hacemos la consulta en la BD, que retorna TODAS las retroalimentaciones que recibió este usuario a estas preguntas.
                    final List<Revision> prevRev = refreshItemsFromMobileServiceTableRevisions();

                    /**
                     * Cuando un usuario envía una respuesta, se guarda, en la columna "ID", el número de esa pregunta
                     * (si era la pregunta 0 o 1).
                     * Inicialmente la idea era guardar el ID de la pregunta pero me equivoqué y quedó así, y sirve igual.
                     */
                    String q1ID = "0";
                    String q2ID = "1";

                    //Las revisiones finales que tenemos que buscar quedarán guardadas en estas variables:
                    Revision r1 = new Revision();
                    Revision r2 = new Revision();

                    //no estoy segura qué hace este for, parece que no sirve y no lo borré, porque después lo sobre-escribo.
                    for (int i = 0; i < prevRev.size(); i++) {
                        if (prevRev.get(i).getQuestion_id().equals(q1ID)) {
                            r1 = prevRev.get(i);
                        } else if (prevRev.get(i).getQuestion_id().equals(q2ID)) {
                            r2 = prevRev.get(i);
                        }
                    }

                    //Acá separamos la lista prevRev en dos: todas las revisiones que tenemos de la pregunta 1 y todas las de la pregunta 2.
                    //Luego, para cada pregunta, buscamos la última revisión recibida, y la guardamos en r1 y r2.
                    for (int i = 0; i < prevRev.size(); i++) {

                        //si estamos en Q1
                        if (prevRev.get(i).getQuestion_id().equals(q1ID)) {

                            Date updated1 = r1.getUpdatedAt();
                            Date updated2 = prevRev.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                r1 = prevRev.get(i);
                            }
                        }

                        //si estamos en Q2
                        else if (prevRev.get(i).getQuestion_id().equals(q2ID)) {

                            Date updated1 = r2.getUpdatedAt();
                            Date updated2 = prevRev.get(i).getUpdatedAt();

                            if (updated2.after(updated1)) {
                                r2 = prevRev.get(i);
                            }

                        }
                    }

                    //las copiamos a rr1 y rr2, porque para usarlas en UI thread tienen que ser declaradas como variables FINALES.
                    final Revision rr1 = r1;
                    final Revision rr2 = r2;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Retroalimentación Pregunta 1
                            TextView r1 = (TextView) findViewById(R.id.r1);
                            //Retroalimentación Pregunta 2
                            TextView r2 = (TextView) findViewById(R.id.r2);
                            //Nota Pregunta 1
                            TextView n1 = (TextView) findViewById(R.id.n1);
                            //Nota Pregunta 2
                            TextView n2 = (TextView) findViewById(R.id.n2);

                            //seteamos los textview con la información correspondiente.
                            r1.setText(getString(R.string.S3_3_rTV) + rr1.getRevision_Feedback());
                            n1.setText(getString(R.string.S3_3_nTV) + rr1.getRevision_Grade());

                            r2.setText(getString(R.string.S3_3_rTV) + rr2.getRevision_Feedback());
                            n2.setText(getString(R.string.S3_3_nTV) + rr2.getRevision_Grade());
                        }
                    });
                } catch (final Exception e) {

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Método que llama a la BD y despliega la información que necesitamos en la vista.
     * Tiene una estructura rígida que no se puede
     * cambiar, funciona con Threads.
     * Buscamos las preguntas de la etapa 3
     */
    private void refreshItemsFromTableQuestionsNow() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Question> resultsQ = refreshItemsFromMobileServiceTableQuestionsNow();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            TextView p1 = (TextView) findViewById(R.id.p1);
                            TextView p2 = (TextView) findViewById(R.id.p2);

                            p1.setText(resultsQ.get(0).getQuestion_text());
                            p2.setText(resultsQ.get(1).getQuestion_text());
                        }
                    });
                } catch (final Exception e) {

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Buscamos en la BD las respuestas de:
     * 1- esta actividad
     * 2- etapa 1
     * 3- usuario loggeado
     * */
    private List<Answer> refreshItemsFromMobileServiceTableAnswers() throws ExecutionException, InterruptedException {

        List<Answer> a = answerTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage1ID).and().field("Username").eq(Login.userLoggedIn.getUsername()).orderBy("Question_id", QueryOrder.Ascending).execute().get();
        return a;
    }

    /**
     * Buscamos en la BD las revisiones de:
     * 1- esta actividad
     * 2- etapa 2
     * 3- usuario loggeado como usuario REVISADO
     * 4- un usuario revisor específico que se recibe en "username_reviser"
     * */
    private List<Revision> refreshItemsFromMobileServiceTableRevisions() throws ExecutionException, InterruptedException {

        List<Revision> a = revisionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage2ID).and().field("Username_Reviewed").eq(Login.userLoggedIn.getUsername()).and().field("Username_Reviser").eq(username_Reviser).orderBy("Question_id", QueryOrder.Ascending).execute().get();
        return a;
    }

    /**
     * Buscamos en la BD las preguntas de:
     * 1- esta actividad
     * 2- esta etapa
     * */
    private List<Question> refreshItemsFromMobileServiceTableQuestionsNow() throws ExecutionException, InterruptedException {

        List<Question> q = questionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage3ID).orderBy("Question_Order", QueryOrder.Ascending).execute().get();
        return q;
    }

    /**
     * Método que guarda las revisiones a las retroalimentaciones y actualiza la BD y la vista. Se llama al
     * apretar el botón de "Enviar"
     */
    public void onEnviar(View view) {

        if (mClient == null) {
            return;
        }

        //Primero creamos ambas revisiones (a la pregunta 1 y pregunta 2 de esta etapa)
        final Revision an1 = new Revision();

        an1.setActivity_id(SessionCode.finalActivitySessionID);
        an1.setStage_id(StagesGrid.stage3ID);
        an1.setQuestion_id("0");
        an1.setUsername_Reviser(Login.userLoggedIn.getUsername());
        an1.setUsername_Reviewed(username_Reviser);
        an1.setRevision_Feedback("");
        an1.setRevision_Status(getString(R.string.estadoRevisionNA));
        an1.setRevision_Grade(Float.toString(ev1.getRating()));

        final Revision an2 = new Revision();

        an2.setActivity_id(SessionCode.finalActivitySessionID);
        an2.setStage_id(StagesGrid.stage3ID);
        an2.setQuestion_id("0");
        an2.setUsername_Reviser(Login.userLoggedIn.getUsername());
        an2.setUsername_Reviewed(username_Reviser);
        an2.setRevision_Feedback("");
        an2.setRevision_Status(getString(R.string.estadoRevisionNA));
        an2.setRevision_Grade(Float.toString(ev1.getRating()));

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    //Agregamos las revisiones a la BD
                    final Revision entity1 = addItemInTable(an1);
                    final Revision entity2 = addItemInTable(an2);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //deshabilitamos los botones
                            if (entity1 != null && entity2 != null) {
                                ok.setVisibility(View.VISIBLE);
                                ev1.setEnabled(false);
                                ev2.setEnabled(false);
                                send.setEnabled(false);

                            }
                        }
                    });

                } catch (final Exception e) {

                }

                return null;
            }
        };

        runAsyncTask(task);

        //Por último, actualizamos la tabla de revisiones para cambiar el estado de estas revisiones de "pendiente"
        // a "completado"
        updateRevisionTable();

    }

    /**
     * Método que actualiza la tabla de revisiones para cambiar el estado de estas revisiones de "pendiente"
     // a "completado"
     */
    public void updateRevisionTable() {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    final List<Revision> prevRev = refreshItemsFromMobileServiceTableRevisionsPrev();

                    for (Revision item : prevRev) {
                        try {
                            item.setRevision_Status(getString(R.string.estadoRevisionCompletado));
                            Revision a = updateItemInTable(item);
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                } catch (Exception e) {

                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Buscamos en la BD las revisiones de:
     * 1- esta actividad
     * 2- etapa 2
     * 3- usuario loggeado como usuario REVISADO
     * 4- un usuario revisor específico que se recibe en "username_reviser"
     *
     * Este método es llamado por "updateRevisionTable" solamente.
     * */
    private List<Revision> refreshItemsFromMobileServiceTableRevisionsPrev() throws ExecutionException, InterruptedException {

        List<Revision> a = revisionTable.where().field("Activity_id").eq(SessionCode.finalActivitySessionID).and().field("Stage_id").eq(StagesGrid.stage2ID).and().field("Username_Reviewed").eq(Login.userLoggedIn.getUsername()).and().field("Username_Reviser").eq(username_Reviser).orderBy("Question_id", QueryOrder.Ascending).execute().get();
        return a;
    }

    //Método que agrega una revisión a la BD
    public Revision addItemInTable(Revision u) throws ExecutionException, InterruptedException {
        Revision entity = revisionTable.insert(u).get();
        return entity;
    }

    //Método que actualiza una revisión en la BD
    public Revision updateItemInTable(Revision as1) throws ExecutionException, InterruptedException {
        Revision as11 = revisionTable.update(as1).get();
        return as11;
    }

    //Vuelve a stage3
    public void onVolver(View view) {

        chrone.setSoundEffectsEnabled(false);

        Intent intent = new Intent(this, stage3.class);

        intent.putExtra("stageID", stageID);
        intent.putExtra("stageName", sName);
        intent.putExtra("stageInstructions", sInstructions);

        intent.putExtra("stageOpen", sOpen);
        intent.putExtra("stageClose", sClose);

        this.finish();

        startActivity(intent);
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    public long getTimeinMilliseconds() {

        try {
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date date = (Date) formatter.parse(sClose);

            Calendar today = Calendar.getInstance();
            long milisClose = date.getTime();
            long milisNow = today.getTimeInMillis();

            long diff = milisClose - milisNow  + StagesGrid.currentDiffWithLocalTime;
            return diff;

        } catch (Exception e) {
        }

        return 30000;

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_peer_review, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle("Bienvenido " + Login.userNameLoggedIn);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {

            return true;
        }

        else if (id == R.id.changeActivity){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, SelectActivitySession.class);
            this.finish();
            startActivity(intent);

            return true;
        }

        else if (id == R.id.observations){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, ObserverMain.class);
            this.finish();
            startActivity(intent);

            return true;
        }


        else if (id == R.id.closeSession){

            chrone.setSoundEffectsEnabled(false);

            Intent intent = new Intent(this, Login.class);
            Login.userLoggedIn = null;
            this.finish();
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }
}

