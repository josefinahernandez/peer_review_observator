package com.example.peerreviewciae;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class COPUSClassResumeActivity extends AppCompatActivity {

    TextView classResumeStudent;
    TextView classResumeTeacher;
    Map<Long,String> data;
    Map<Long,String> commentsMap;
    Long sLTime = 0L,
            sIndTime = 0L,
            sCGTime = 0L,
            sWGTime = 0L,
            sAnQTime = 0L,
            sSQTime = 0L,
            sWCTime = 0L,
            sPrdTime = 0L,
            sSPTime = 0L,
            sTQTime = 0L,
            sWTime = 0L,
            sOGTime = 0L;
    Long tLecTime = 0L,
            tWTime = 0L,
            tRtWTime = 0L,
            tFUpTime = 0L,
            tPQTime = 0L,
            tCQTime = 0L,
            tAnQTime = 0L,
            tMGTime = 0L,
            t1o1Time = 0L,
            tDVTime = 0L,
            tAdmTime = 0L;
    Long totalTime = 0L;
    String[] codesStudent = {"sL","sInd","sCG","sWG","sAnQ","sSQ","sWC","sPrd","sSP","sTQ","sW","sOG"};
    String[] codesTeacher = {"tLec","tW","tRtW","tFUp","tPQ","tCQ","tAnQ","tMG","t1o1","tDV","tAdm"};
    Button saveObservation;
    PeerReviewDB dbhelper = new PeerReviewDB(this);

    //Tratando de subir a la nube
    private MobileServiceClient mClient;
    //observationsdone obs = new observationsdone();
    private PieChart mChartTeacher;
    private PieChart mChartStudent;
    private LineChart mLineChartStudent;
    private LineChart mLineChartTeacher;
    private ArrayList<Entry> mStudentEntriesIndividual = new ArrayList<>();
    private ArrayList<Entry> mStudentEntriesGroup = new ArrayList<>();
    private ArrayList<Entry> mStudentEntriesQuestion = new ArrayList<>();
    private ArrayList<Entry> mStudentEntriesWaiting = new ArrayList<>();
    private ArrayList<Entry> mTeacherEntriesLecturing = new ArrayList<>();
    private ArrayList<Entry> mTeacherEntriesQuestion = new ArrayList<>();
    private ArrayList<Entry> mTeacherEntriesAdministration = new ArrayList<>();
    float observationDuration = 0L;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = (Map<Long,String>) getIntent().getExtras().get("table");
        commentsMap = (Map<Long,String>) getIntent().getExtras().get("comments");
        setContentView(R.layout.activity_class_resume);
        classResumeStudent = (TextView)findViewById(R.id.resumeviewStudent);
        classResumeTeacher = (TextView)findViewById(R.id.resumeviewTeacher);
        saveObservation = (Button)findViewById(R.id.saveObservationCOPUS);
        saveObservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                saveDataBase();
            }
        });
        mChartTeacher = (PieChart) findViewById(R.id.teacher_chart);
        mLineChartStudent = (LineChart) findViewById(R.id.student_line_chart);
        mLineChartTeacher = (LineChart) findViewById(R.id.teacher_line_chart);
        mChartTeacher.setRotationEnabled(true);
        mChartStudent = (PieChart) findViewById(R.id.student_chart);
        mChartStudent.setRotationEnabled(true);


        getDurations();
        putChartStudent();
        putChartTeacher();
        putLineChartStudent();
        putLineChartTeacher();

        try {
            mClient = new MobileServiceClient("https://stem-speech.azurewebsites.net", this);

        }catch (Exception e) {
            Log.e("MDUCClResAct",e.toString());
        }

    }

    private void putLineChartStudent(){
        SortedSet<Long> keys = new TreeSet<Long>(data.keySet());
        Log.d("EVENTOS, ",""+keys.size());
        long starting = keys.first();
        long finishing = keys.last();
        BigDecimal duration,delta,min,milli2min;
        milli2min = new BigDecimal("60000");
        duration = new BigDecimal(""+(finishing-starting)).divide(milli2min,2,BigDecimal.ROUND_CEILING);
        observationDuration = (float)duration.floatValue();

        for(Long k:keys){
            //BigDecimal delta,min,milli2min;
            int diff = (int)(k-starting);
            //BigDecimal min = new BigDecimal((diff/60000)*100/100);
            delta = new BigDecimal(""+diff);
            //milli2min = new BigDecimal("60000");
            min = delta.divide(milli2min,2,BigDecimal.ROUND_CEILING);
            float minutes = (float)min.floatValue();

            String event = data.get(k);

            if (event.charAt(0)=='s'){
                String code = event.substring(0,event.indexOf("-"));

                if (code.equals("sL") || code.equals("sInd") || code.equals("sTQ")){
                    mStudentEntriesIndividual.add(new Entry(minutes,1f));
                    Log.d("MINUTES",""+minutes+"INDIVIDUAL");
                }else if(code.equals("sWG")||code.equals("sOG")||code.equals("sWC")){
                    mStudentEntriesGroup.add(new Entry(minutes,2f));
                    Log.d("MINUTES",""+minutes+"GRUP");
                }else if (code.equals("sAnQ")||code.equals("sSQ")){
                    mStudentEntriesQuestion.add(new Entry(minutes,3f));
                    Log.d("MINUTES",""+minutes+"QUEST");
                }else if (code.equals("sW")||code.equals("sO")){
                    mStudentEntriesWaiting.add(new Entry(minutes,4f));
                    Log.d("MINUTES",""+minutes+"OTRO");
                }
            }else if (event.charAt(0)=='t'){
                String code = event.substring(0,event.indexOf("-"));
                if (code.equals("tLec") || code.equals("tRtW") || code.equals("tDV")){
                    mTeacherEntriesLecturing.add(new Entry(minutes, 1f));
                    Log.d("MINUTES",""+minutes+"INDIVIDUAL");
                }else if(code.equals("tFUp")||code.equals("tPQ")||code.equals("tCQ") || code.equals("tAnQ") || code.equals("tMG") || code.equals("t1o1")){
                    mTeacherEntriesQuestion.add(new Entry(minutes, 2f));
                    Log.d("MINUTES",""+minutes+"feed");
                }else if (code.equals("tAdm")||code.equals("tW")||code.equals("tO")){
                    mTeacherEntriesAdministration.add(new Entry(minutes, 3f));
                    Log.d("MINUTES",""+minutes+"otro");
                }
            }
        }

        if (sLTime + sIndTime + sTQTime + sWGTime + sOGTime + sWCTime + sAnQTime + sSQTime + sWTime != 0){
            LineDataSet datasetStudentIndividual;
            LineDataSet datasetStudentGroup;
            LineDataSet datasetStudentQuestion;
            LineDataSet datasetStudentWaiting;


            if (mLineChartStudent.getData() != null && mLineChartStudent.getData().getDataSetCount() > 0){
                datasetStudentIndividual = (LineDataSet)mLineChartStudent.getData().getDataSetByIndex(0);
                datasetStudentGroup = (LineDataSet)mLineChartStudent.getData().getDataSetByIndex(0);
                datasetStudentQuestion = (LineDataSet)mLineChartStudent.getData().getDataSetByIndex(0);
                datasetStudentWaiting = (LineDataSet)mLineChartStudent.getData().getDataSetByIndex(0);
                datasetStudentIndividual.setValues(mStudentEntriesIndividual);
                datasetStudentGroup.setValues(mStudentEntriesGroup);
                datasetStudentQuestion.setValues(mStudentEntriesQuestion);
                datasetStudentWaiting.setValues(mStudentEntriesWaiting);
                mLineChartStudent.getData().notifyDataChanged();
                mLineChartStudent.notifyDataSetChanged();
            }else{
                datasetStudentIndividual = new LineDataSet(mStudentEntriesIndividual,"Individual");
                datasetStudentGroup = new LineDataSet(mStudentEntriesGroup,"Group");
                datasetStudentQuestion = new LineDataSet(mStudentEntriesQuestion,"Question");
                datasetStudentWaiting = new LineDataSet(mStudentEntriesWaiting,"Waiting");

                if (mStudentEntriesIndividual.size() > 0){
                    datasetStudentIndividual.setColor(getResources().getColor(R.color.jyu_orange));
                    datasetStudentIndividual.setCircleColor(getResources().getColor(R.color.dark_jyu_orange));
                    datasetStudentIndividual.setLineWidth(3f);
                    datasetStudentIndividual.setCircleRadius(5f);
                    datasetStudentIndividual.setDrawCircleHole(false);
                    datasetStudentIndividual.setValueTextSize(0f);
                    datasetStudentIndividual.setDrawFilled(false);
                    datasetStudentIndividual.setFormLineWidth(3f);
                    datasetStudentIndividual.setFormLineDashEffect(new DashPathEffect(new float[] {10f,5f},0f));
                    datasetStudentIndividual.setFormSize(15f);
                }
                if (mStudentEntriesGroup.size() > 0){
                    datasetStudentGroup.setColor(getResources().getColor(R.color.jyu_blue));
                    datasetStudentGroup.setCircleColor(getResources().getColor(R.color.dark_jyu_blue));
                    datasetStudentGroup.setLineWidth(3f);
                    datasetStudentGroup.setCircleRadius(5f);
                    datasetStudentGroup.setDrawCircleHole(false);
                    datasetStudentGroup.setValueTextSize(0f);
                    datasetStudentGroup.setDrawFilled(false);
                    datasetStudentGroup.setFormLineWidth(3f);
                    datasetStudentGroup.setFormLineDashEffect(new DashPathEffect(new float[] {10f,5f},0f));
                    datasetStudentGroup.setFormSize(15f);
                }

                if (mStudentEntriesQuestion.size() > 0){
                    datasetStudentQuestion.setColor(getResources().getColor(R.color.jyu_green));
                    datasetStudentQuestion.setCircleColor(getResources().getColor(R.color.dark_jyu_green));
                    datasetStudentQuestion.setLineWidth(3f);
                    datasetStudentQuestion.setCircleRadius(5f);
                    datasetStudentQuestion.setDrawCircleHole(false);
                    datasetStudentQuestion.setValueTextSize(0f);
                    datasetStudentQuestion.setDrawFilled(false);
                    datasetStudentQuestion.setFillColor(Color.rgb(232,186,217));
                    datasetStudentQuestion.setFormLineWidth(3f);
                    datasetStudentQuestion.setFormLineDashEffect(new DashPathEffect(new float[] {10f,5f},0f));
                    datasetStudentQuestion.setFormSize(15f);
                }

                if (mStudentEntriesWaiting.size() > 0){
                    datasetStudentWaiting.setColor(getResources().getColor(R.color.dark_fucsia));
                    datasetStudentWaiting.setCircleColor(getResources().getColor(R.color.darker_fucsia));
                    datasetStudentWaiting.setLineWidth(3f);
                    datasetStudentWaiting.setCircleRadius(5f);
                    datasetStudentWaiting.setDrawCircleHole(false);
                    datasetStudentWaiting.setValueTextSize(0f);
                    datasetStudentWaiting.setDrawFilled(false);
                    datasetStudentWaiting.setFormLineWidth(3f);
                    datasetStudentWaiting.setFormLineDashEffect(new DashPathEffect(new float[] {10f,5f},0f));
                    datasetStudentWaiting.setFormSize(15f);
                }

            }

            ArrayList<ILineDataSet> dataSetsStudent = new ArrayList<ILineDataSet>();

            if (mStudentEntriesIndividual.size() > 0)
                dataSetsStudent.add(datasetStudentIndividual);
            if (mStudentEntriesGroup.size() > 0)
                dataSetsStudent.add(datasetStudentGroup);
            if (mStudentEntriesQuestion.size() > 0)
                dataSetsStudent.add(datasetStudentQuestion);
            if (mStudentEntriesWaiting.size() > 0)
                dataSetsStudent.add(datasetStudentWaiting);

            LineData studentData= new LineData(dataSetsStudent);

            mLineChartStudent.animateY(5000);
            mLineChartStudent.getAxisLeft().setDrawGridLines(false);
            mLineChartStudent.getXAxis().setDrawGridLines(false);
            mLineChartStudent.getXAxis().setDrawAxisLine(true);
            mLineChartStudent.getXAxis().setAxisMinimum(0f);
            mLineChartStudent.getXAxis().setAxisMaximum(observationDuration);
            LimitLine startingLimitLine = new LimitLine(0,"");
            startingLimitLine.setLineColor(getResources().getColor(R.color.black));
            LimitLine endingLimitLine = new LimitLine(observationDuration,"");
            endingLimitLine.setLineColor(getResources().getColor(R.color.black));
            mLineChartStudent.getXAxis().addLimitLine(startingLimitLine);
            mLineChartStudent.getXAxis().addLimitLine(endingLimitLine);
            YAxis yAxisStudent = mLineChartStudent.getAxisLeft();
            LimitLine l0 = new LimitLine(0,"");
            LimitLine l1 = new LimitLine(1, "Individual");
            LimitLine l2 = new LimitLine(2, "Group");
            LimitLine l3 = new LimitLine(3, "Question");
            LimitLine l4 = new LimitLine(4, "Waiting");
            l0.setLineColor(getResources().getColor(R.color.black));
            l0.disableDashedLine();
            l1.setLineColor(getResources().getColor(R.color.light_jyu_orange));
            l1.enableDashedLine(6f,3f,1f);
            l1.setTextSize(15f);
            l2.setLineColor(getResources().getColor(R.color.light_jyu_blue));
            l2.enableDashedLine(6f,3f,1f);
            l2.setTextSize(15f);
            l3.setLineColor(getResources().getColor(R.color.light_jyu_green));
            l3.enableDashedLine(6f,3f,1f);
            l3.setTextSize(15f);
            l4.setLineColor(getResources().getColor(R.color.light_fucsia));
            l4.enableDashedLine(6f,3f,1f);
            l4.setTextSize(15f);
            yAxisStudent.addLimitLine(l0);
            yAxisStudent.addLimitLine(l1);
            yAxisStudent.addLimitLine(l2);
            yAxisStudent.addLimitLine(l3);
            yAxisStudent.addLimitLine(l4);
            mLineChartStudent.setData(studentData);
            yAxisStudent.setDrawGridLines(false);
            yAxisStudent.setDrawLabels(false);
            yAxisStudent.setDrawZeroLine(true);
            yAxisStudent.setAxisMaximum(5);
            yAxisStudent.setAxisMinimum(0);
            mLineChartStudent.getAxisRight().setDrawGridLines(false);
            mLineChartStudent.getAxisRight().setDrawLabels(false);
            mLineChartStudent.getAxisRight().setDrawZeroLine(true);
            mLineChartStudent.getAxisRight().setAxisMaximum(5);
            mLineChartStudent.getAxisLeft().setAxisMinimum(0);
            mLineChartStudent.invalidate();
        }


        if (tLecTime + tRtWTime + tFUpTime + tPQTime + tCQTime + tAnQTime + tMGTime + tAdmTime + tWTime + t1o1Time + tDVTime!= 0){
            LineDataSet datasetTeacherLecturing;
            LineDataSet datasetTeacherQuestion;
            LineDataSet datasetTeacherAdministration;

            if (mLineChartTeacher.getData() != null && mLineChartTeacher.getData().getDataSetCount() > 0){
                datasetTeacherLecturing = (LineDataSet)mLineChartTeacher.getData().getDataSetByIndex(0);
                datasetTeacherQuestion = (LineDataSet)mLineChartTeacher.getData().getDataSetByIndex(0);
                datasetTeacherAdministration = (LineDataSet)mLineChartTeacher.getData().getDataSetByIndex(0);
                datasetTeacherLecturing.setValues(mTeacherEntriesLecturing);
                datasetTeacherQuestion.setValues(mTeacherEntriesQuestion);
                datasetTeacherAdministration.setValues(mTeacherEntriesAdministration);
                mLineChartTeacher.getData().notifyDataChanged();
                mLineChartTeacher.notifyDataSetChanged();
            }else{
                datasetTeacherLecturing = new LineDataSet(mTeacherEntriesLecturing,"Lecturing");
                datasetTeacherQuestion = new LineDataSet(mTeacherEntriesQuestion,"Question");
                datasetTeacherAdministration = new LineDataSet(mTeacherEntriesAdministration,"Administration");

                if (mTeacherEntriesLecturing.size() > 0){
                    datasetTeacherLecturing.setColor(getResources().getColor(R.color.jyu_orange));
                    datasetTeacherLecturing.setCircleColor(getResources().getColor(R.color.dark_jyu_orange));
                    datasetTeacherLecturing.setLineWidth(3f);
                    datasetTeacherLecturing.setCircleRadius(5f);
                    datasetTeacherLecturing.setDrawCircleHole(false);
                    datasetTeacherLecturing.setValueTextSize(0f);
                    datasetTeacherLecturing.setDrawFilled(false);
                    datasetTeacherLecturing.setFormLineWidth(3f);
                    datasetTeacherLecturing.setFormLineDashEffect(new DashPathEffect(new float[] {10f,5f},0f));
                    datasetTeacherLecturing.setFormSize(15f);
                }
                if (mTeacherEntriesQuestion.size() > 0){
                    datasetTeacherQuestion.setColor(getResources().getColor(R.color.jyu_blue));
                    datasetTeacherQuestion.setCircleColor(getResources().getColor(R.color.dark_jyu_blue));
                    datasetTeacherQuestion.setLineWidth(3f);
                    datasetTeacherQuestion.setCircleRadius(5f);
                    datasetTeacherQuestion.setDrawCircleHole(false);
                    datasetTeacherQuestion.setValueTextSize(0f);
                    datasetTeacherQuestion.setDrawFilled(false);
                    datasetTeacherQuestion.setFormLineWidth(3f);
                    datasetTeacherQuestion.setFormLineDashEffect(new DashPathEffect(new float[] {10f,5f},0f));
                    datasetTeacherQuestion.setFormSize(15f);
                }
                if (mTeacherEntriesAdministration.size() > 0){
                    datasetTeacherAdministration.setColor(getResources().getColor(R.color.jyu_green));
                    datasetTeacherAdministration.setCircleColor(getResources().getColor(R.color.dark_jyu_green));
                    datasetTeacherAdministration.setLineWidth(3f);
                    datasetTeacherAdministration.setCircleRadius(5f);
                    datasetTeacherAdministration.setDrawCircleHole(false);
                    datasetTeacherAdministration.setValueTextSize(0f);
                    datasetTeacherAdministration.setDrawFilled(false);
                    datasetTeacherAdministration.setFormLineWidth(3f);
                    datasetTeacherAdministration.setFormLineDashEffect(new DashPathEffect(new float[] {10f,5f},0f));
                    datasetTeacherAdministration.setFormSize(15f);
                }
            }


            ArrayList<ILineDataSet> dataSetsTeacher = new ArrayList<ILineDataSet>();

            if (mTeacherEntriesLecturing.size() > 0)
                dataSetsTeacher.add(datasetTeacherLecturing);
            if (mTeacherEntriesQuestion.size() > 0)
                dataSetsTeacher.add(datasetTeacherQuestion);
            if (mTeacherEntriesAdministration.size() > 0)
                dataSetsTeacher.add(datasetTeacherAdministration);

            LineData teacherData= new LineData(dataSetsTeacher);

            mLineChartTeacher.setData(teacherData);
            mLineChartTeacher.animateY(5000);

            mLineChartTeacher.getAxisLeft().setDrawGridLines(false);
            mLineChartTeacher.getXAxis().setDrawGridLines(false);
            mLineChartTeacher.getXAxis().setDrawAxisLine(true);
            mLineChartTeacher.getXAxis().setAxisMinimum(0f);
            mLineChartTeacher.getXAxis().setAxisMaximum(observationDuration);
            LimitLine startingLimitLine = new LimitLine(0,"");
            startingLimitLine.setLineColor(getResources().getColor(R.color.black));
            LimitLine endingLimitLine = new LimitLine(observationDuration,"finishing");
            endingLimitLine.setLineColor(getResources().getColor(R.color.black));
            mLineChartTeacher.getXAxis().addLimitLine(startingLimitLine);
            mLineChartTeacher.getXAxis().addLimitLine(endingLimitLine);
            YAxis yAxisTeacher = mLineChartTeacher.getAxisLeft();
            LimitLine l0 = new LimitLine(0,"");
            LimitLine l1 = new LimitLine(1, "Lecturing");
            LimitLine l2 = new LimitLine(2, "Question");
            LimitLine l3 = new LimitLine(3, "Administration");
            l0.setLineColor(getResources().getColor(R.color.black));
            l0.disableDashedLine();
            l1.setLineColor(getResources().getColor(R.color.light_jyu_orange));
            l1.enableDashedLine(6f,3f,2f);
            l1.setTextSize(15f);
            l2.setLineColor(getResources().getColor(R.color.light_jyu_blue));
            l2.enableDashedLine(6f,3f,2f);
            l2.setTextSize(15f);
            l3.setLineColor(getResources().getColor(R.color.light_jyu_green));
            l3.enableDashedLine(6f,3f,2f);
            l3.setTextSize(15f);
            yAxisTeacher.addLimitLine(l0);
            yAxisTeacher.addLimitLine(l1);
            yAxisTeacher.addLimitLine(l2);
            yAxisTeacher.addLimitLine(l3);
            yAxisTeacher.setDrawGridLines(false);
            yAxisTeacher.setDrawLabels(false);
            yAxisTeacher.setDrawZeroLine(true);
            yAxisTeacher.setAxisMaximum(4);
            yAxisTeacher.setAxisMinimum(0);
            mLineChartTeacher.getAxisRight().setDrawGridLines(false);
            mLineChartTeacher.getAxisRight().setDrawLabels(false);
            mLineChartTeacher.getAxisRight().setDrawZeroLine(true);
            mLineChartTeacher.getAxisRight().setAxisMaximum(4);
            mLineChartTeacher.getAxisLeft().setAxisMinimum(0);
            mLineChartTeacher.setData(teacherData);
            mLineChartTeacher.invalidate();
        }

    }

    private void putLineChartTeacher(){

    }

    private void saveDataBase(){
        List<Photo> p = COPUSRegister.getInstance().getPhotos();
        Hashtable<Long,String> events = COPUSRegister.getInstance().getEvents();
        Hashtable<Long,String> comments = COPUSRegister.getInstance().getComments();


        final Long time = Calendar.getInstance().getTimeInMillis();
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        ContentValues values_done = new ContentValues();
        values_done.put("time",time);
        values_done.put("observation_template","COPUS");
        final long done_id = db.insert("observation_done",null,values_done);
        Log.d("INSERTING","ID: "+done_id);
        //db.beginTransaction();

        Date d = new Date(time);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String t = df.format(d);
        Log.d("----",t);
        if (comments.size() > 0)
            for (Long l : comments.keySet()) {
                String fullComment = comments.get(l);

                String item = fullComment.substring(0, fullComment.indexOf("-"));
                String com = fullComment.substring(fullComment.indexOf("-") + 1);

                ContentValues values = new ContentValues();
                values.put("time", l);
                values.put("observation_template", "COPUS");
                values.put("observation_item", item);
                values.put("comment", com);
                Log.d("PONIENDO EN CODE", com);
                values.put("observation_code", done_id);
                db.insert("comments", null, values);

            }
        if (events.size() > 0)
            for(Long l:events.keySet()) {
                ContentValues values = new ContentValues();
                values.put("time", l);
                values.put("observation_template", "COPUS");
                values.put("code", events.get(l));
                Log.d("PONIENDO EN CODE", events.get(l));
                values.put("observation_code", done_id);
                db.insert("observation", null, values);

            }
        if (p.size() > 0){
            for(Photo photo:p) {
                ContentValues values = new ContentValues();
                values.put("time", photo.getTime());
                values.put("observation_template", "COPUS");
                values.put("observation_code",done_id);
                values.put("observation_item",photo.getItem());
                values.put("path",photo.getPath());
                values.put("comment",photo.getComment());
                db.insert("photos", null, values);

            }
        }

        //First we write the observations_done table
        /*final Long time = Calendar.getInstance().getTimeInMillis();
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        ContentValues values_done = new ContentValues();
        values_done.put("time",time);
        values_done.put("observation_template","COPUS");
        final long done_id = db.insert("observation_done",null,values_done);
        Log.d("INSERTING","ID: "+done_id);
        //db.beginTransaction();

        Date d = new Date(time);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String t = df.format(d);
        Log.d("----",t);


        obs.time = t;
        obs.observationTemplate = "COPUS";
        */


        /* ESTO ES PARA GRABAR EN LA BD AZURE
        mClient.getTable(observationsdone.class).insert(obs, new TableOperationCallback<observationsdone>() {
            @Override
            public void onCompleted(observationsdone entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null){
                    final String done_server = entity.getId();
                    //setObservationDone(entity.getId());
                    Log.d("*****",entity.getId());
                    postObservations(time,done_id,done_server);
                    postComments(time,done_id,done_server);
                }else{
                    Log.e("COPUS obdone",exception.getMessage());
                }
            }
        });

        */
        Intent intent = new Intent(this,ObserverMain.class);
        startActivity(intent);

    }

    public void postComments(long time, long done_id, String done_server_id){
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        Date d = new Date(time);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String t = df.format(d);


        for(Long l:commentsMap.keySet()){
            String fullComment = commentsMap.get(l);

            String item = fullComment.substring(0,fullComment.indexOf("-"));
            String com = fullComment.substring(fullComment.indexOf("-")+1);

            ContentValues values = new ContentValues();
            values.put("time",l);
            values.put("observation_template","COPUS");
            values.put("observation_item",item);
            values.put("comment",com);
            Log.d("PONIENDO EN CODE",com);
            values.put("observation_code",done_id);
            db.insert("comments",null,values);

            /*
            comments comment = new comments();
            comment.comment = com;
            comment.observationCode = ""+done_server_id;
            comment.time = t;
            comment.observationItem = item;
            comment.observationTemplate = "COPUS";


            mClient.getTable(comments.class).insert(comment, new TableOperationCallback<comments>() {
                @Override
                public void onCompleted(comments entity, Exception exception, ServiceFilterResponse response) {
                    if (exception == null){

                    }else{
                        Log.e("COPUS--",exception.getMessage());
                    }
                }
            });
*/

        }
    }

    public void postObservations(long time, long done_id, String done_server_id){
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        Date d = new Date(time);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String t = df.format(d);


        for(Long l:data.keySet()){
            ContentValues values = new ContentValues();
            values.put("time",l);
            values.put("observation_template","COPUS");
            values.put("code",data.get(l));
            Log.d("PONIENDO EN CODE",data.get(l));
            values.put("observation_code",done_id);
            db.insert("observation",null,values);

            /*
            observations observation = new observations();
            observation.code = data.get(l);
            observation.observationCode = ""+done_server_id;
            Log.d("ID",observation.observationCode);
            observation.observationTemplate = "COPUS";
            observation.time = t;


            mClient.getTable(observations.class).insert(observation, new TableOperationCallback<observations>() {
                @Override
                public void onCompleted(observations entity, Exception exception, ServiceFilterResponse response) {
                    if (exception == null){
                        Log.d("COMMENT","Se pudo poner");
                    }else{
                        Log.e("MIN",exception.getMessage());
                    }
                }
            });*/



        }
    }



    //Retorna la hora de inicio y duracion
    private Map<Long,Long> getResume(String Code){
        HashMap<Long,Long> mAux = new HashMap<Long, Long>();
        SortedSet<Long> keys = new TreeSet<Long>(data.keySet());
        Long initial = 0L;
        for (Long key : keys) {
            String value = data.get(key);
            if (value.substring(0,value.length()-2).equals(Code)){
                if (value.charAt(value.length()-1) == 'S'){
                    initial = key;
                }
                else{
                    mAux.put(initial,key-initial);
                }
            }
        }
        if (mAux.isEmpty()){
            return null;
        }else{
            return mAux;
        }

    }

    private void putChartStudent() {

        /*
        BigDecimal total_1,total_2,total_3,total_4;

        BigDecimal milliseconds = new BigDecimal("1000");
        BigDecimal individual, group, question, waiting;
        individual = new BigDecimal(""+(sLTime + sIndTime + sTQTime));
        group = new BigDecimal(""+(sWGTime + sOGTime + sWCTime));
        question = new BigDecimal(""+sAnQTime + sSQTime);
        waiting = new BigDecimal(""+sWTime);
        total_1 = individual.divide(milliseconds,3,)
        */
        float total_1 = ((sLTime + sIndTime + sTQTime));
        float total_2 = ((sWGTime + sOGTime + sWCTime));
        float total_3 = ((sAnQTime + sSQTime));
        float total_4 = ((sWTime));

        //ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        ArrayList<PieEntry> studentEntries = new ArrayList<PieEntry>();

        if (total_1 > 0)
            studentEntries.add(new PieEntry((total_1 / (total_1 + total_2 + total_3 + total_4) * 100), "Individual"));

        if (total_2 > 0)
            studentEntries.add(new PieEntry((total_2 / (total_1 + total_2 + total_3 + total_4) * 100), "Group"));

        if (total_3 > 0)
            studentEntries.add(new PieEntry((total_3 / (total_1 + total_2 + total_3 + total_4) * 100), "Question"));

        if (total_4 > 0)
            studentEntries.add(new PieEntry((total_4 / (total_1 + total_2 + total_3 + total_4) * 100), "Waiting"));


        // create pieDataSet
        PieDataSet dataSet = new PieDataSet(studentEntries, "Student");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);

        // adding colors
        ArrayList<Integer> colors = new ArrayList<Integer>();

        // Added My Own colors

        if (total_1 > 0)
            colors.add(getResources().getColor(R.color.light_jyu_orange));

        if (total_2 > 0)
            colors.add(getResources().getColor(R.color.light_jyu_blue));

        if (total_3 > 0)
            colors.add(getResources().getColor(R.color.light_jyu_green));

        if (total_4 > 0)
            colors.add(getResources().getColor(R.color.light_fucsia));


        dataSet.setColors(colors);

        PieData studentData = new PieData(dataSet);
        studentData.setValueFormatter(new PercentFormatter());
        studentData.setValueTextSize(11f);
        studentData.setValueTextColor(Color.BLACK);


        //studentData.setValueTypeface(mTfLight);
        mChartStudent.setData(studentData);

        // undo all highlights
        mChartStudent.highlightValues(null);

        mChartStudent.invalidate();
        mChartStudent.setEntryLabelColor(Color.BLACK);
        mChartStudent.setEntryLabelTextSize(9f);

        mChartStudent.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Legend l = mChartStudent.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

    }

    private void putChartTeacher(){
        float total_1 = (int)((tLecTime + tRtWTime + tDVTime)/1000);
        float total_2 = (int)((tFUpTime + tPQTime + tCQTime + tAnQTime + tMGTime + t1o1Time)/1000);
        float total_3 = (int)((tAdmTime + tWTime)/1000);

        ArrayList<PieEntry> teacherEntries = new ArrayList<PieEntry>();

        if (total_1 != 0)
            teacherEntries.add(new PieEntry(total_1/(total_1+total_2+total_3)*100,"Lecturing"));

        if (total_2 != 0)
            teacherEntries.add(new PieEntry(total_2/(total_1+total_2+total_3)*100,"Question/Feedback"));

        if (total_3 != 0)
            teacherEntries.add(new PieEntry(total_3/(total_1+total_2+total_3)*100,"Administration"));




        // create pieDataSet
        PieDataSet dataSet = new PieDataSet(teacherEntries,"Teacher");
        dataSet.setSliceSpace(2);
        dataSet.setSelectionShift(5);

        // adding colors
        ArrayList<Integer> colors = new ArrayList<Integer>();

        // Added My Own colors
        if (total_1 !=0)
            colors.add(getResources().getColor(R.color.light_jyu_orange));

        if (total_2 != 0)
            colors.add(getResources().getColor(R.color.light_jyu_blue));

        if (total_3 != 0)
            colors.add(getResources().getColor(R.color.light_jyu_green));

        dataSet.setColors(colors);

        PieData teacherData = new PieData(dataSet);
        teacherData.setValueFormatter(new PercentFormatter());
        teacherData.setValueTextSize(11f);

        teacherData.setValueTextColor(Color.BLACK);
        //studentData.setValueTypeface(mTfLight);
        mChartTeacher.setData(teacherData);

        // undo all highlights
        mChartTeacher.highlightValues(null);

        mChartTeacher.invalidate();


        mChartTeacher.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        mChartTeacher.setEntryLabelColor(Color.BLACK);
        mChartTeacher.setEntryLabelTextSize(9f);


        Legend l = mChartTeacher.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

    }



    private void getDurations(){
        HashMap<Long,Long> mAux = new HashMap<Long, Long>();
        SortedSet<Long> keys = new TreeSet<Long>(data.keySet());
        Long initial = 0L;

        for (Long key : keys) {

            String value = data.get(key);
            String code = value.substring(0,value.length()-2);
            String event = ""+value.charAt(value.length()-1);

            if (value.charAt(0) == 't'){
                if (event.equals("S")){
                    initial = key;
                }else{
                    switch (code){
                        case "tLec":
                            tLecTime += key-initial;
                            break;
                        case "tW":
                            tWTime += key - initial;
                            break;
                        case "tRtW":
                            tRtWTime += key - initial;
                            break;
                        case "tFUp":
                            tFUpTime += key - initial;
                            break;
                        case "tPQ":
                            tPQTime += key - initial;
                            break;
                        case "tCQ":
                            tCQTime += key - initial;
                            break;
                        case "tAnQ":
                            tAnQTime += key - initial;
                            break;
                        case "tMG":
                            tMGTime += key - initial;
                            break;
                        case "t1o1":
                            t1o1Time += key - initial;
                            break;
                        case "tDV":
                            tDVTime += key - initial;
                            break;
                        case "tAdm":
                            tAdmTime += key - initial;
                            break;
                    }
                }
            }
            else{
                if (event.equals("S")){
                    initial = key;
                }else{
                    switch (code){
                        case "sL":
                            sLTime += key-initial;
                            break;
                        case "sInd":
                            sIndTime += key - initial;
                            break;
                        case "sCG":
                            sCGTime += key - initial;
                            break;
                        case "sWG":
                            sWGTime += key - initial;
                            break;
                        case "sAnQ":
                            sAnQTime += key - initial;
                            break;
                        case "sSQ":
                            sSQTime += key - initial;
                            break;
                        case "sWC":
                            sWCTime += key - initial;
                            break;
                        case "sPrd":
                            sPrdTime += key - initial;
                            break;
                        case "sSP":
                            sSPTime += key - initial;
                            break;
                        case "sTQ":
                            sTQTime += key - initial;
                            break;
                        case "sW":
                            sWTime += key - initial;
                            break;
                        case "sOG":
                            sOGTime += key - initial;
                            break;
                    }
                }
            }
        }
    }
}
