package com.example.peerreviewciae;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Daniela on 24-06-2016.
 * SINGLETON que va registrando todos los eventos, comentarios y fotos que el usuario ingresa
 */
public class COPUSRegister extends Application{
    private Hashtable<Long,String> events;
    private Hashtable<Long,String> comments;
    private Hashtable<String,String> photos;
    private Hashtable<String,String> photosComment;
    private List<Photo> photoList = new ArrayList<>();
    private static COPUSRegister ourInstance = new COPUSRegister();
    private boolean mHasStarted = false;
    private COPUSRegisterListener listener;
    private COPUSRegisterListenerTeacher listenerTeacher;
    String[] codesStudent = {"sL","sInd","sCG","sWG","sAnQ","sSQ","sWC","sPrd","sSP","sTQ","sW","sOG"};
    String[] codesTeacher = {"tLec","tW","tRtW","tFUp","tPQ","tCQ","tAnQ","tMG","t1o1","tDV","tAdm"};
    Boolean[] isOnStudent = {false,false,false,false,false,false,false,false,false,false,false,false};
    Boolean[] isOnTeacher = {false,false,false,false,false,false,false,false,false,false,false};


    public static COPUSRegister getInstance() {
        return ourInstance;
    }


    private COPUSRegister() {
        events = new Hashtable<Long,String>();
        comments = new Hashtable<Long,String>();
        photos = new Hashtable<String,String>();
        photosComment = new Hashtable<String,String>();
        this.listener = null;
    }


    public void put(Long milliseconds, String event){
        Log.d("SINGLETON","Se trata de agregar"+milliseconds.toString()+"  "+event.toString());
        events.put(milliseconds,event);
        String code = event.substring(0,event.length()-2);
        char p = event.charAt(event.length()-1);
        Log.d("SACANDO",""+p);
        Boolean isOn = p == 'S';
        Boolean isStudent = event.charAt(0) == 's';
        Boolean isTeacher = event.charAt(0) == 't';

        if (isStudent){
            for (int i = 0; i < codesStudent.length; i++) {
                if (codesStudent[i].equals(code)){
                    isOnStudent[i] = isOn;
                }
            }
        }else if (isTeacher){
            for (int i = 0; i < codesTeacher.length; i++) {
                if (codesTeacher[i].equals(code)){
                    isOnTeacher[i] = isOn;
                }
            }
        }

    }

    public void putComment(Long milliseconds, String comment, String item){
        Log.d("SINGLETON","Se pone el comentario"+comment.toString());
        comments.put(milliseconds,item+"-"+comment);
    }

    public void startObservation(){
        Log.d("SINGLETON","Se llamo a startObservation");
        events.clear();
        comments.clear();
        mHasStarted = true;
        photoList.clear();
        events.put(Calendar.getInstance().getTimeInMillis(),"START");
    }



    public List<Photo> getPhotos() {return photoList;}

    public Hashtable<Long, String> getEvents(){
        return events;
    }

    public Hashtable<Long, String> getComments() {
        return comments;
    }

    public boolean hasObservationStarted(){
        return mHasStarted;
    }

    public void end(){
        mHasStarted = false;
        Long time = Calendar.getInstance().getTimeInMillis();
        Long time_finished = finishOpenEvents(time);

        Log.d("Tiempo",""+time);
        Log.d("TFinished",""+time_finished);
        events.put(time_finished+1,"FINISH");

    }

    public long finishOpenEvents(Long time){
        Long t = time;
        for (int i = 0; i < isOnStudent.length; i ++){
            if (isOnStudent[i]){
                put(t,codesStudent[i]+"-E");
                t += 1;
            }
        }

        for (int i = 0; i < isOnTeacher.length; i ++){
            if (isOnTeacher[i]){
                put(t,codesTeacher[i]+"-E");
                t += 1;
            }
        }
        return t;
    }

    public void putPhoto(String path, String comment){
        Photo p = new Photo();
        p.setPath(path);
        p.setItem("");
        p.setComment(comment);
        p.setTime(Calendar.getInstance().getTimeInMillis());
        photoList.add(p);
        photos.put("",path);
        photosComment.put("",comment);
    }

    public interface COPUSRegisterListener{
        public void onObservationStarted(boolean isStarted);
    }

    public interface COPUSRegisterListenerTeacher{
        public void onObservationStarted(boolean isStarted);
    }

    public void setCOPUSRegisterListener(COPUSRegisterListener listener){
        this.listener = listener;
        listener.onObservationStarted(false);
    }

    public void setCOPUSRegisterListenerTeacher(COPUSRegisterListenerTeacher listener){
        this.listenerTeacher = listener;
        listenerTeacher.onObservationStarted(false);
    }
}
