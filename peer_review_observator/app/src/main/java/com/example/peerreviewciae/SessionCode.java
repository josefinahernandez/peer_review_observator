package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SessionCode extends Activity {

    public static String finalActivitySessionID;

    String activitySessionID;
    String activitySessionName;
    String activitySessionDescription;
    String activitySessionLoginCode;

    TextView incorrectCode;

    Boolean isAdmin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_code);

        activitySessionID = getIntent().getStringExtra("activityID");
        activitySessionName = getIntent().getStringExtra("activityName");
        activitySessionDescription = getIntent().getStringExtra("activityDescription");
        activitySessionLoginCode = getIntent().getStringExtra("activityLoginCode");

        isAdmin = getIntent().getBooleanExtra("isAdmin", false);

        TextView actName = (TextView)findViewById(R.id.actName_textView);
        TextView actDescrip = (TextView)findViewById(R.id.actDescription_textView);

        incorrectCode = (TextView)findViewById(R.id.incorrect_textView);
        incorrectCode.setVisibility(View.GONE);

        actName.setText(activitySessionName);
        actDescrip.setText(activitySessionDescription);
    }

    public void onOKPress(View view){
        EditText codeInput = (EditText)findViewById(R.id.code_editText);
        String codeIn = codeInput.getText().toString();

        if(codeIn.equals(activitySessionLoginCode)){

            finalActivitySessionID = activitySessionID;

            if(isAdmin){
                Intent intent = new Intent (this, Admin.class);
                startActivity(intent);
            }

            else {
                Intent intent = new Intent(this, ChooseObservatorOrPeerreview.class);
                intent.putExtra("activityID", activitySessionID);
                intent.putExtra("activityName", activitySessionName);
                intent.putExtra("activityDescription", activitySessionDescription);
                intent.putExtra("requiresSessionAdd", true);
                this.finish();
                startActivity(intent);
            }

        }

        else {
            incorrectCode.setVisibility(View.VISIBLE);
        }


    }
}
