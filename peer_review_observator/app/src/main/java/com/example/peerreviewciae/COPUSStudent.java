package com.example.peerreviewciae;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

//import android.app.Fragment;


/**
 * Created by Daniela on 22-06-2016.
 */
public class COPUSStudent extends Fragment {
    private List<COPUSData> mList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private CustomCopusAdapter mAdapter;

    COPUSRegister cr = COPUSRegister.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_copus_student, container, false);


        if (mList.isEmpty()){
            mList.add(new COPUSData("sL",getString(R.string.sL_COPUS)));
            mList.add(new COPUSData("sInd",getString(R.string.sInd_COPUS)));
            mList.add(new COPUSData("sCG",getString(R.string.sCG_COPUS)));
            mList.add(new COPUSData("sWG",getString(R.string.sWG_COPUS)));
            mList.add(new COPUSData("sOG",getString(R.string.sOG_COPUS)));
            mList.add(new COPUSData("sAnQ",getString(R.string.sAnQ_COPUS)));
            mList.add(new COPUSData("sSQ",getString(R.string.sSQ_COPUS)));
            mList.add(new COPUSData("sWC",getString(R.string.sWC_COPUS)));
            mList.add(new COPUSData("sPrd",getString(R.string.sPrd_COPUS)));
            mList.add(new COPUSData("sSP",getString(R.string.sSP_COPUS)));
            mList.add(new COPUSData("sTQ",getString(R.string.sTQ_COPUS)));
            mList.add(new COPUSData("sW",getString(R.string.sW_COPUS)));

        }

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_student_copus);
        mRecyclerView.setLayoutManager(new GridLayoutManager(COPUSStudent.this.getContext(), 2));
        mAdapter = new CustomCopusAdapter(this.getContext(),mList);
        mRecyclerView.setAdapter(mAdapter);


        return rootView;
    }


}

