package com.example.peerreviewciae;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Esta clase no se usa
 */
public class Stage_CompleteAdapter extends ArrayAdapter<Stage_Complete> {

    Context context;

    int layoutResourceId;

    public Stage_CompleteAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        context = context;
        layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        return row;
    }
}
