package com.example.peerreviewciae;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ObserverMain extends AppCompatActivity {
    private ListView obs_list_view;
    private ListView obs_done_view;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> obs_done_list;
    private static final String LOG_TAG = ObserverMain.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observer_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarObserver);
        setSupportActionBar(toolbar);

        obs_list_view = (ListView) findViewById(R.id.observation_list);
        obs_done_view = (ListView) findViewById(R.id.observations_done_list);

        obs_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?>  parent, View clickView, int position, long id) {
                Log.d("OBSERVER",Integer.toString(position));
                switch (position){
                    case 0:
                        Intent intent_copus = new Intent(clickView.getContext(),COPUSActivity.class);
                        startActivity(intent_copus);
                        Log.d(LOG_TAG,"COPUS");
                        break;
                    case 1:
                        //Intent intent_copus = new Intent(clickView.getContext(),COPUSActivity.class);
                        //startActivity(intent_copus);
                        //Log.d(LOG_TAG,"COPUS");
                        break;
                    case 2:
                        //Intent intent_jyu = new Intent(clickView.getContext(),JYUActivity.class);
                        //startActivity(intent_jyu);
                        //Log.d(LOG_TAG,"JYU");
                        break;
                }
            }
        });

        PeerReviewDB dbhelper = new PeerReviewDB(getApplicationContext());
        Log.d(LOG_TAG, "Creando base de datos");
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        Log.d(LOG_TAG, "getReadableDataBase");
        String selectQuery = "SELECT * FROM observation_done";
        obs_done_list = new ArrayList<>();
        Log.d(LOG_TAG, "BUSCANDO "+selectQuery);
        Cursor c = db.rawQuery(selectQuery,null);

        if (c.moveToFirst()){
            do {
                String observationText = "";
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                int id = c.getInt(c.getColumnIndex("_id"));
                String template = c.getString(c.getColumnIndex("observation_template"));
                long time = c.getLong(c.getColumnIndex("time"));
                String code = c.getString(c.getColumnIndex("id_session"));
                observationText += id + "-"+ "("+template+")"+formatter.format(time)+" - session: "+code+"\n";
                obs_done_list.add(observationText);
            }while(c.moveToNext());
        }

        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, obs_done_list){

            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                TextView ListItemShow = (TextView) view.findViewById(android.R.id.text1);
                ListItemShow.setTextColor(Color.parseColor("#262626"));

                return view;
            }

        };

        obs_done_view.setAdapter(adapter);

        obs_done_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent timeLineIntent = new Intent (ObserverMain.this, TimeLineActivity.class);
                String data = (String)obs_done_view.getItemAtPosition(position);
                timeLineIntent.putExtra("data",data);
                startActivity(timeLineIntent);
                Log.d(LOG_TAG,"Deberia irse a dar resumen de observacion");
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_observer, menu);

        MenuItem welcomeName = menu.findItem(R.id.userLoggedIn);
        welcomeName.setTitle("Bienvenido " + Login.userNameLoggedIn);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {

            return true;
        }

        else if (id == R.id.changeActivity){
            Intent intent = new Intent(this, SelectActivitySession.class);
            startActivity(intent);

            return true;
        }

        else if (id == R.id.changeToPeerReview){
            Intent intent = new Intent(this, StagesGrid.class);
            startActivity(intent);

            return true;
        }

        else if (id == R.id.closeSession){
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);

            return true;
        }
        else{
            return true;
        }

    }

}
