package com.example.peerreviewciae;

/**
 * Created by Daniela on 20-07-2016.
 */
public class COPUSData {
    private String description_code; //sLEC...
    private String description; //Lecturing
    public final String COPUS_ITEM_START = "S";
    public final String COPUS_ITEM_END = "E";
    private boolean isRecording = false;
    private boolean isObservationRecording = false;

    public COPUSData(String description_code, String description){
        this.description_code = description_code;
        this.description = description;
    }


    public String getDescriptionCode(){
        return description_code;
    }

    public String getDescription() {
        return description;
    }

    public void setIsRecording(){
        isRecording = true;
    }

    public boolean isRecording(){
        return isRecording;
    }

    public void setNotRecording(){
        isRecording = false;
    }

    public void setObservationRecording(){
        isObservationRecording = true;
    }

    public boolean isObservationRecording(){
        return isObservationRecording;
    }
}
