package com.example.peerreviewciae;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.zumoappname.R;

import java.util.Calendar;
import java.util.Hashtable;

public class COPUSActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private COPUSRegister register = COPUSRegister.getInstance();
    private Chronometer chronometer;
    private String comment = "";
    private final String LOG_TAG = COPUSActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copus);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        chronometer = (Chronometer)findViewById(R.id.chronometer_copus);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }
        });

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        //Generamos un AlertDialog que permite comenzar la actividad
        alertDialogBuilder.setPositiveButton(getString(R.string.start), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(COPUSActivity.this,getString(R.string.observation_start),Toast.LENGTH_LONG).show();
                startObservation();
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
            }
        });

        alertDialogBuilder.setNegativeButton(R.string.cancel,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialogBuilder.setMessage(getString(R.string.copus_dialog));

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    /*
   Pedimos confirmar cuando se aprieta boton para atras
    */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.really_cancel_observation))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do finish
                            COPUSActivity.this.finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do nothing
                            return;
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }
        return super.onKeyDown(keyCode,event);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //El menu copus tiene Terminar, comentario o foto...
        getMenuInflater().inflate(R.menu.menu_copus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.copus_finish) {
            stopObservation();
            chronometer.stop();
            register.getEvents();
            showClassResume();
            finish();
            return true;
        }else if (id == R.id.copus_comment){
            promptComment();
            return true;
        }else if (id == R.id.copus_photo){
            dispatchTakePictureIntent();
            return true;
        }else{
            return true;
        }

        //return super.onOptionsItemSelected(item);
    }

    /*
    Setea el comentario que hace el usuario
     */
    private void setComment(String newComment){
        comment = newComment;
    }

    /*
    Levanta el dialog para que el usuario ingrese el comentario que desea
     */
    private void promptComment(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View viewDialog = inflater.inflate(R.layout.copus_comment,null);

        builder.setView(viewDialog)
                .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int id){
                        EditText commentEditText = (EditText)viewDialog.findViewById(R.id.copus_comment_editText);
                        RadioGroup radioGroup = (RadioGroup)viewDialog.findViewById(R.id.radioComment);
                        setComment(commentEditText.getText().toString());
                        switch (radioGroup.getCheckedRadioButtonId()){
                            case R.id.radioButtonStudent:
                                register.putComment(Calendar.getInstance().getTimeInMillis(),comment,"S");
                                Log.d(LOG_TAG,comment+"-S");
                                break;
                            case R.id.radioButtonTeacher:
                                register.putComment(Calendar.getInstance().getTimeInMillis(),comment,"T");
                                Log.d(LOG_TAG,comment+"-T");
                                break;
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel),new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){

                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /*
    Permite irnos al resumen de clase donde van a estar todos los graficos
     */
    private void showClassResume(){
        Intent classresumeIntent = new Intent(this,COPUSClassResumeActivity.class);
        Hashtable<Long,String> resume = register.getEvents();
        Hashtable<Long,String> comments = register.getComments();
        for (Long name: resume.keySet()){
            String key =name.toString();
            String value = resume.get(name).toString();
            Log.d("STUDENT",key + " " + value);


        }
        classresumeIntent.putExtra("table",resume);
        classresumeIntent.putExtra("comments",comments);
        startActivity(classresumeIntent);

    }

    /*
    Inicia la observacion en el register
     */
    private void startObservation(){
        register.startObservation();
    }

    /*
    Para la observacion en el register
     */
    private void stopObservation(){
        register.end();
    }

    /*
    Levanta el intent para sacar la foto
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(this, PhotographActivity.class);
        takePictureIntent.putExtra("template",1);
        startActivity(takePictureIntent);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_copus, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            switch (position){
                case 0:
                    COPUSTeacher tab1 = new COPUSTeacher();
                    return tab1;
                case 1:
                    COPUSStudent tab2 = new COPUSStudent();
                    return tab2;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.teacher);
                case 1:
                    return getResources().getString(R.string.student);
            }
            return null;
        }
    }
}
