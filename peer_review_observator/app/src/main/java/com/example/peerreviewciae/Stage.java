package com.example.peerreviewciae;

import java.util.Date;

/**
 * Created by Josefina on 13-09-2016.
 */
public class Stage {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("Activity_id")
    private String Activity_id;

    @com.google.gson.annotations.SerializedName("Stage_Name")
    private String Stage_Name;

    @com.google.gson.annotations.SerializedName("Stage_Status")
    private String Stage_Status;

    @com.google.gson.annotations.SerializedName("Start_Date")
    private Date Start_Date;

    @com.google.gson.annotations.SerializedName("Close_Date")
    private Date Close_Date;

    @com.google.gson.annotations.SerializedName("Stage_Type")
    private String Stage_Type;

    @com.google.gson.annotations.SerializedName("Answer_Type")
    private String Answer_Type;

    @com.google.gson.annotations.SerializedName("Last_Stage")
    private Boolean Last_Stage;

    @com.google.gson.annotations.SerializedName("Instructions")
    private String Instructions;

    @com.google.gson.annotations.SerializedName("Amount_Revision_Assignments")
    private int Amount_Revision_Assignments;

    @com.google.gson.annotations.SerializedName("Assignment_Status")
    private String Assignment_Status;

    @com.google.gson.annotations.SerializedName("Stage_Order")
    private int Stage_Order;

    @com.google.gson.annotations.SerializedName("Max_Grade")
    private String Max_Grade;


    public Stage(){

    }

    public Stage(String id){
        this.setId(id);
    }

    public String getID () {return id;}
    public final void setId(String id){this.id = id;}
    public String getActivity_id(){return Activity_id;}
    public void setActivity_id(String at){this.Activity_id= at;}

    public String getStage_Name() {return Stage_Name;}
    public void setStage_Name(String desc) {this.Stage_Name= desc;}

    public String getStage_Status() {return Stage_Status;}
    public void setStage_Status(String str) {this.Stage_Status= str;}

    public String getStage_Type() {return Stage_Type;}
    public void setStage_Type(String str) {this.Stage_Type= str;}

    public String getAnswer_Type() {return Answer_Type;}
    public void setAnswer_Type(String str) {this.Answer_Type= str;}

    public String getInstructions() {return Instructions;}
    public void setInstructions(String str) {this.Instructions= str;}

    public String getAssignment_Status() {return Assignment_Status;}
    public void setAssignment_Status(String str) {this.Assignment_Status= str;}

    public String getMax_Grade() {return Max_Grade;}
    public void setMax_Grade(String str) {this.Max_Grade= str;}

    public Date getStart_Date () {return Start_Date;}
    public void setStart_Date (Date str) {this.Start_Date = str;}

    public Date getClose_Date_Date () {return Close_Date;}
    public void setClose_Date (Date str) {this.Close_Date = str;}

    public Boolean getLast_Stage() {return Last_Stage;}
    public void setLast_Stage (Boolean str) {this.Last_Stage = str;}

    public int getAmount_Revision_Assignments () {return Amount_Revision_Assignments;}
    public void setAmount_Revision_Assignments (int str) {this.Amount_Revision_Assignments= str;}

    public int getStage_Order () {return Stage_Order;}
    public void setStage_Order (int str) {this.Stage_Order= str;}




}
