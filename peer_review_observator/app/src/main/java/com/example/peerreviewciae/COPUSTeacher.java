package com.example.peerreviewciae;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

//import android.app.Fragment;


/**
 * Created by Daniela on 22-06-2016.
 */
public class COPUSTeacher extends Fragment {
    private List<COPUSData> mList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private CustomCopusAdapter mAdapter;

    COPUSRegister cr = COPUSRegister.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_copus_student, container, false);


        if (mList.isEmpty()){
            mList.add(new COPUSData("tLec",getString(R.string.tLec_COPUS)));
            mList.add(new COPUSData("tW",getString(R.string.tW_COPUS)));
            mList.add(new COPUSData("tRtW",getString(R.string.tRtW_COPUS)));
            mList.add(new COPUSData("tFUp",getString(R.string.tFUp_COPUS)));
            mList.add(new COPUSData("tPQ",getString(R.string.tPQ_COPUS)));
            mList.add(new COPUSData("tCQ",getString(R.string.tCQ_COPUS)));
            mList.add(new COPUSData("tAnQ",getString(R.string.tAnQ_COPUS)));
            mList.add(new COPUSData("tMG",getString(R.string.tMG_COPUS)));
            mList.add(new COPUSData("t1o1",getString(R.string.t101_COPUS)));
            mList.add(new COPUSData("tDV",getString(R.string.tDV_COPUS)));
            mList.add(new COPUSData("tAdm",getString(R.string.tAdm_COPUS)));


        }

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_student_copus);
        mRecyclerView.setLayoutManager(new GridLayoutManager(COPUSTeacher.this.getContext(), 2));
        mAdapter = new CustomCopusAdapter(this.getContext(),mList);
        mRecyclerView.setAdapter(mAdapter);


        return rootView;
    }


}

